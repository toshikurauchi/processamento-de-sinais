\section{Filtragem e convolução (Capítulo 4)}

\underline{\textbf{Exemplo}}: filtro da média

\begin{itemize}
	\item entrada: $x_n, n = 0, 1, \dots, N-1$
	\item saída: $y_n = \frac{1}{2}x_n + \frac{1}{2}x_{n-1}$
	\item hipótese inicial: extensão periódica de $x$ ($x_n = x_{(n\mod N)}$)
\end{itemize}

Considere a entrada

\begin{equation*}
	x_n = e^{i2\pi qn/N}, n = 0, 1, \dots, N-1, q = 0, 1, \dots, N/2
\end{equation*}

teremos

\begin{align*}
	y_n &= \frac{1}{2}e^{i2\pi qn/N} + \frac{1}{2}e^{i2\pi q(n-1)/N} \\
	&= \frac{1}{2}e^{i2\pi q\left(n-\frac{1}{2}\right)/N}\left[e^{i2\pi q\frac{1}{2}\frac{1}{N}} + e^{-i2\pi q\frac{1}{2}\frac{1}{N}}\right] \\
	&= \cos\left(2\pi q \frac{1}{2}\frac{1}{N}\right)e^{i2\pi q\left(-\frac{1}{2}\right)/N}e^{i2\pi qn/N} \\
	&= H_q x_n
\end{align*}

Onde $H_q = \cos\left(2\pi q \frac{1}{2}\frac{1}{N}\right)e^{i2\pi q\left(-\frac{1}{2}\right)/N}$ e $|H_q| = \cos\left(2\pi q\frac{1}{2}\frac{1}{N}\right)$. Ou seja, equivale a uma atenuação que depende da frequência e uma mudança de fase (filtro passa baixa).

No caso real ($x_n = \sin(2\pi qn/N) = imag(e^{i2\pi qn/N})$), teremos

\begin{align*}
	y_n &= imag(H_qx_n) \\
	&= \cos\left(2\pi q \frac{1}{2}\frac{1}{N}\right)\sin\left(2\pi q\left(-\frac{1}{2}\right)/N\right)\sin(2\pi qn/N) \\
	&= \sin\left(2\pi q\left(n-\frac{1}{2}\right)/N\right)\sin(2\pi qn/N) \\
	&= A\sin(2\pi qn/N) - B\cos(2\pi qn/N)
\end{align*}

Onde $A = \frac{1}{2} + \frac{1}{2}\cos(2\pi q/N)$ e $B = \frac{1}{2}\sin(2\pi q/N)$.

Observe que:

\begin{align*}
	\begin{pmatrix}
		y_0 \\ y_1 \\ y_2 \\ \vdots \\ y_{N-1}
	\end{pmatrix}
	&= \frac{1}{2}\begin{pmatrix}
		1 & 0 & \dots & \dots & 0 & 1 \\
		1 & 1 & 0 & \dots & \dots & 0 \\
		0 & 1 & 1 & 0 & \dots & 0 \\
		\vdots & \vdots & \ddots & \ddots & \ddots & \vdots \\
		0 & \dots & \dots & 0 & 1 & 1
	\end{pmatrix}\begin{pmatrix}
		x_0 \\ x_1 \\ x_2 \\ \vdots \\ x_{N-1}
	\end{pmatrix}
\end{align*}

\textbf{Def. 4.2.1}: Se $x, y\in\mathbb{C}^N$, definimos a convolução circular de $x$ e $y$ como o vetor $w\in\mathbb{C}^N$ dado por

\begin{equation*}
	w_n = \sum\limits_{k=0}^{N-1}x_ky_{n-k}
\end{equation*}

denotamos $w=x*y$.

Por exemplo:

\begin{align*}
	w_0 (n=0) &= \begin{matrix}
		& x_0 & x_1 & x_2 & \dots & x_{N-1} \\
		& \times & \times & \times & \dots & \times \\
		& y_0 & y_{-1} & y_{-2} & \dots & y_{-(N-1)} \\
		& & y_{N-1} & y_{N-2} & \dots & y_1 \\
		\hline
		\sum & \rightarrow & w_0
	\end{matrix} \\
	w_1 (n=1) &= \begin{matrix}
		& x_0 & x_1 & x_2 & \dots & x_{N-1} \\
		& \times & \times & \times & \dots & \times \\
		& y_1 & y_{0} & y_{-1} & \dots & y_{-(N-2)} \\
		& & & y_{N-1} & \dots & y_2 \\
		\hline
		\sum & \rightarrow & w_1
	\end{matrix}
\end{align*}

em geral

\begin{align*}
	w_n &= \begin{matrix}
		& x_0 & x_1 & x_2 & \dots & x_{N-1} \\
		& \times & \times & \times & \dots & \times \\
		& y_n & y_{n-1} & y_{n-2} & \dots & y_{n-(N-1)} \\
		\sum & \rightarrow & w_n
	\end{matrix}
\end{align*}

observe que:

\begin{equation*}
	W = \begin{pmatrix}
		y_0 & y_{N-1} & y_{N-2} & \dots & y_1 \\
		y_1 & y_0 & y_{N-1} & \dots & y_2 \\
		y_2 & y_1 & y_0 & \dots & y_3 \\
		\vdots & \vdots & \vdots & \ddots & \vdots \\
		y_{N-1} & y_{N-2} & y_{N-3} & \dots & y_0 \\
	\end{pmatrix}\begin{pmatrix}
		x_0 \\ x_1 \\ \vdots \\ x_{N-1}
	\end{pmatrix}
\end{equation*}

\underline{obs. 1}: o filtro da média corresponde à equação $y = x*l$ onde $l_0 = \frac{1}{2}, l_1 = \frac{1}{2}, l_n = 0, \forall n\neq 0, 1$

\underline{obs. 2}: como $x$ e $y$ são tratados pela hipótese de extensão periódica, temos que

\begin{equation*}
	\sum\limits_{n=0}^{N-1}x_n = \sum\limits_{n=k}^{N+k-1}x_n, \forall k
\end{equation*}

e

\begin{equation*}
	\sum\limits_{n=0}^{N-1}x_ny_{r-n} = \sum\limits_{n=k}^{N+k-1}x_ny_{r-n}, \forall k, \forall r
\end{equation*}

\underline{\textbf{Teo 4.2.1}}: Sejam $x,y,w\in\mathbb{C}^N$. Então

\begin{enumerate}
	\item Linearidade: $x*(ay + bw) = a(x*y) + b(x*w)$
	\item Comutatividade: $x*y = y*x$
	\item Formulação matricial: se $w = x*y$, então $w = M_yx$, onde
	\begin{equation*}
		M_y = \begin{pmatrix}
			y_0 & y_{N-1} & \dots & y_1 \\
			y_1 & y_0 & \dots & y_2 \\
			\vdots & \vdots & \ddots & \vdots \\
			y_{N-1} & y_{N-2} & \dots & y_0 \\
		\end{pmatrix}
	\end{equation*}
	Ou seja, $(M_y)_{k,m} = y_{k-m}$ e $M_y$ é chamada matriz circulante de $y$. Além disso $M_xM_y = M_{x*y}$
	\item Associatividade: $x*(y*w) = (x*y)*w$
	\item Periodicidade: se $x$ e $y$ são tratados por extensão periódica, então
	\begin{equation*}
		w_n = \sum\limits_{k=0}^{N-1}x_k y_{n-k}
	\end{equation*}
	também está definido $\forall n\in\mathbb{Z}$ e 
	\begin{equation*}
		w_n = w_{n\mod N}
	\end{equation*}
\end{enumerate}

\underline{Prova}:

\begin{enumerate}
	\item exercício.
	\item Seja $w = x*y$ onde $w_n = \sum\limits_{k=0}^{N-1}x_ky{n-k}$. Então
	\begin{align*}
		w_n &= \sum\limits_{k=0}^{N-1}x_ky_{n-k} = \sum\limits_{l=n}^{n-(N-1)}x_{n-l}y_l \\
		&= \sum\limits_{l=0}^{-(N-1)}x_{n-l}y_l \\
		&= \sum\limits_{l=N-1}^0 x_{n-l}y_l \\
		&= \sum\limits_{l=0}^{N-1}x_{n-l}y_l
	\end{align*}
	a última expressão corresponde à definição de $y*x$, logo $w = x*y = y*x$.
	\item Já mostramos a construção da matriz, falta só $M_xM_y = M_{x*y}$. Note que:
	\begin{align*}
		(M_xM_y)_{k,m} &= \sum\limits_{l=0}^{N-1}(M_x)_{k,l}(M_y)_{l,m} \\
		&= \sum\limits_{l=0}^{N-1}x_{k-l}y_{l-m} \\
		\sum\limits_{r=0}^{N-1}x_{(k-r-m)}y_r \\
	\end{align*}
	note que $\sum\limits_{r=0}^{N-1}x_{(k-m)-r}y_r = w_{k-m}$ (que está definido para todo $k-m\in\mathbb{Z}$ - propriedade 5) onde $w = x*y$. Logo $(M_xM_y)_{k,m} = w_{k-m} = (x*y)_{k-m}$ e, portanto, $M_xM_y = M_{x*y}$.
	\item \begin{align*}
		x*(y*w) &= x*(M_yw) \\
		&= M_x(M_y w) \\
		&= (M_xM_y)w \\
		&= M_{(x*y)}w \\
		&= (x*y)*w
	\end{align*}
	\item É só usar a obs.2 (exercício 4.6).
\end{enumerate}

\subsection{Teorema da Convolução}

Se $x, y, w\in\mathbb{C}^N$ onde $w=x*y$, então

\begin{equation*}
	W_k = X_k Y_k, k= 0, 1, \dots, N-1
\end{equation*}

onde $X, Y, W\in\mathbb{C}^N$ são as DFT's de $x, y, w$, respectivamente.

\underline{\textbf{Prova}}: Pela DFT de $w$, temos

\begin{align*}
	W_k &= \sum\limits_{n=0}^{N-1}w_ne^{-i2\pi kn/N} \\
	&= \sum\limits_{n=0}^{N-1}\left(\sum\limits_{m=0}^{N-1}x_my_{n-m}\right)e^{-i2\pi kn/N} \\
	&= \sum\limits_{m=0}^{N-1}\sum\limits_{r=-m}^{N-1-m}x_my_re^{-i2\pi k(r+m)/N} \\
	&= \sum\limits_{m=0}^{N-1}\sum\limits_{r=-m}^{N-1-m}x_my_re^{-i2\pi kr/N}e^{-i2\pi km/N} \\
	&= \left(\sum\limits_{m=0}^{N-1}x_me^{-i2\pi km/N}\right)\left(\sum\limits_{r=-m}^{N-1-m}y_re^{-i2\pi kr/N}\right) \\
	&= X_kY_k
\end{align*}

\underline{\textbf{Observação}}: considere um filtro definido por um vetor de coeficientes $h\in\mathbb{C}^N$ com saída $y=x*h\in\mathbb{C}^N$:

\begin{center}
	\includegraphics[width=0.3\textwidth]{images/teoremaConvolucao.pdf}
\end{center}

Quando $x=E_{N,k}$, temos $X = (0, 0, \dots, 0, N, 0, \dots, 0)'$.

Pelo teorema da convolução

\begin{equation*}
	Y = X\cdot H = (0, 0, \dots, 0, N\cdot H_k, 0, \dots, 0)'
\end{equation*}

Esta saída no domínio do tempo é

\begin{equation*}
	y = \frac{1}{N}\sum\limits_{r=0}^{N-1}Y_r E_{N,r} = H_kE_{N,k}
\end{equation*}

Mas $y = x*h = M_h x$, logo

\begin{equation*}
	M_hE_{N,k} = H_kE_{N,k}
\end{equation*}

Esta expressão mostra que os vetores $E_{N,k}$ são autovetores do operador $M_h$ associado ao filtro, com correspondentes autovalores dados por $H_k$.

\begin{center}
	\includegraphics[width=0.6\textwidth]{images/convolucaoAutovetores.pdf}
\end{center}

Cada entrada $E_{N,k}$ é processada pelo filtro produzindo uma saída

\begin{align*}
	y &= H_kE_{N,k} \\
	&= |H_k|e^{i(\angle H_k)}\begin{pmatrix}
		\vdots \\ e^{i2\pi kn/N} \\ \vdots
	\end{pmatrix} \\
	&= |H_k|\begin{pmatrix}
		\vdots \\ e^{i(2\pi kn/N + \angle H_k)} \\ \vdots
	\end{pmatrix} \\
	&= |H_k|\begin{pmatrix}
		\vdots \\ e^{i2\pi k\left(\frac{n}{N} + \angle \frac{H_k}{2\pi k}\right)} \\ \vdots
	\end{pmatrix}
\end{align*}

Onde $\angle H_k$ é o ângulo de $H_k$. No caso particular em que $\angle H_k = \alpha k$ (resposta linear em fase), teremos

\begin{equation*}
	y = |H_k|\begin{pmatrix}
		\vdots \\ e^{i2\pi k\left(\frac{n}{N} + \angle \frac{\alpha}{2\pi}\right)} \\ \vdots
	\end{pmatrix}
\end{equation*}

Como $\frac{\alpha}{2\pi}$ não depende da frequência, um filtro com resposta em fase linear preservará as relações de fase entre componentes senoidais diversos em um sinal de entrada geral (todas as componentes senoidais avançam os mesmos $\frac{\alpha}{2\pi}$ segundos).

No exemplo do filtro da média,

\begin{equation*}
	M_hE_{N,k} = \cos(2\pi k/N) e^{-i2\pi k\left(\frac{1}{2}\right)/N}E_{N,k}
\end{equation*}

logo $\angle H_k = -2\pi k\left(\frac{1}{2}\right)/N$ (resposta linear em $k$), com atraso correspondente a $\frac{-2\pi\left(\frac{1}{2}\right)/N}{2\pi} = \frac{\left(\frac{1}{2}\right)}{N}$ (atraso de grupo de $\frac{1}{2}$ amostra).

\textbf{Resposta ao impulso}: $h\in\mathbb{C}^N$ associado à equação $y=x*h$. Seja

\begin{equation*}
	\delta = \left\{\begin{array}{ll}
		1 & \text{ se }n=0 \\
		0 & \text{ c.c.} \\
	\end{array}\right.
\end{equation*}

Considere $y = h*\delta$:

\begin{align*}
	y_n &= \sum\limits_{k=0}^{N-1}h_k\delta_{n-k} \\
	&= h_n
\end{align*}

Ou seja, $\delta$ é o elemento neutro da operação de convolução.

\textbf{Resposta em Frequência}: $H = DFT(h)\in\mathbb{C}^N$

\begin{itemize}
	\item resposta em magnitude: $|H_k|$
	\item resposta em fase: $\angle H_k$
\end{itemize}

\subsection{Desenho de filtros}

Em princípio, podemos definir arbitrariamente qualquer resposta em frequência $H\in\mathbb{C}^N$ desejada, obtendo a resposta ao impulso pela expressão $h = IDFT(H)$. Entretanto

\begin{enumerate}
	\item Se $H\in\mathbb{C}^N$ é qualquer $h$ pode não ser real, e com isso $x*h$ também pode não ser real. Lembrando que
	\begin{equation*}
		h\in\mathbb{R}^N \Leftrightarrow H_k = \overline{H_{-k}}
	\end{equation*}
	temos que a solução aqui é definir $H_k$ arbitrariamente para $k=0, 1, \dots, \frac{N}{2}$ e definindo $H_{N-k} = H_{-k} = H_{k}, k=\frac{N}{2}+1, \dots, N-1$
	\item Normalmente o vetor $h = IDFT(H)$ pode conter muitos coeficientes diferentes de 0, tornando o filtro computacionalmente caro.
	
	\textbf{obs}: custo da convolução
	\begin{enumerate}[(I)]
		\item $y_n = \sum\limits_{k=0}^{N-1}x_kh_{n-k}, n = 0, \dots, N-1$ (custo O($N^2$))
		\item $y = IDFT(Y)$ onde $Y_k = X_kH_k, k = 0, 1, \dots, N-1$ (custo O($N\log N$))
		\item se $\#h = \#\{h_n\neq 0\}$ é O(1), então o cálculo direto
		\begin{align*}
			y_n &= \sum\limits_{k=0}^{N-1}x_kh_{n-k} \\
			&= \sum\limits_{k\in\{m|h_m\neq 0\}}x_kh_{n-k}
		\end{align*}
		cujo custo é O($N$). Filtros computacionalmente mais eficientes.
	\end{enumerate}
\end{enumerate}

Uma ``solução'' neste caso (para deixar o filtro mais eficiente) é zerar componentes de $h$ que satisfaçam $|h_m| < \epsilon$ para algum $\epsilon > 0$ escolhido.

\underline{\textbf{Exemplo}}: filtro passa alta com resposta em frequência da forma

\begin{center}
	\includegraphics[width=0.4\textwidth]{images/passaAlta.pdf}
\end{center}

\subsection{Seção 4.4 - Convolução e filtragem 2D}

Dada uma imagem $x$ queremos calcular a imagem $y$ onde $y_{i,j}$ é uma combinação linear dos pixels de $x$ em relação à coordenada $(i,j)$.

\underline{\textbf{Def}}: A convolução de duas imagens $x, h\in \mathcal{M}_{M,N}(\mathbb{C})$ é a imagem $y=x*h\in\mathcal{M}_{M,N}(\mathbb{C})$ dada por 

\begin{align*}
	y_{i,j} &= \sum\limits_{r=0}^{M-1}\sum\limits_{s=0}^{N-1}h_{r,s}x_{i-r,j-s} \\
	&= \sum\limits_{r=0}^{M-1}\sum\limits_{s=0}^{N-1}x_{r,s}h_{i-r,j-s} \\
\end{align*}

\underline{\textbf{Obs}}: considera-se aqui a extensão periódica de todas as imagens, ou seja, $w_{i,j} = w_{i\mod M, j\mod N}$.

\subsubsection{Propriedades da convolução}

\begin{enumerate}
	\item Linearidade
	\begin{equation*}
		x*(\alpha y + \beta z) = \alpha x*y + \beta x*z
	\end{equation*}
	\item Comutatividade
	\begin{equation*}
		x*y = y*x
	\end{equation*}
	\item Associatividade
	\begin{equation*}
		x*(y*z) = (x*y)*z
	\end{equation*}
	\item Representação Matricial (Exercício 4.24):
	
	Se $h\in\mathcal{M}_{M,N}(\mathbb{C})$ é da forma $h = lc^T$ com $l\in\mathbb{C}^M$ e $c\in\mathbb{C}^N$, então $x*h = M_l x M_c^T$.
\end{enumerate}

\subsubsection{Teorema da convolução}

Se $x, h, y\in\mathcal{M}_{M,N}(\mathbb{C})$ e $y=x*h$, então

\begin{equation*}
	Y_{k,l} = X_{k,l}H_{k,l}, \forall k=0, 1, \dots, M-1, \forall l = 0, 1, \dots, N-1
\end{equation*}

onde $X, H, Y\in\mathcal{M}_{M,N}(\mathbb{C})$ são as DFT's de $x, h$ e $y$ respectivamente.

\underline{\textbf{Obs}}: pela periodicidade de $x$ e $h$, temos 

\begin{equation*}
	\sum\limits_{r=0}^{M-1}\sum\limits_{s=0}^{N-1} x_{r,s}h_{i-r,j-s} = \sum\limits_{r=a}^{a+M-1}\sum\limits_{s=b}^{b+N-1} x_{r,s}h_{i-r,j-s} = \sum\limits_{r=0}^{M-1}\sum\limits_{s=0}^{N-1} x_{r-a,s-b}h_{i-r+a,j-s+b}, \forall a, b
\end{equation*}

\subsubsection{Filtragem em imagens}

\begin{center}
	\includegraphics[width=0.4\textwidth]{images/teoremaConvolucao2d.pdf}
\end{center}

\subsubsection{Efeito de um filtro de convolução em uma exponencial complexa}

\begin{center}
	\includegraphics[width=0.4\textwidth]{images/convolucaoExpComplex.pdf}
\end{center}

Pela equação de síntese

\begin{align*}
	y &= \frac{1}{MN}\sum\limits_{r=0}^{M-1}\sum\limits_{s=0}^{N-1} Y_{r,s}E_{M,N,r,s} \\
	&= \frac{1}{MN}Y_{k,l}E_{M,N,k,l} \\
	&= \frac{1}{MN}(MNH_{k,l})x \\
	&= H_{k,l}x
\end{align*}

Se denotamos a convolução $x*h$ como um operador linear

\begin{equation*}
	\varphi_h: \mathcal{M}_{M,N}(\mathbb{C})\rightarrow\mathcal{M}_{M,N}(\mathbb{C})
\end{equation*}

definida como $\varphi_h(x) = x*h$, então podemos dizer que

\begin{equation*}
	E_{M,N,k,l}\in\mathcal{M}_{M,N}(\mathbb{C})
\end{equation*}

são autovetores de $\varphi_h, \forall k, l$, com autovalores dados por $H_{k,l}$.

\underline{\textbf{Exemplo}}: filtro da média 2D com 9 ``taps''

\begin{align*}
	y_{i,j} &= \frac{1}{9}(x_{i-1,j-1} + x_{i-1,j} + x_{i-1,j+1} + x_{i,j-1} + x_{i,j} + x_{i,j+1} + x_{i+1,j-1} + x_{i+1,j} + x_{i+1,j+1})
\end{align*}

ou equivalentemente

\begin{equation*}
	y = x*h
\end{equation*}

onde $h = \frac{1}{9}\begin{pmatrix}
	0 & \dots & \dots & 0 & \dots & \dots & 0 \\
	\vdots & & \vdots & 0 & \vdots & & \vdots \\
	\vdots & \dots & 1 & 1 & 1 & \dots & \vdots \\
	0 & \dots & 1 & 1 & 1 & \dots & 0 \\
	\vdots & \dots & 1 & 1 & 1 & \dots & \vdots \\
	\vdots & & \vdots & 0 & \vdots & & \vdots \\
	0 & \dots & \dots & 0 & \dots & \dots & 0 \\
\end{pmatrix}$.

A DFT de $h$ é

\begin{align*}
	H_{k,l} &= \sum\limits_{r=0}^{M-1}\sum\limits_{s=0}^{N-1}h_{r,s}e^{-i2\pi(kr/M + ls/N)} \\
	&= \frac{1}{9}(e^{-i2\pi(-k/M - l/N)} + e^{-i2\pi(-k/M)} + e^{-i2\pi(-k/M + l/N)} \\
	&+ e^{-i2\pi(-l/N)} + 1 + e^{-i2\pi(l/N)} \\
	&+ e^{-i2\pi(k/M - l/N)} + e^{-i2\pi k/M} + e^{-i2\pi(k/M + l/N)})
\end{align*}

\subsubsection{Filtro para detecção de bordas}

\begin{center}
	\includegraphics[width=0.4\textwidth]{images/detecBordas.pdf}
\end{center}

Daí o gradiente $\nabla f = \begin{pmatrix}
	\frac{\partial f}{\partial x} \\ \frac{\partial f}{\partial y}
\end{pmatrix}$ pode ser utilizado para calcular bordas como $\|\nabla f\| >> 0$. Da definição de derivada temos:

\begin{align*}
	\frac{\partial f(x,y)}{\partial x} &= \lim\limits_{\partial\rightarrow 0}\frac{f(x+\partial, y)-f(x,y)}{\partial} \\
	&= \approx \frac{f(x+\Delta, y)-f(x,y)}{\Delta_x} \\
	\frac{\partial f(x,y)}{\partial x} &= \lim\limits_{\partial\rightarrow 0}\frac{f(x+\partial, y)-f(x,y)}{\partial} \\
	&= \approx \frac{f(x+\Delta, y)-f(x,y)}{\Delta_x}
\end{align*}

Onde $\Delta_x$ é a resolução de amostragem horizontal e $\Delta_y$ é a resolução de amostragem vertical.

\begin{itemize}
	\item \textbf{bordas horizontais}: $y_{i,j} = x_{i+1,j}-x_{i,j}$
	\item \textbf{bordas verticais}: $z_{i,j} = x_{i,j+1}-x_{i,j}$
\end{itemize}

que podem ser implementados por convoluções. Um indicador de bordas ``genérico'' poderia ser construído como

\begin{equation*}
	B_{i,k} = \sqrt{|y_{i,j}|^2 + |z_{i,j}|^2}
\end{equation*}

\subsubsection{Filtros infinitos e bi-infinitos}

Vamos considerar $f(t)$ amostrada em $t_k = a + kt$. Se $k\in\mathbb{N}$, temos $x = (x_0, x_1, \dots)$. Se $k\in\mathbb{Z}$, temos $x = (\dots, x_{-1}, x_0, x_1, \dots)$.

\underline{\textbf{$L^2(\mathbb{N})$}}

\underline{\textbf{Def}}: O espaço vetorial $L^2(\mathbb{N})$ consiste dos elementos $x = (x_0, x_1, \dots)$ onde $x_k\in\mathbb{C}, \forall k\in\mathbb{N}$, tal que

\begin{equation*}
	\sum\limits_{k=0}^{\infty}|x_k|^2 < \infty
\end{equation*}

Lembrando

\begin{equation*}
	\sum\limits_{k=0}^\infty a_k = \lim\limits_{n\rightarrow\infty}\sum\limits_{k=0}^na_k
\end{equation*}

Será que dados $x, y\in L^2(\mathbb{N})$,

\begin{equation*}
	(x, y) = \sum\limits_0^\infty x_k\overline{y_k}
\end{equation*}

define um produto interno em $L^2(\mathbb{N})$?

As propriedades do produto interno são fáceis de mostrar. Vamos mostrar a convergência de $(x, y)$. Vamos usar:

\begin{itemize}
	\item $|w||z| \leq \frac{1}{2}(|w|^2 + |z|^2)$
	\item $|Re(zw)| \leq |z||w|$
	\item $|Im(zw)| \leq |z||w|$
	\item $\sum\limits_{k=0}^\infty x_k\overline{y_k}$ converge se e só se $\sum\limits_{k=0}^\infty Re(x_k\overline{y_k})$ e $\sum\limits_{k=0}^\infty Im(x_k\overline{y_k})$ convergem
\end{itemize}

Então

\begin{align*}
	\sum\limits_{k=0}^\infty |Re(x_k\overline{y_k})| &\leq \sum\limits_{k=0}^\infty |x_k||\overline{y_k}| \\
	&\leq \frac{1}{2}\sum\limits_{k=0}^\infty (|x_k|^2 + |y_k|^2) \\
	&= \frac{1}{2}\left[\sum\limits_{k=0}^\infty |x_k|^2 + \sum\limits_{k=0}^\infty|y_k|^2\right] \\
	&< \infty
\end{align*}

A demonstração para $\sum\limits_{k=0}^\infty |Im(x_k,\overline{y_k})|$ é análoga e portanto

\begin{equation*}
	(x, y) = \sum\limits_{k=0}^\infty x_k\overline{y_k} < \infty
\end{equation*}

A norma associada é

\begin{equation*}
	\|x\| = \sqrt{(x,x)}
\end{equation*}

A demonstração em $L^2(\mathbb{Z})$ é análoga.

\subsubsection{Integral de Riemann}

\underline{\textbf{Def}}: A integral de Riemann de $f(x)$ no intervalo $[a,b]$ é:

\begin{equation*}
	\int\limits_a^b f(x)dx = \lim\limits_{\max(\Delta x)\rightarrow\infty}\sum\limits_{k=1}^Nf(x^*_k)\Delta x_k
\end{equation*}

onde $x^*_k$ é um valor arbitrário no intervalo $[x_k, x_k+\Delta x_k]$. Se todos os $N$ intervalos são iguais entre $-1/2$ e $1/2$ temos, para uma função $X(f)$:

\begin{equation*}
	\lim\limits_{N\rightarrow\infty}\sum\limits_{k=1}^N X(f^*_k)\frac{1}{N} = \int\limits_{-1/2}^{1/2} X(f)df
\end{equation*}

Se escrevermos

\begin{equation*}
	X(f) = \sum\limits_{-\infty}^\infty x_ke^{-2\pi ikf}
\end{equation*}

$X(f)$ é combinação linear de funções $e^{-2\pi ikf}$. Então, no caso finito,

\begin{equation*}
	x_k = \frac{1}{N}\sum\limits_{m=0}^{N-1}X_me^{2\pi ikm/N}
\end{equation*}

podemos tomar o limite de $N\rightarrow\infty$, temos

\begin{align*}
	x_k &= \lim\limits_{N\rightarrow\infty}\sum\limits_{m=0}^{N-1}X_me^{2\pi ikm/N}\frac{1}{N} \\
	&= \int\limits_{-1/2}^{1/2}X(f)e^{2\pi ikf}df
\end{align*}

com $X_m = X\left(\frac{m}{N}\right)$.

\underline{\textbf{Def}}: Para um elemento $x\in L^2(\mathbb{Z})$, a transformada de Fourier de tempo discreto (DTFT) é a função $X(f)$ definida para $-\frac{1}{2}\leq f\leq \frac{1}{2}$ dada por

\begin{equation*}
	X(f) = \sum\limits_{k=-\infty}^{\infty}x_ke^{-2\pi ikf}
\end{equation*}

A transformada inversa (IDTFT) é

\begin{equation*}
	x_k = \int\limits_{-\frac{1}{2}}^{\frac{1}{2}}X(f)e^{2\pi ikf}df
\end{equation*}

Se $x$ foi obtido pela amostragem de uma função $x(t)$ da forma $x_k = x(kt)$ a frequência de amostragem é 1Hz e a frequência de Nyquist é $\frac{1}{2}$Hz. Se $x$ foi obtido amostrando $x_k = x(kT)$, então, a frequência de amostragem é $\frac{1}{T}$Hz e a frequência de Nyquist é $\frac{1}{2T}$Hz.

Nesse caso, podemos fazer $\tilde{f} = \frac{f}{T}$ e $\tilde{X}(\tilde{f}) = X(\tilde{f}T)$ e assim podemos escrever a IDTFT como

\begin{equation*}
	x_k = \int\limits_{-\frac{1}{2T}}^{\frac{1}{2T}}\tilde{X}(\tilde{f})e^{2\pi ik\tilde{f}T}Td\tilde{f}
\end{equation*}

e nesse caso, a DTFT é

\begin{equation*}
	\tilde{X}(\tilde{f}) = \sum\limits_{k=-\infty}^\infty x_ke^{-2\pi ik\tilde{f}T}
\end{equation*}

Para $x\in L^2(\mathbb{N})$, podemos definir $x'$ em $L^2(\mathbb{Z})$ no qual $x_k' = x_k$ se $k\geq 0$ e $x_k' = 0$ se $k < 0$. A DTFT em $L^2(\mathbb{N})$ pode ser calculada como

\begin{equation*}
	\sum\limits_{k=0}^\infty x_ke^{-2\pi ikf}
\end{equation*}

e a inversa

\begin{equation*}
	x_k = \int\limits_{-\frac{1}{2}}^\frac{1}{2}X(f)e^{2\pi ikf}df
\end{equation*}

\underline{\textbf{Def}}: A convolução de dois elementos $x,y\in L^2(\mathbb{Z})$ é definida como:

\begin{equation*}
	w_m = (x*y)_m = \sum\limits_{k=-\infty}^{\infty}x_ky_{(m - k)}
\end{equation*}

Mesmo que $\|x\| < \infty$ e $\|y\| < \infty$, pois $x,y\in L^2(\mathbb{Z})$ não necessariamente $w = x*y$ pertence a $L^2(\mathbb{Z})$. É fato que $w\in L^\infty (\mathbb{Z})$, ou seja, existe $M$ tal que $w_m < M, \forall m\in\mathbb{Z}$.

Se $w = x*y$, então:

\begin{align*}
	w_0 &= \sum\limits_{k=-\infty}^\infty x_ky_{-k} \\
	w_1 &= \sum\limits_{k=-\infty}^\infty x_ky_{1-k} \\
	w_m &= \sum\limits_{k=-\infty}^\infty x_ky_{m-k}
\end{align*}

A convolução em $L^2(\mathbb{N})$ é definida como

\begin{equation*}
	(x*y)_m = \sum\limits_{k=0}^{\infty}x_ky_{(m - k)}
\end{equation*}

com $x_k = 0$ para $k < 0$ e $x_{(m - k)} = 0$ para $k > m$.

\underline{\textbf{Prop. 4.5.1}}: Sejam $x, y, w$ todos em $L^2(\mathbb{N})$ ou $L^2(\mathbb{Z})$, então,

\begin{enumerate}
	\item Comutatividade: $x*y = y*x$
	\item Associatividade: $(x*y)*w = x*(y*w)$
	\item Linearidade: $x*(ay + bw) = a(x*y) = b(x*w)$
\end{enumerate}

\underline{\textbf{Teorema 4.5.1}}: Sejam $x$ e $y\in L^2(\mathbb{Z})$ com transformadas $X(f)$ e $Y(f)$. Se $w = x*y$, então a transformada de Fourier de tempo discreto de $w$ é

\begin{equation*}
	W(f) = X(f)Y(f)
\end{equation*}

\textbf{Prova}:

\begin{align*}
	X(f) &= \sum\limits_{k=-\infty}^\infty x_ke^{-2\pi ikf} \\
	&= \sum\limits_{k=-\infty}^\infty x_kz^{-k} \\
	Y(f) &= \sum\limits_{k=-\infty}^\infty y_kz^{-k} \\
\end{align*}

onde $z = e^{2\pi if}$ e assim $z^{-k} = (e^{2\pi if})^{-k} = e^{-2\pi ikf}$. Então,

\begin{align*}
	X(f)Y(f) = \left[\sum\limits_{k=-\infty}^\infty x_kz^{-k}\right]\left[\sum\limits_{k=-\infty}^\infty y_kz^{-k}\right] \\
	&= \sum\limits_{k=-\infty}^\infty\sum\limits_{m=-\infty}^\infty x_kz^{-k}y_mz^{-m} \\
	&= \sum\limits_{k=-\infty}^\infty\sum\limits_{m=-\infty}^\infty x_ky_mz^{-k-m} (*)
\end{align*}

Defina $s = k+m$ ($m = s-k$), então

\begin{align*}
	(*) &= \sum\limits_{k=-\infty}^\infty\sum\limits_{s=-\infty}^\infty x_ky_{s-k}z^{-s} \\
	&= \sum\limits_{s=-\infty}^\infty\left[ \sum\limits_{k=-\infty}^\infty x_ky_{s-k}\right] z^{-s} \\
	&= \sum\limits_{s=-\infty}^\infty (x*y)_s z^{-s} \\
	&= \sum\limits_{s=-\infty}^\infty w_s z^{-s} \\
	&= \sum\limits_{s=-\infty}^\infty w_s e^{-2\pi isf} \\
	&= W(f)
\end{align*}

\qed

\underline{\textbf{Exemplo 4.5}}: Seja $h\in L^2(\mathbb{Z})$ com componentes $h_0 = \frac{1}{2}, h_1 = \frac{1}{2}$ e $h_k = 0$ para $k\neq 0, 1$. $h = \left(\dots, 0, \frac{1}{2}, \frac{1}{2}, 0, 0, \dots, \right)$. Considerando $y = x*h$ para $x\in L^2(\mathbb{Z})$ então

\begin{equation*}
	Y(f) = X(f)H(f)
\end{equation*}

onde

\begin{align*}
	H(f) &= \sum\limits_{k=-\infty}^\infty h_ke^{-2\pi ikf} \\
	&= \frac{1}{2}1 + \frac{1}{2}e^{-2\pi if} \\
	&= e^{\pi if}\frac{1}{2}(e^{\pi if} + e^{-\pi if}) \\
	&= e^{\pi if}\cos(\pi f) \\
	|H(f)| &= \cos(\pi f)
\end{align*}

\subsubsection{Transformada $z$}

Dada $x\in L^2(\mathbb{Z})$, sua transformada $z$ é

\begin{equation*}
	X(z) = \sum\limits_{n=-\infty}^{\infty}x_n z^{-n}
\end{equation*}

Duas interpretações:

\begin{enumerate}
	\item Formal/Algébrico: $z$ é interpretado como um ``literal'', sujeito a manipulações algébricas usuais, mas não é substituído por valores numéricos
	\item Analítica: $z$ é interpretado como uma variável complexa, e $X(\cdot)$ é uma ``função de $\mathbb{C}$ em $\mathbb{C}$''
\end{enumerate}

\underline{\textbf{Exemplos:}}

\begin{enumerate}
	\item (ex. 4.6) Seja $x\in L^2(\mathbb{Z})$ com $x_{-1} = 2, x_0 = 1, x_1 = 3, x_2 = -2$ e $x_k = 0, \forall k\neq -1, 0, 1, 2$. A transformada $z$ de $x$ é
	\begin{equation*}
		X(z) = 2z^1 + 1z^0 + 3z^{-1} - 2z^{-2}
	\end{equation*}
	\begin{itemize}
		\item interpretação formal: ``recodificação'' da sequência
		\item interpretação analítica: $X$ é uma função de $\mathbb{C}\\\{0\}\rightarrow\mathbb{C}$.
	\end{itemize}
	\item (ex. 4.7) $x\in L^2(\mathbb{Z})$ onde $x_0 = 0$ e $x_k = \frac{1}{|k|}, \forall k\neq 0$. A transformada $z$ de $x$ é
	\begin{equation*}
		X(z) = \dots + \frac{1}{3}z^3 + \frac{1}{2}z^2 + z^1 + 0 + z^{-1} + \frac{1}{2}z^{-2} + \frac{1}{3}z^{-3} + \dots
	\end{equation*}
	Do ponto de vista analítico, temos:
	\begin{itemize}
		\item $|z| < 1$: a soma dos expoentes positivos converge, mas a soma dos termos com expoente negativo diverge
		\item $|z| > 1$: a soma dos expoentes positivos diverge, mas a soma dos termos com expoente negativo converge
	\end{itemize}
	Na melhor das hipóteses, a interpretação analítica permite considerar $X(z)$ como função com domínio $D\subseteq\{z | |z| = 1\}$
	\item (ex. 4.8) $x\in L^2(\mathbb{Z})$ com $x_k = \left\{\begin{array}{ll}
		\frac{1}{2^k}, & k\geq 0 \\
		0, & k < 0
	\end{array}\right.$ cuja transformada $z$ é
	\begin{equation*}
		X(z) = \sum\limits_{k=0}^{\infty}\frac{1}{2^k}z^{-k} = \frac{1}{(2z)^k}
	\end{equation*}
	Se $|\frac{1}{2z}| < 1$ ($|z| > \frac{1}{2}$) então
	\begin{equation*}
		X(z) = \frac{1}{1-\frac{1}{2z}}
	\end{equation*}
	($X: D\rightarrow\mathbb{C}$ com $D = \{z||z|<\frac{1}{2}\}$). Considere agora $x\in L^2(\mathbb{Z})$ onde $x_k = \left\{\begin{array}{ll}
		0, & k\geq 0 \\
		\frac{-1}{2^k}, & k < 0
	\end{array}\right.$ com transformada $z$
	\begin{align*}
		X(z) &= \sum\limits_{k=-\infty}^{-1}-\frac{1}{2^k}z^{-k} \\
		&= \frac{-1}{(2z)^k} \\
		&= -2z - 4z^2 - 8z^3 - \dots
	\end{align*}
	Se $|2z| < 1$ ($|z| < \frac{1}{2}$)
\end{enumerate}

\subsubsection{Convolução e transformada $z$}

Se $x, y\in L^2(\mathbb{Z})$ e $w = x*y\in L^\infty (\mathbb{Z})$, se $X(z), Y(z)$ e $W(z)$ são as transformadas $z$ de $x, y$ e $w$ respectivamente, então

\begin{equation*}
	W(z) = X(z)Y(z), \forall z\text{ nos domínios de }X, Y\text{ e }W
\end{equation*}

A prova deste resultado é idêntica à do teorema 4.5.1.

Levando em consideração que

\begin{align*}
	X(f) &= \sum\limits_{-\infty}^{\infty}x_ne^{-i2\pi fn} \\
	&= \sum\limits_{-\infty}^{\infty}x_n(e^{i2\pi f})^{-n} \\
	&= X(z)\text{ para }z = e^{i2\pi f}
\end{align*}

e analogamente para $Y$ e $W$, podemos entender o teorema 4.5.1 como um caso particular do teorema da convolução para transformadas $z$ usando o mapeamento $z\leftrightarrow e^{i2\pi f}$.

Relação entre convolução (circular) em $\mathbb{C}^N$ e convolução (linear) em $L^2(\mathbb{Z})$.

\underline{\textbf{Exemplo}}: $x=\begin{pmatrix}x_0\\x_1\\x_2\end{pmatrix}\in\mathbb{C}^3$ e $y=\begin{pmatrix}y_0\\y_1\\y_2\end{pmatrix}\in\mathbb{C}^3$ e suas extensões $\tilde{x}, \tilde{y}\in L^2(\mathbb{C})$ fazendo $\tilde{x}_k = \tilde{y}_k = 0, \forall k\neq 0, 1, 2$. Seja $w = x*y\in\mathbb{C}^3$

\begin{align*}
	w_0 &= x_0y_0 + x_1y_2 + x_2y_1 \\
	w_1 &= x_0y_1 + x_1y_0 + x_2y_2 \\
	w_2 &= x_0y_2 + x_1y_1 + x_2y_0
\end{align*}

Seja $\tilde{w} = \tilde{x}*\tilde{y}\in L^2(\mathbb{Z})$

\begin{align*}
	\tilde{w}_0 &= x_0y_0 + x_10 + x_20 = x_0y_0 \\
	\tilde{w}_1 &= x_0y_1 + x_1y_0 + x_20 = x_0y_1 + x_1y_0 \\
	\tilde{w}_2 &= x_0y_2 + x_1y_1 + x_2y_0 \\
	\tilde{w}_3 &= x_00 + x_1y_2 + x_2y_1 = x_1y_2 + x_2y_1 \\
	\tilde{w}_4 &= x_00 + x_10 + x_2y_2 = x_2y_2 \\
	\tilde{w}_k &= 0 \forall k\neq 0, 1, 2, 3, 4
\end{align*}

\subsubsection{Polinômios de Laurent}

\begin{equation*}
	p(z) = \sum\limits_{k=-m}^n a_kz^k
\end{equation*}

Definimos

% TODO Verificar o limite -k\mod N ou k\mod N?
\begin{equation*}
	p(z)\mod z^{-N} = \sum\limits_{k=0}^{N-1}\left(\sum\limits_{l=-k\mod N}a_l\right)z^{-k}
\end{equation*}

\underline{\textbf{Exemplo}}: $p(z) = 2z^{-2} - z^{-1} + 3 + \frac{1}{2}z + z^2 + 5z^3$ tomando $N=3$, temos

\begin{align*}
	p(z)\mod z^{-3} &= (3+5)z^0 + (-1 + 1)z^{-1} + \left(2 + \frac{1}{2}\right)z^{-2} \\
	&= 8z^0 + \frac{5}{2}z^{-2}
\end{align*}

\underline{\textbf{Teorema 4.5.3}}: Considere $x, y, w\in\mathbb{C}^N$ onde $w=x*y$ (convolução circular), e considere 

\begin{align*}
	X(z) &= \sum\limits_{k=0}^{N-1}x_kz^{-k} \\
	Y(z) &= \sum\limits_{k=0}^{N-1}y_kz^{-k} \\
	W(z) &= \sum\limits_{k=0}^{N-1}w_kz^{-k}
\end{align*}

Então $W(z) = X(z)Y(z)\mod z^{-N}$

\underline{\textbf{Prova}}: Considere $w_k = e^{i2\pi k/N}$, para $k = 0, 1, \dots, N-1$. Note que as DFT's de $x, y$ e $w$ satisfazem

\begin{align*}
	X_k &= X(w_k) \\
	Y_k &= Y(w_k) \\
	W_k &= W(w_k)
\end{align*}

Pelo teorema da convolução em $\mathbb{C}^N$ temos $W_k = X_kY_k, \forall k = 0, 1, \dots, N-1$ ou $W(w_k) = X(w_k)Y(w_k)$. Definindo $P(z) = X(z)Y(z)\mod z^{-N}$, temos

\begin{align*}
	P(w_k) &= X(w_k)Y(w_k)\mod z^{-N} \\
	&= \left[\sum\limits_{l=0}^{N-1}x_lw_k^l\right]\left[\sum\limits_{m=0}^{N-1}y_mw_k^m\right]\mod z^{-N} \\
	\left[\sum\limits_{l=0}^{N-1}\sum\limits_{m=0}^{N-1}x_ly_mw_k^{l+m}\right]\mod z^{-N}
\end{align*}

Por outro lado,

\begin{equation*}
	X(w_k)Y(w_k) = \sum\limits_{l=0}^{N-1}\sum\limits_{m=0}^{N-1}x_ly_mw_k^{l+m}
\end{equation*}

Note que se $\tilde{l},\tilde{m}$ e $\hat{l},\hat{m}$ satisfazem $\tilde{l} + \tilde{m} = \hat{l} + \hat{m}\mod N$, como $w_k^{\tilde{l}+\tilde{m}} = w_k^{\hat{l}+\hat{m}}$, os termos serão agrupados junto do expoente $w_k^{\left[(\tilde{l}+\tilde{m}\mod N)\right]}$. Esse argumento mostra que

\begin{equation*}
	W(w_k) = X(w_k)Y(w_k) = P(w_k), \forall k = 0, 1, 2, \dots, N-1
\end{equation*}

Como os polinômios de Laurent $P(z)$ e $W(z)$ ambos têm grau $N-1$ e coincidem em $N$ pontos ($w_0, w_1, \dots, w_{N-1}$), então necessariamente

\begin{equation*}
	P(z) = W(z), \forall z\in\mathbb{C}\\\{0\}
\end{equation*}

\underline{\textbf{Exemplo}}: Voltando ao exemplo em $\mathbb{C}^3$ com

\begin{align*}
	w_0 &= x_0y_0 + x_1y_2 + x_2y_1 \\
	w_1 &= x_0y_1 + x_1y_0 + x_2y_2 \\
	w_2 &= x_0y_2 + x_1y_1 + x_2y_0
\end{align*}

e

\begin{align*}
	\tilde{w}_0 &= x_0y_0 \\
	\tilde{w}_1 &= x_0y_1 + x_1y_0 \\
	\tilde{w}_2 &= x_0y_2 + x_1y_1 + x_2y_0 \\
	\tilde{w}_3 &= x_1y_2 + x_2y_1 \\
	\tilde{w}_4 &= x_2y_2 \\
	\tilde{w}_k &= 0 \forall k\neq 0, 1, 2, 3, 4
\end{align*}

As transformadas $z$ de $w$ e $\tilde{w}$ são $W(z) = w_0z^0 + w_1z^{-1} + w_2z^{-2}$ e $\tilde{W}(z) = \tilde{w}_0z^0 + \tilde{w}_1z^{-1} + \tilde{w}_2z^{-2} + \tilde{w}_3z^{-3} + \tilde{w}_4z^{-4}$. Note que

\begin{align*}
	\tilde{W}(z)\mod z^{-3} &= (\tilde{w}_0 + \tilde{w}_3)z^0 + (\tilde{w}_1 + \tilde{w}_4)z^{-1} + \tilde{w}_2z^{-2}
\end{align*}

regra geral

\begin{equation*}
	(\tilde{w}_k + \tilde{w}_{N+k}) = w_k, \forall k
\end{equation*}
