\section{Capítulo 2}

Sejam $x = \begin{pmatrix}x_0 \\ x_1 \\ \vdots \\ x_{N-1}\end{pmatrix}\in\mathbb{C}^N$ e $E_{N,k} = \begin{pmatrix}e^{i 2\pi k0/N} \\ e^{i 2\pi k1/N} \\ \vdots \\ e^{i 2\pi k(N-1)/N}\end{pmatrix}\in\mathbb{C}^N, k = 0, 1, \dots, N-1$. Como a base $\{E_{N,k}\}_{k=0, 1, \dots, N-1}$ é ortogonal, segue que

\begin{equation*}
	x = \sum\limits_{k=0}^{N-1}\alpha_k E_{N,k}
\end{equation*}

onde $\alpha_k = \frac{(x, E_{N,k})}{(E_{N,k}, E_{N,k})}$.

\textbf{Definimos} a Transformada Discreta de Fourier (DFT) do vetor $x\in\mathbb{C}^N$ como o vetor $X\in\mathbb{C}^N$ com componentes $X_k = (x, E_{N,k}) = \sum\limits_{n=0}^{N-1} x_ne^{-i2\pi kn/N} = E_{N,k}^*x$ (\textbf{Equação de Análise}, onde $^*$ é o operador Hermitiano, definido como $A^* = (\overline{A})^T$) de tal forma que $x = \frac{1}{N}\sum\limits_{k=0}^{N-1}X_k E_{N,k}$ (\textbf{Equação de Síntese ``vetorial''}), ou analogamente, $x_n = \frac{1}{N}\sum\limits_{k=0}^{N-1}X_ke^{i2\pi kn/N}$ (\textbf{Equação de Síntese}, \textbf{iDFT} - inversa de Transformada Discreta de Fourier). Na maioria das linguagens de programação é implementado dessa forma e por isso utilizamos o fator de correção. Por esse motivo é importante verificar como cada livro define a DFT, inclusive alguns definem um fator $\frac{1}{\sqrt{N}}$ em cada um dos lados para equilibrar o fator de escala.

\textbf{Observação}: no contexto da Análise de Fourier dizemos que $x\in\mathbb{C}^N$ associado à representação na \textbf{base canônica} está no \textbf{domínio do tempo}, ao passo que $x\in\mathbb{C}^N$ associado aos coeficientes da representação de $x$ na \textbf{base das funções exponenciais complexas} está no \textbf{domínio da(s) frequência(s)}.

\underline{\textbf{Exemplo}}: Considere o sinal analógico:

\begin{equation*}
	x(t) = 2\cos(2\pi 5t) + 0.8\sin(2\pi 12t) + 0.3\cos(2\pi 47t)
\end{equation*}

amostrado usando $N=128$ amostras no intervalo $[0,1)$:

\begin{equation*}
	x[n] = x(n/128), \text{ para }n=0, 1, \dots, 127
\end{equation*}

Aplicando a transformada de Fourier (DFT) em $x$ obtemos o vetor $X_k = \sum\limits_{n=0}^{127}x[n]e^{-i2\pi kn/128}$. Cuja equação de síntese é

\begin{equation*}
	x[n] = \frac{1}{128}\sum\limits_{k=0}^{127}X_ke^{i2\pi nk/128}
\end{equation*}

$\frac{X_k}{128}$ é o coeficiente da combinação linear da reconstrução de $x$ a partir das exponenciais complexas $E_{N,k}$ ($X_k$ depende do número de amostras). Note que vale para qualquer $k$, em particular os seguintes intervalos são possíveis: $k = 0, 1, \dots, N-1$ e $k = -\frac{N}{2}, \dots, 0, \dots, +\frac{N}{2}$.

\textbf{\underline{Energia do sinal}} ($\|x\|^2$):

\begin{align*}
	\|x\|^2 &= (x, x) \\
	&= (\frac{1}{N}\sum\limits_{k=0}^{N-1}X_k E_{N,k}, \frac{1}{N}\sum\limits_{l=0}^{N-1}X_l E_{N,l}) \\
	&= \frac{1}{N^2}\sum\limits_{k=0}^{N-1}\sum\limits_{l=0}^{N-1}X_k\overline{X_l}(E_{N,k}, E_{N,l}) \\
	&= \frac{1}{N}\sum\limits_{k=0}^{N-1}|X_k|^2
\end{align*}

\underline{\textbf{Exemplos}}:

O sinal quadrado

\begin{center}
\includegraphics[width=0.7\textwidth]{images/quadrado.pdf}
\end{center}

\begin{align*}
	X_k &= \sum\limits_{n = 0}^{N-1}x(n)e^{-i2\pi kn/N} \\
	&= \sum\limits_{n=0}^{R-1}e^{-i2\pi kn/N} \\
	&= \sum\limits_{n=0}^{R-1}(e^{-i2\pi k/N})^n \\
	&= \frac{1 - e^{-i2\pi kR/N}}{1 - e^{-i2\pi k/N}} \\
	|X_k| &= \frac{\sqrt{(1 - \cos(2\pi kR/N))^2 + \sin(2\pi kR/N)^2}}{\sqrt{(1-\cos(2\pi k/N))^2 + \sin(2\pi k/N)^2}} \\
	&= \frac{\sqrt{2 - 2\cos(2\pi kR/N)}}{\sqrt{2 - 2\cos(2\pi k/N)}} \\
	&= \frac{\sqrt{1 - \cos(2\pi kR/N)}}{\sqrt{1 - \cos(2\pi k/N)}}
\end{align*}

Da definição de DFT temos:

\begin{align*}
	X &= \begin{pmatrix}
		X_0 \\ X_1 \\ \vdots \\ X_{N-1}
	\end{pmatrix} = \begin{pmatrix}
		e^{-2\pi 0\cdot 0/N} & e^{-2\pi 0\cdot 1/N} & \dots & e^{-2\pi 0\cdot (N-1)/N} \\
		e^{-2\pi 1\cdot 0/N} & e^{-2\pi 1\cdot 1/N} & \dots & e^{-2\pi 1\cdot (N-1)/N} \\
		\vdots & & \ddots & \vdots \\
		e^{-2\pi (N-1)\cdot 0/N} & e^{-2\pi (N-1)\cdot 1/N} & \dots & e^{-2\pi (N-1)\cdot (N-1)/N} \\
	\end{pmatrix} \\
	&= \begin{pmatrix}
		x_0 \\ x_1 \\ \vdots \\ x_{N-1}
	\end{pmatrix} = F_N \begin{pmatrix}
		x_0 \\ x_1 \\ \vdots \\ x_{N-1}
	\end{pmatrix}
\end{align*}

Temos então que $DFT(x) = F_N x, \forall x\in\mathbb{C}^N$. Substituindo $e^{-i2\pi /N} = z$ temos que $z^{kn} = (e^{-i2\pi /N})^{kn} = e^{-i2\pi kn/N} = (F_N)_{kn}$. Podemos rescrever $F_N$ como

\begin{equation*}
	F_N = \begin{pmatrix}
		1 & 1 & 1 & \dots & 1 \\
		1 & z & z^2 & \dots & z^{N-1} \\
		1 & z^2 & z^4 & \dots & z^{2(N-1)} \\
		1 & z^3 & z^6 & \dots & z^{3(N-1)} \\
		\vdots & \vdots & \vdots & \ddots & \vdots \\
		1 & z^{(N-1)} & z^{2(N-1)} & \dots & z^{(N-1)(N-1)}
	\end{pmatrix}
\end{equation*}

Observe que $F_n$ é simétrica, e note que a representação matricial torna explícita a linearidade da DFT:

\begin{equation*}
	DFT(\alpha x + \beta y) = F_N(\alpha x + \beta y) = \alpha F_N x + \beta F_N y = \alpha DFT(x) + \beta DFT(y)
\end{equation*}

\subsection{Transformada Inversa de Fourier}

Lembrando da equação de síntese

\begin{align*}
	x &= \frac{1}{N} \sum\limits_{k=0}^{N-1}X_k E_{N,k} \text{ (forma vetorial)} \\
	x_n &= \frac{1}{N} \sum\limits_{k=0}^{N-1}X_k e^{i2\pi kn/N} \text{ (forma separada por componentes)}
\end{align*}

Note que

\begin{equation*}
	x = \begin{pmatrix}
		x_0 \\ x_1 \\ \vdots \\ x_{N-1}
	\end{pmatrix} = \frac{1}{N} \begin{pmatrix}
		1 & 1 & 1 & \dots & 1 \\
		1 & z^{-1} & z^{-2} & \dots & z^{-(N-1)} \\
		1 & z^{-2} & z^{-4} & \dots & z^{-2(N-1)} \\
		1 & z^{-3} & z^{-6} & \dots & z^{-3(N-1)} \\
		\vdots & \vdots & \vdots & \ddots & \vdots \\
		1 & z^{-(N-1)} & z^{-2(N-1)} & \dots & z^{-(N-1)(N-1)} \\
	\end{pmatrix} \begin{pmatrix}
		X_0 \\ X_1 \\ \vdots \\ X_{N-1}
	\end{pmatrix} = (\frac{1}{N}\overline{F_N})X = \tilde{F_N}X
\end{equation*}

Onde $\tilde{F_N}$ é a matriz que representa a IDFT. Assim, como $F_N$ é simétrica, temos que:

\begin{equation*}
	\tilde{F_N} = \frac{1}{N}\overline{F_N} = \frac{1}{N}F_N^*
\end{equation*}

e analogamente à DFT, o operador $IDFT(X) = \tilde{F_N}X$ é linear.

\subsection{Simetria da DFT para $x\in\mathbb{R}^N$}

\textbf{Teorema 2.5.1}: Seja $x\in\mathbb{C}^N$ e $DFT(x)\in\mathbb{C}^N$. Então

\begin{equation*}
	x\in\mathbb{R}^N \Leftrightarrow X_{-k} = \overline{X_{k}}, \forall k\in\mathbb{Z}
\end{equation*}

\textbf{Prova}:

\textbf{($\Rightarrow$)} Suponha que $x\in\mathbb{R}^N$, e portanto $x_n = \overline{x_N}, \forall n$. Então

\begin{align*}
	X_{-k} &= (x, E_{N,-k}) \\
	&= \sum\limits_{n=0}^{N-1} x_n e^{-i2\pi(-k)n/N} \\
	&= \sum\limits_{n=0}^{N-1} x_n e^{i2\pi kn/N} \\
	&= \overline{\overline{\sum\limits_{n=0}^{N-1} x_n e^{i2\pi kn/N}}} \\
	&= \overline{\sum\limits_{n=0}^{N-1} \overline{x_n} \overline{e^{i2\pi kn/N}}} \\
	&= \overline{\sum\limits_{n=0}^{N-1} x_n e^{-i2\pi kn/N}} \\
	&= \overline{X_k}
\end{align*}

\textbf{($\Leftarrow$)} Suponha que $X_{-k} = \overline{X_{k}}, \forall k$ e portanto $X_k = \overline{X_{-k}}, \forall k$. Então

\begin{align*}
	x_n &= \frac{1}{N}\sum\limits_{k=0}^{N-1} X_k e^{i2\pi kn/N} \\
	&= \frac{1}{N}\sum\limits_{k=0}^{N-1} \overline{X_{-k}} \overline{\overline{e^{i2\pi kn/N}}} \\
	&= \overline{\frac{1}{N}\sum\limits_{k=0}^{N-1} X_{-k} e^{-i2\pi kn/N}} \\
	&= \overline{\frac{1}{N}\sum\limits_{l=0}^{-(N-1)} X_{l} e^{i2\pi ln/N}}
\end{align*}

Como $X_l$ e $e^{i2\pi ln/N}$ são periódicos \textbf{com período $N$}:

\begin{equation*}
	X_l e^{i2\pi ln/N} = X_{l+N}e^{i2\pi(l+N)n/N}
\end{equation*}

Daí

\begin{align*}
	x_n &= \overline{\frac{1}{N}\sum\limits_{l=0}^{-(N-1)} X_{l} e^{i2\pi ln/N}} \\
	&= \overline{\frac{1}{N}\sum\limits_{m=0}^{N-1} X_{m} e^{i2\pi mn/N}} \\
	&= \overline{x_n}
\end{align*}

\textbf{Comentário}: Além da simetria $X_{-k} = \overline{X_{k}}, \forall k$ temos também $X_{N-k} = \overline{X_k}, \forall k$. Escrevendo $k = \frac{N}{2} + l$ temos

\begin{equation*}
	X_{\frac{N}{2}-l} = X_{N-(\frac{N}{2}+l)} = \overline{X_{\frac{N}{2}+l}}
\end{equation*}
