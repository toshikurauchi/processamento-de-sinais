\section{Seção 1.8: Produto interno e ortogonalidade}

\underline{Def:} Seja $V$ um espaço vetorial sobre $\mathbb{C}$. Um produto interno é uma função de $V\times V$ em $\mathbb{C}$ que satisfaz:

\begin{enumerate}[a)]
	\item $(v,w) = \overline{(w,v)}$ (simetria conjugada)
	\item $(\alpha u + \beta v, w) = \alpha (u,w) + \beta(v, w)$ (linearidade no primeiro argumento)
	\item $(v,v)\geq 0, \forall v\in V e (v,v)=0\Rightarrow v=\mathbf{0}$
\end{enumerate}

onde $\overline{a + ib} = a - ib$ é a conjugação complexa. Observe que $\overline{Ae^{ix}} = \overline{A}e^{-ix}$, ou seja, é possível entender a conjugação complexa como a inversão do sinal do ângulo de $e^{ix}$. 

% TODO imagem de complexo z com angulo phi e conjugado de z com angulo -phi

Observe também que (linearidade ``conjugada'' no segundo argumento):

\begin{align*}
	(u, \alpha v + \beta w) &= \overline{(\alpha v + \beta w, u)} \\
	&= \overline{\alpha(v,u)+\beta(w,u)} \\
	&= \overline{\alpha}\overline{(v,u)} + \overline{\beta}\overline{(w,u)} \\
	&= \overline{\alpha}(u,v) + \overline{\beta}(u,w)
\end{align*}

Além disso, por (a), $(v,v) = \overline{(v,v)} \Rightarrow (v,v)\in\mathbb{R}$, portanto a comparação $(v,v)\geq 0$ está bem definida.

\underline{Def:} Uma norma em $V$ é uma função $\|\cdot\|:V\rightarrow\mathbb{R}$ tal que:

\begin{enumerate}[a)]
	\item $\|v\|\geq 0, \forall v\in V$ e $\|v\| = 0\Rightarrow v = 0$
	\item $\|\alpha v\| = |\alpha|\|v\|$
	\item $\|v+w\| \leq \|v\| + \|w\|$ (desigualdade triangular)
\end{enumerate}

Obs: Todo produto interno $(\cdot,\cdot)$ pode ser usado para definir uma norma associada através da expressão $\|v\| = \sqrt{(v,v)}$ (ou $\|v\|^2 = (v,v)$). Porém nem toda norma possui um produto interno associado.

\subsection{Exemplos}

\begin{enumerate}
	\item $\mathbb{R}^N$ com $(x,y)=\sum\limits_{i=1}^N x_iyi$ que induz a norma Euclideana $\|x\| = \sqrt{\sum\limits_{i=1}^N x_i^2}$
	\item $\mathbb{C}^N$ com $(x,y)=\sum\limits_{i=1}^N x_i\overline{y_i}$ com a norma Euclideana $\|x\| = \sqrt{\sum\limits_{i=1}^N x_i\overline{x_i}} = \sqrt{\sum\limits_{i=1}^N |x_i|^2}$
	\item $M_{m\times n}(\mathbb{C})$ com $(A,B) = \sum\limits_{k=1}^n\sum\limits_{l=1}^n A_{kl}\overline{B_{kl}}$ com a norma de Frobenius $\|A\| = \sqrt{\sum\limits_{k=1}^m\sum\limits_{l=1}^n |A_{kl}|^2}$
	\item $e[a,b]$ = funções contínuas de $[a,b]$ em $\mathbb{C}$, $(f,g) = \int\limits_a^b f(t)\overline{g(t)}dt$ e $\|f\| = \sqrt{\int\limits_a^b |f(t)|^2dt}$ (ver observação abaixo)
	\item $C[a,b]$ com a norma $\|f\|_\infty = \max\limits_{t\in [a,b]} |f(t)|$ (esta norma não está associada a nenhum produto interno)
	\item $C([a,b]\times[c,d]) = \{f:[a,b]\times[c,d]\rightarrow\mathbb{C} | f$ é contínua$\}$ com produto interno $(f,g) = \int\limits_a^b\int\limits_c^d f(x,y)\overline{g(x,y)}dydx$, $\|f\| = \sqrt{\int\limits_a^b\int\limits c^d |f(x,y)|^2dydx}$
\end{enumerate}

Obs: $(f,g) = \int\limits_a^b f(t)\overline{g(t)}dt$ satisfaz a propriedade (c):

\begin{align*}
	f &\neq 0 \Rightarrow \exists \overline{t}\in[a,b]:f(\overline{t})\neq 0
\end{align*}

Pela continuidade de $f$ então existe um $\varepsilon > 0$ tal que $|f(x)| \geq \frac{|f(\overline{t})}{2}, \forall x\in [\overline{t}-\varepsilon, \overline{t}+\varepsilon]\Rightarrow \int\limits_a^b |f(t)|^2dt \geq |f(\overline{t})|^2\varepsilon^2 \Rightarrow (f,f) > 0$.

\subsection{Ortogonalidade}

Em $\mathbb{R}^2$ e $\mathbb{R}^3$: $(u,v) = \|u\|\|v\|\cos\theta$ onde $\theta$ é o ângulo entre $u$ e $v$. $u\perp v\Leftrightarrow \theta = 90^o \Leftrightarrow \cos\theta = 0 \Leftrightarrow (u,v) = 0$.

\underline{Def}: Dizemos que $u,v\in V$ são ortogonais em relação a um produto interno $(\cdot,\cdot)$ se $(u,v) = 0$, e denotamos $u\perp v$.

\underline{Def}: Dizemos que um conjunto $S\subseteq V$ é ortogonal se $\forall u, v\in S$, $(u,v) = 0$.

\subsubsection{Exemplo}

\begin{enumerate}
	\item Em $\mathbb{C}^N$ sejam $\varepsilon^k = \begin{pmatrix}
		0 \\ 0 \\ \vdots \\ 1 \\ 0 \\ \vdots \\ 0
	\end{pmatrix}
	\begin{matrix}
		\\ \\ \\ \rightarrow k \\ \\ \\ 
	\end{matrix}$ e $(x,y) = \sum\limits_{i=1}^N x_i\overline{y_i}$ . Temos que $(\varepsilon^k, \varepsilon^l) = \left\{\begin{matrix}
		0 & \text{ se }k \neq l \\
		1 & \text{ se }k = l 
	\end{matrix}\right.$
	\item Seja o espaço $C([-T,T]), T\in\mathbb{R}$ e as funções (contínuas)
	\begin{align*}
		f^{(k)}(t) = e^{i2\pi k\frac{1}{(2T)}t}
	\end{align*}
	Considere $f^{(k)}(t)$ e $f^{(l)}(t)$ com $k\neq l$. Então
	\begin{align*}
		(f^{(k)}, f^{(l)}) &= \int\limits_{-T}^T e^{i2\pi k\frac{1}{2T}t}\overline{e^{i2\pi l\frac{1}{2T}t}}dt \\
		&= \int\limits_{-T}^T e^{i2\pi k\frac{1}{2T}t}e^{-i2\pi l\frac{1}{2T}t}dt \\
		&= \int\limits_{-T}^T e^{i2\pi (k-l)\frac{1}{2T}t}dt \\
		&= \left[\frac{e^{i2\pi(k-l)\frac{1}{2T}t}}{i2\pi(k-l)\frac{1}{2T}}\right]_{-T}^T \\
		&= \frac{T(e^{i\pi (k-l)} - e^{-i\pi (k-l)})}{i\pi (k-l)} \\
		&= \frac{T((-1)^{k-l}-(-1)^{k-l})}{i\pi(k-l)} = 0
	\end{align*}
	Além disso, se $k\neq 0$,
	\begin{align*}
		\|f^{(k)}\| &= \sqrt{\int\limits_{-T}^T |e^{i2\pi k\frac{1}{2T}t}|^2dt} \\
		&= \sqrt{\int\limits_{-T}^T 1 dt} \\
		&= \sqrt{2T}
	\end{align*}
	Obs: Esse exemplo mostra, em particular, que $\cos(2\pi k\frac{1}{2T}t)\perp \cos(2\pi\frac{1}{2T}t)$ se $k\neq l$, $\sin(2\pi k\frac{1}{2T}t)\perp \sin(2\pi l\frac{1}{2T}t)$ se $k\neq l$, $\forall k, l$ vale que $\cos(2\pi k\frac{1}{2T}t)\perp \sin(2\pi l\frac{1}{2T}t)$ (utilizando as relações $\cos(x) = \frac{e^{ix}+e^{-ix}}{2}$ e $\sin(x) = \frac{e^{ix}-e^{-ix}}{2i}$).
	\item Em $\mathbb{C}^N$, considere
	\begin{align*}
		E_{N,k} &= \begin{pmatrix}
			e^{i2\pi k0/N} \\ e^{i2\pi k1/N} \\ \vdots \\ e^{i2\pi k(N-1)/N}
		\end{pmatrix}
	\end{align*}
	(versões amostradas da função $e^{i2\pi k\frac{1}{T}t}$ para $t=0, \frac{T}{N}, \frac{2T}{N}, \dots, \frac{(N-1)T}{N}$). Se $k\neq l$:
	\begin{align*}
		(E_{N,k},E_{N,l}) &= \sum\limits_{j=0}^{N-1}e^{i2\pi kj/N}\overline{e^{2\pi lj/N}} \\
		&= \sum\limits_{j=0}^{N-1} e^{i2\pi (k-l)j/N} \\
		&= \sum\limits_{j=0}^{N-1} \left(e^{i2\pi (k-l)/N}\right)^j \\
		&= \frac{1-(e^{i2\pi(k-l)/N})^N}{1-e^{i2\pi (k-l)/N}} \\
		&= \frac{1-e^{i2\pi(k-l)}}{1-e^{i2\pi(k-l)/N}} \\
		&= 0
	\end{align*}
	Além disso
	\begin{align*}
		\|E_{N,k}\| &= \sqrt{\sum\limits_{j=0}^{N-1} |e^{i2\pi k j/N}|^2} \\
		&= \sqrt{N}
	\end{align*}
\end{enumerate}

\subsection{Desigualdade de Cauchy-Schwarz}

\begin{equation*}
	|(u,v)| \leq \|u\|\|v\|
\end{equation*}

A ideia da demonstração sai do fato de que dada a projeção $\alpha v$ de $u$ em $v$, $\|u-\alpha v\|^2 \geq 0$.

\begin{align*}
	(u-\alpha v, v) = 0 &\Rightarrow (u,v)-\alpha(v,v) = 0 \\
	&\Rightarrow \alpha = \frac{(u,v)}{\|v\|^2} \text{ (usando o fato de que }v\neq 0\text{)}
\end{align*}

Cauchy-Shwarz: $\|u-\alpha v\|^2 \geq 0$

\begin{align*}
	(u-\alpha v, u-\alpha v) &\geq 0 \\
	(u, u-\alpha v) - \alpha (v, u-\alpha v) &\geq 0 \\
	(u,u) - \overline{\alpha}(u,u) - \alpha(v,u) + \alpha\overline{\alpha}(v,v) &\geq 0 \\
	\|u\|^2 - \frac{\overline{(u,v)}}{\|v\|^2}(u,v) - \frac{(u,v)}{\|u\|^2}\overline{(u,v)} + \frac{|(u,u)|^2}{\|v\|^4}\|v\|^2 &\geq 0 \\
	\|u\|^2 - 2\frac{|(u,v)|^2}{\|v\|^2} + \frac{|(u,v)|^2}{\|v\|^2} &\geq 0 \\
	\|u\|^2 - \frac{|(u,v)|^2}{\|v\|}^2 &\geq 0 \\
	\|u\|^2 \|v\|^2 &\geq |(u,v)|^2 \\
	\|u\|\|v\| &\geq |(u,v)|
\end{align*}

\subsection{Bases e decomposição ortogonal}

Ideia: representar vetores através de expressões

\begin{align*}
	v &= \alpha_1 v^{(1)} + \alpha_2 v^{(2)} + \dots + \alpha_n v^{(n)}
\end{align*}

Def: Uma coleção $\{v^{k}\}_{k\in I}\subseteq V$ gera o espaço vetorial $V$ se para todo $v\in V$ existem $\alpha_1, \alpha_2, \dots, \alpha_N\in\mathbb{C}$ e $v^{(k_1)}, v^{(k_2)}, \dots, v^{(k_N)}, k_j\in I$, tais que $v = \sum\limits_{i = 1}^{N}\alpha_i v^{(k_i)}$.

Def: Um conjunto $S\subseteq V$ é linearmente independente se, para qualquer coleção finita $\{s^{(1)}, s^{(2)}, \dots, s^{(N)}\}\subseteq S$, a única solução do sistema

\begin{align*}
	\alpha_1s^{(1)} + \alpha_2s^{(2)} + \dots + \alpha_Ns^{(N)} &= 0
\end{align*}

com $\alpha_1, \alpha_2, \dots, \alpha_N \in \mathbb{C}$ é a solução trivial $\alpha_1 = \alpha_2 = \dots = \alpha_N = 0$.

Def: Uma \underline{base} do espaço vetorial $V$ é um conjunto $S\subseteq V$ \underline{que gera $V$} e é \underline{linearmente independente}.

\textbf{\underline{Exemplo:}} Seja $V$ o conjunto de funções polinomiais (com grau finito, não limitado a priori), ou seja, funções da forma

\begin{align*}
	p(x) &= \sum\limits_{k=0}^{N_p} a_kx^k (*)
\end{align*}

A família infinita $S = \{1, x, x^2, x^3, \dots\}$ é uma base para $V$.

\begin{enumerate}
	\item $S$ gera $V$, pois qualquer polinômio $p\in V$ tem a forma geral $(*)$.
	\item Se $\sum\limits_{k=0}^k = 0 (\forall x)$, então $\alpha_k = 0, \forall k$ (exercício).
\end{enumerate}

\subsubsection{Bases ortogonais e ortonormais}

\textbf{\underline{Teo 1.8.2:}} Se uma coleção $S\subseteq V\\\{0\}$ é ortogonal $(\forall s^{(1)}, s^{(2)}\in S, (s^{(1)}, s^{(2)}) = 0)$ então $S$ é linearmente independente.

\textbf{\underline{Prova}}: Considere $\{\alpha_k\}_{k=1}^N\in\mathbb{C}$ e $\{s^{(k)}\}_{k=1}^N\subseteq S$ tais que $\sum\limits_{k=1}^N \alpha_k s^{(k)} = 0$ (**). Multiplicando (**) por $s^{(j)}$ (para $j\in\{1, 2, \dots, N\}$) teremos

\begin{align*}
	(\sum\limits_{k=1}^N\alpha_ks^{(k)}, s^{(j)}) &= (0, s^{(j)}) = 0 \\
	\sum\limits_{k=1}^N \alpha_k(s^{(k)}, s^{(j)}) &= 0 \\
	\alpha_j (s^{(j)}, s^{(j)}) &= 0 \\
	\alpha_j &= 0
\end{align*}

pois $(s^{(j)}, s^{(j)}) > 0$, já que $s^{(j)} \neq \mathbf{0}$. Como o argumento independe de $j$, segue que $S$ é linearmente independente.

\textbf{\underline{Teo 1.8.3:}} Seja $S = \{s^{(1)}, s^{(2)}, \dots, s^{(N)}\}\subseteq V$ uma base ortogonal para o espaço vetorial $V$. Então todo $v\in V$ pode ser escrito como $v = \sum\limits_{k=1}^n \alpha_k s^{(k)}$ onde $\alpha_k = \frac{(v, s^{(k)})}{(s^{(k)}, s^{(k)})}$.

\textbf{\underline{Prova}}: Como $S$ gera $V$, todo $v\in V$ pode ser escrito como $v = \sum\limits_{k=1}^N \alpha_k s^{(k)}$ para alguma coleção de escalares $\alpha_1, \alpha_2, \dots, \alpha_N\in\mathbb{C}$. Além disso, repetindo o argumento do teorema 1.8.2, para $j\in\{1,\dots, N\}$,

\begin{align*}
	(v, s^{(j)}) &= (\sum\limits_{k=1}^n \alpha_k s^{(k)}, s^{(j)}) \\
	&= \sum\limits_{k=1}^n \alpha_k (s^{(k)}, s^{(j)}) \\
	&= \alpha_j (s^{(j)}, s^{(j)})
\end{align*}

mas $(s^{(j)}, s^{(j)}) \neq 0$, pois $S$ é base. Daí

\begin{align*}
	\alpha_j &= \frac{(v, s^{(j)})}{(s^{(j)}, s^{(j)})}, \forall j (=1, 2, \dots, n)
\end{align*}

Def: Uma base $S$ é ortonormal se $S$ é ortogonal e $\|s\| = 1, \forall s\in S$.

Obs: Se $S = \{s^{(1)}, s^{(2)}, \dots, s^{(n)}\}$ é base ortonormal de $V$, então todo $v\in V$ pode ser escrito como

\begin{align*}
	v &= \sum\limits_{k=1}^{n} (v, s^{(k)})s^{(k)}
\end{align*}

ou seja, $\alpha_k = (v, s^{(k)})$. Além disso, toda base ortogonal pode ser transformada em base ortogonal substituindo $s\in S$ por $\frac{s}{\|s\|}, \forall s$.

\textbf{\underline{Exemplo:}} Em $\mathbb{C}^N$, os vetores

\begin{align*}
	E_{N, k} = \begin{pmatrix}
		e^{i2\pi k 0/N} \\
		e^{i2\pi k 1/N} \\
		\vdots \\
		e^{i2\pi k (N-1)/N} 
	\end{pmatrix}, k = 0, \dots, N-1
\end{align*}

formam uma família ortogonal e pelo teorema 1.8.2, linearmente independente. Como esta coleção tem tamanho $N = dim(\mathbb{C}^N)$, temos que esta coleção é uma base ortogonal de $\mathbb{C}^N$, e portanto, todo elemento $v\in\mathbb{C}^N$ pode ser escrito como

\begin{align*}
	v &= \sum\limits_{k=0}^{N-1} \frac{(v, E_{N, k})}{(E_{N, k}, E_{N, k})}E_{N, k}
\end{align*}

onde $v, E_{N, k} = \sum\limits_{n = 0}^{N-1} v_n e^{-i2\pi kn/N}$ e 

\begin{align*}
	(E_{N, k}, E_{N, k} &= \sum\limits_{n=0}^{N-1} e^{i2\pi kn/N} e^{-i2\pi kn/N} \\
	&= \sum\limits_{n=1}^{N-1} 1 \\
	&= N
\end{align*}

\textbf{\underline{Exemplo:}} Em $M_{M\times N}(\mathbb{C})$, a família

\begin{align*}
	E_{M, N, k, l} &= \begin{pmatrix}
		\ddots & \vdots, & \\
		\dots & e^{i2\pi (km/M + ln/N)} & \dots \\
		& \vdots & \ddots
	\end{pmatrix}
\end{align*}

contém $M\times N$ matrizes ortogonais duas a duas e portanto linearmente independentes, ou seja, esta família é uma base ortogonal de $M_{M\times N}(\mathbb{C})$. Assim, toda matriz $A\in M_{M\times N}(\mathbb{C})$ pode ser escrita como

\begin{align*}
	A &= \sum\limits_{k=0}^{M-1}\sum\limits_{l = 0}^{N-1} \alpha_{kl} E_{M, N, k, l}
\end{align*}

onde

\begin{align*}
	\alpha_{k,l} &= \frac{(A, E_{M, N, k, l})}{(E_{M, N, k, l}, E_{M, N, k, l})} \\
	&= \frac{1}{MN}\sum\limits_{k=0}^{M-1}\sum\limits_{l = 0}^{N-1} A_{mn}e^{-i2\pi (km/M + ln/N)}
\end{align*}

\subsubsection{Identidade de Parseval}

Seja $S$ uma base ortonormal de um espaço vetorial $V$, e seja $V = \sum\limits_{k=1}^N \alpha_k s^{(k)}\in V$. Então

\begin{align*}
	\|v\|^2 &= (v, v) \\
	&= (\sum\limits_{k=1}^N \alpha_k s^{(k)}, \sum\limits_{l=1}^N \alpha_l s^{(l)}) \\
	&= \sum\limits_{k=1}^N \alpha_k (s^{(k)}, \sum\limits_{l=1}^N \alpha_l s^{(l)}) \\
	&= \sum\limits_{k=1}^N \alpha_k \sum\limits_{l=1}^N \overline{\alpha_l} (s^{(k)}, s^{(l)}) \\
	&= \sum\limits_{k=1}^N \alpha_k \overline{\alpha_k} \\
	&= \sum\limits_{k=1}^N |\alpha_k|^2
\end{align*}

Assim qualquer base ortonormal de $V$ pode ser usada para obtenção da norma através da expressão acima.
