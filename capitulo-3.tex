\section{Capítulo 3}

\subsection{Motivação para a DCT (Transformada Discreta do Cosseno): Compressão de sinais}

Muitos dos arquivos que utilizamos passam por algum tipo de compactação: .jpg, .mp3, .avi, .mpeg, .mkv. 

Estratégia simplória de compactação:

\begin{enumerate}
	\item \textbf{limiarização}: Definir um parâmetro $c\in [0,1]$ e preservar apenas características espectrais com magnitude relativa $> c$ , ou seja, se $\hat{A}$ é o espectro de uma imagem, então preservamos $\hat{A}_{k,l}$ se: \begin{equation*}
		|\hat{A}_{k,l}| \geq c\max\limits_{p,q} |\hat{A}_{p,q}|
	\end{equation*}
	\item codificação de modo a evitar a representação explícita das componentes espectrais suprimidas (exemplos: \emph{run length encoding}: representar sequências de valores iguais da forma $N\times $valor; codificação de Huffman: cria árvores associadas a representações binárias baseadas em frequência de observação dos símbolos
\end{enumerate}

Esquema de compressão simplificado:

\begin{center}
\includegraphics[width=0.7\textwidth]{images/codificacao.pdf}
\end{center}

Duas medidas importantes:

Taxa de compressão: $\frac{\#X_\text{Esparso}}{\#x} = $ percentual de componentes eliminadas na limiarização.

Perda de qualidade: $\frac{\|x-\hat{x}\|^2}{\|x\|^2} = $ distância relativa = variação relativa de energia.

Exemplo em aula: \emph{tabelas 3.1} e \emph{3.2} e figuras \emph{3.5} a \emph{3.7}. Qualquer descontinuidade no sinal é ruim para a compressão. É comum recortar o sinal em trechos contínuos para obter uma boa compressão sem causar muitas deformações.

\subsection{Transformada Discreta do Cosseno}

Motivação: eliminar a descontinuidade das ``bordas'' (ou seja, o fato de que, em geral, $x_0 \neq x_{N-1}$), espelhando a informação e transformando $x = (x_0, x_1, \dots\linebreak, x_{N-1})$ em $\tilde{x}$ $ = $ $(x_0, x_1, \dots, x_{N-1},$ $ x_{N-1}, x_{N-2}, $ $\dots, X_0)$.

\begin{equation*}
	\tilde{x}_n = \left\{\begin{array}{ll}
		x_n & \text{se } n = 0, 1, \dots, N-1 \\
		x_{2N-n-1} & \text{se } n = N, N+1, \dots, 2N-1
	\end{array}\right.
\end{equation*}

A DFT de $\tilde{x}$ é

\begin{align*}
	\tilde{X}_k &= \sum\limits_{n=0}^{2N-1} \tilde{x}_n e^{-i2\pi kn/(2N)} \\
	&= \sum\limits_{n=0}^{N-1} x_ne^{-i\pi kn/N} + \sum\limits_{n=N}^{2N-1} x_{2N-n-1}e^{-i\pi kn/N} \\
	&= \sum\limits_{n=0}^{N-1} x_ne^{-i\pi kn/N} + \sum\limits_{l=0}^{N-1} x_{N-l-1}e^{-i\pi k(l+N)/N} \\
	&= \sum\limits_{n=0}^{N-1} x_ne^{-i\pi kn/N} + \sum\limits_{m=0}^{N-1} x_me^{-i\pi k(2N-m-1)/N} \\
	&= \sum\limits_{n=0}^{N-1} x_n(e^{-i\pi kn/N} + e^{i\pi k(n+1)/N}\cdot e^{-i\pi k2}) \\
	&= \sum\limits_{n=0}^{N-1} x_n(e^{-i\pi kn/N} + e^{i\pi k(n+1)/N}) \\
\end{align*}

mas isso lembra a representação do cosseno como soma de exponenciais complexas $\cos(x) = \frac{e^{ix} + e^{-ix}}{2}$. Para chegar nisso utilizaremos $e^{-i\pi kn/N} =\linebreak e^{-i\pi k(n + \frac{1}{2})/N}e^{i\pi k/(2N)}$ e $e^{i\pi k(n + \frac{1}{2})/N}e^{i\pi k/(2N)}$. Daí

\begin{align*}
	\tilde{X}_k &= 2e^{i\pi k/(2N)} \sum\limits_{n=0}^{N-1} x_n \cos(\pi k (n+\frac{1}{2})/N) 
\end{align*}

Seja $c_k = 2\sum\limits_{n=0}^{N-1} x_n \cos(\pi k (n+\frac{1}{2})/N)$ (equação de análise). Temos que:

\begin{enumerate}
	\item $c_k\in\mathbb{R}$ sempre que $x\in\mathbb{R}^N$
	\item $c_{-k} = c_k, \forall k$
	\item $c_{k+2N} = -c_k$
\end{enumerate}

Pela propriedade (3) podemos guardar apenas $c_0, c_1, \dots, c_{N-1}$, sem perder a possibilidade de reconstrução exata do vetor $\tilde{X}\in\mathbb{C}^{2N}$. A partir de $c_0, c_1, \dots\linebreak, c_{N-1}$ podemos ressintetizar o vetor $\tilde{x} = (x_0, x_1, \dots, x_{N-1}, x_{N-1}, \dots, x_0)$ assim:

\begin{align*}
	\tilde{x}_n &= \frac{1}{2N}\sum\limits_{k=0}^{2N-1} \tilde{X}_k e^{i2\pi kn/(2N)} \\
	&= \frac{1}{2N}\sum\limits_{k=0}^{2N-1} e^{i\pi k/(2N)} c_k e^{i2\pi kn/(2N)} \\
	&= \frac{1}{2N}\left[\sum\limits_{k=0}^{N-1}c_k e^{i2\pi k(n+\frac{1}{2})/(2N)} + \sum\limits_{k=N}^{2N-1}c_k e^{i2\pi k(n+\frac{1}{2})/(2N)}\right] \\
	&= \frac{1}{2N}\left[\sum\limits_{k=0}^{N-1}c_k e^{i2\pi k(n+\frac{1}{2})/(2N)} + \sum\limits_{l=0}^{N-1}c_{2N-1-l} e^{i2\pi (2N-1-l)(n+\frac{1}{2})/(2N)}\right] \\
	&= \frac{1}{2N}\left[\sum\limits_{k=0}^{N-1}c_k e^{i2\pi k(n+\frac{1}{2})/(2N)} + \sum\limits_{k=1}^{N-1}c_k e^{-i2\pi k(n+\frac{1}{2})/(2N)}\right] \\
	&= \frac{1}{2N}\left[c_0 + 2\sum\limits_{k=1}^{N-1}c_k\cos(\pi k(n+\frac{1}{2})/N)\right]
\end{align*}

Definindo a equação de síntese: $\tilde{x}_n = \frac{1}{2N}\left[c_0 + 2\sum\limits_{k=1}^{N-1}c_k\cos(\pi k(n+\frac{1}{2})/N)\right]$.

\textbf{\underline{Def}}: A transformada do cosseno do vetor $x\in\mathbb{C}^N$ é o vetor $C\in\mathbb{C}^N$ dado por

\begin{align*}
	C_0 &= \sqrt{\frac{1}{N}}\sum\limits_{m=0}^{N-1} x_m\cos\left(\frac{\pi k\left(m+\frac{1}{2}\right)}{N}\right) \\
	&= \sqrt{\frac{1}{N}}\sum\limits_{m=0}^{N-1} x_m \\
	C_k &= \sqrt{\frac{2}{N}}\sum\limits_{m=0}^{N-1} x_m\cos\left(\frac{\pi k\left(m+\frac{1}{2}\right)}{N}\right), k = 1, 2, \dots, N-1 \\
\end{align*}

com a respectiva equação de síntese dada por

\begin{equation*}
	x_m = \sqrt{\frac{1}{N}}C_0 + \sqrt{\frac{2}{N}}\sum\limits_{k=1}^{N-1}C_k\cos\left(\frac{\pi\left(m + \frac{1}{2}\right)k}{N}\right)
\end{equation*}

Podemos escrever a $DCT(x)$ em forma matricial como $DCT(x) = C_N x$ onde

\begin{equation*}
	C_N = \begin{pmatrix}
		\sqrt{\frac{1}{N}} & \sqrt{\frac{1}{N}} & \dots & \sqrt{\frac{1}{N}} \\
		\sqrt{\frac{2}{N}}\cos\left(\frac{\pi\left(0 + \frac{1}{2}\right)}{N}\right) & \sqrt{\frac{2}{N}}\cos\left(\frac{\pi\left(1 + \frac{1}{2}\right)}{N}\right) & \dots & \sqrt{\frac{2}{N}}\cos\left(\frac{\pi\left((N-1) + \frac{1}{2}\right)}{N}\right) \\
		\vdots & \vdots & \ddots & \vdots \\
		\sqrt{\frac{2}{N}}\cos\left(\frac{\pi(N-1)\left(0 + \frac{1}{2}\right)}{N}\right) & \sqrt{\frac{2}{N}}\cos\left(\frac{\pi(N-1)\left(1 + \frac{1}{2}\right)}{N}\right) & \dots & \sqrt{\frac{2}{N}}\cos\left(\frac{\pi(N-1)\left((N-1) + \frac{1}{2}\right)}{N}\right) \\
	\end{pmatrix}
\end{equation*}

onde

\begin{equation*}
	(C_N)_{k,m} = \left\{\begin{array}{ll}
		\sqrt{\frac{1}{N}} & \text{ se }k = 0 \\
		\sqrt{\frac{2}{N}}\cos\left(\frac{\pi k\left(m + \frac{1}{2}\right)}{N}\right) & \text{ se } k = 1, 2, \dots, N-1
	\end{array}\right.
\end{equation*}

A inversa da DCT pode ser expressa como $IDCT(C) = \hat{C}_N C$ onde

\begin{equation*}
	\hat{C}_N = \begin{pmatrix}
		\sqrt{\frac{1}{N}} & \sqrt{\frac{2}{N}}\cos\left(\frac{\pi\left(0 + \frac{1}{2}\right)}{N}\right) & \dots & \sqrt{\frac{2}{N}}\cos\left(\frac{\pi(N-1)\left(0 + \frac{1}{2}\right)}{N}\right) \\
		\sqrt{\frac{1}{N}} & \sqrt{\frac{2}{N}}\cos\left(\frac{\pi\left(1 + \frac{1}{2}\right)}{N}\right) & \dots & \sqrt{\frac{2}{N}}\cos\left(\frac{\pi(N-1)\left(1 + \frac{1}{2}\right)}{N}\right) \\
		\vdots & \vdots & \ddots & \vdots \\
		\sqrt{\frac{1}{N}} & \sqrt{\frac{2}{N}}\cos\left(\frac{\pi\left((N-1) + \frac{1}{2}\right)}{N}\right) & \dots & \sqrt{\frac{2}{N}}\cos\left(\frac{\pi(N-1)\left((N-1) + \frac{1}{2}\right)}{N}\right) \\
	\end{pmatrix}
\end{equation*}

ou seja,

\begin{equation*}
	(\hat{C}_N)_{m,k} = \left\{\begin{array}{ll}
		\sqrt{\frac{1}{N}} & \text{ se }k = 0 \\
		\sqrt{\frac{2}{N}}\cos\left(\frac{\pi k\left(m + \frac{1}{2}\right)}{N}\right) & \text{ se } k = 1, 2, \dots, N-1
	\end{array}\right.
\end{equation*}

Daqui observamos que 

\begin{equation*}
	\hat{C}_N = C_N^T
\end{equation*}

Pelo fato de que $IDCT(DCT(x)) = x$, temos que $C_N^T(C_Nx) = x, \forall x$ de onde

\begin{equation*}
	C_N^T = C_N^{-1}
\end{equation*}

Em particular, se $C_N = \begin{pmatrix}
	C_{N,0}^T \\ C_{N,1}^T \\ \vdots \\ C_{N,N-1}^T
\end{pmatrix}$ temos 

\begin{equation*}
	I = C_NC_N^{-1} = C_NC_N^T = \begin{pmatrix}
		\ddots & \vdots & \\
		\dots & C_{N,i}^TC_{N,j} & \dots \\
		& \vdots & \ddots
	\end{pmatrix}
\end{equation*}

Logo

\begin{equation*}
	(C_{N,i}, C_{N,j}) = C_{N,i}^TC_{N,j} = \left\{\begin{array}{ll}
		1 & \text{ se }i = j \\
		0 & \text{ se }i \neq j
	\end{array}\right.
\end{equation*}

Desta propriedade segue que a coleção de vetores $\{C_{N,k}\}_{k=0, 1, \dots, N-1}$ onde 

\begin{equation*}
	C_{N,k} = \left\{\begin{array}{ll}
		\begin{pmatrix}
			\sqrt{\frac{1}{N}} \\ \sqrt{\frac{1}{N}} \\ \vdots \\ \sqrt{\frac{1}{N}}
		\end{pmatrix} & \text{ se } k = 0 \\
		\sqrt{\frac{2}{N}}\begin{pmatrix}
			\cos\left(\frac{\pi k\left(0 + \frac{1}{2}\right)}{N}\right) \\
			\cos\left(\frac{\pi k\left(1 + \frac{1}{2}\right)}{N}\right) \\
			\vdots \\
			\cos\left(\frac{\pi k\left(N-1 + \frac{1}{2}\right)}{N}\right)
		\end{pmatrix} & \text{ se } k = 1, 2, \dots, N-1
	\end{array}\right.
\end{equation*}

é uma base ortonormal tanto para $\mathbb{C}^N$ quanto para $\mathbb{R}^N$.

Lembrando que uma base ortonormal $\{C_{N,k}\}_{k=0, 1, \dots, N-1}$ permite a representação de um $x\in\mathbb{C}^N$ qualquer como

\begin{equation*}
	x = \sum\limits_{k=0}^{N-1}\alpha_k C_{N,k}
\end{equation*}

onde $\alpha_k = (x, C_{N,k}) = C_k = \left\{\begin{array}{ll}
	\sum\limits_{m=0}^{N-1} x_m \sqrt{\frac{1}{N}} & \text{ se } k = 0 \\
	\sum\limits_{m=0}^{N-1} x_m \sqrt{\frac{2}{N}}\cos\left(\frac{\pi k\left(m + \frac{1}{2}\right)}{N}\right) & \text{ se } k \neq 0
\end{array}\right.$

\underline{\textbf{Observação}}: Se $x\in\mathbb{R}^N$, temos que $C = DCT(x)\in\mathbb{R}^N$ também.

\subsection{DCT 2D}

Considerando $DCT(x) = C_Nx$ e $IDCT(C) = C_N^T C$ podemos definir uma transformação em 2D para $A\in M_{M\times N}(\mathbb{C})$ assim

\begin{equation*}
	\hat{A} = DCT(A) = (C_M A) C_N^T (\in M_{M\times N}(\mathbb{C}))
\end{equation*}

cuja inversa é dada por

\begin{equation*}
	A = IDCT(\hat{A}) = C_M^T \hat{A} C_N
\end{equation*}

Lembrando dos coeficientes das matrizes $C_N$ e $C_M$, temos

\begin{equation*}
	\hat{A}_{k,l} = u_k v_l\sum\limits_{m = 0}^{M-1}\sum\limits_{n = 0}^{N-1} A_{mn}\cos\left(\frac{\pi k\left(m + \frac{1}{2}\right)}{M}\right)\cos\left(\frac{\pi l\left(n + \frac{1}{2}\right)}{N}\right)
\end{equation*}

onde

\begin{equation*}
	u_k = v_k = \left\{\begin{array}{ll}
		\sqrt{\frac{1}{N}} & \text{ se } k = 0 \\
		\sqrt{\frac{2}{N}} & \text{ se } k \neq 0 \\
	\end{array}\right.
\end{equation*}

\underline{\textbf{Observação}}: $A\in M_{M\times N}(\mathbb{R}) \Leftrightarrow \hat{A}\in M_{M\times N}(\mathbb{R})$.

\subsubsection{Transformadas em Bloco}

\begin{center}
	\includegraphics[width=0.6\textwidth]{images/transformada-em-bloco.pdf}
\end{center}

Onde

\begin{equation*}
	B = \begin{pmatrix}
		A_{8i,8j} & \dots & \dots \\
		\vdots & \ddots & \vdots \\
		\dots & \dots & A_{(8i+7), (8j+7)}
	\end{pmatrix}
\end{equation*}

De onde definimos $\hat{B} \neq DCT(A)$ onde cada $\hat{B}_{i,j} = DCT(B_{i,j})$, ou seja, um bloco de 8x8. Outra organização possível é agrupar os blocos por valores de frequência $k$ e $l$ (matriz $\hat{C}$).

\subsubsection{Compactação JPEG}

\begin{enumerate}
	\item Separação em cores (RGB $\rightarrow R\in M_{M\times N}(\mathbb{R}), G\in M_{M\times N}(\mathbb{R}), B\in M_{M\times N}(\mathbb{R})$ )
	\item Blocagem (8x8) e DCT em blocos
	\item Quantização 
	\begin{equation*}
		E \in M_{8\times 8}
	\end{equation*}
	Onde $e_{k,l}$ é o elemento na $k$-ésima linha e $l$-ésima coluna. Cada coeficiente DCT de frequências ($k,l$) será quantizado como
	\begin{equation*}
		q(x) = round\left(\frac{x}{e_{k,l}}\right)
	\end{equation*}
	A reconstrução é dada por
	\begin{equation*}
		\tilde{x} = e_{k,l}q(x)
	\end{equation*}
	\item Codificação
\end{enumerate}

\subsubsection{Codificação sequencial $\times$ progressiva}

\begin{itemize}
	\item \textbf{Sequencial}: varredura tradicional (por linhas, por exemplo) da matriz $\hat{B}$ com blocos $\hat{B}_{ij}\in M_{8\times 8}(\mathbb{R})$. Pode-se reconstruir a imagem a partir dos blocos $8\times 8$ do canto superior esquerdo, na varredura por linhas, sendo que a qualidade da imagem parcial reconstruída é a melhor possível.
	\item \textbf{Progressiva}: baseada no agrupamento das DCT's dos blocos por pares de frequência ($k,l$) iguais. Transmite-se inicialmente os coeficientes D.C. ($\hat{C}_{0,0}$) e progressivamente adicionam-se frequências mais altas. A informação chega na forma de uma imagem inteira e grosseira, e progressivamente aumenta-se a definição.
\end{itemize}
