\section{Seção 1.10: Espaços vetoriais de dimensão infinita}

Exemplo de espaço vetorial de dimensão infinita: $V = \{f:[a,b]\rightarrow \mathbb{C}, f$ contínua$\}$. Vamos mostrar que existem coleções de funções linearmente independentes de tamanho $N > 0$ arbitrariamente grande, e por isso o espaço não admite base finita.

\begin{center}
\includegraphics[width=0.7\textwidth]{images/espacoFuncoesInf.pdf}
\end{center}

Seja $N > 0$ qualquer e $N+1$ pontos $x^1, x^2, \dots, x^{N+1}$ tais que $a\leq x^1 \leq x^2 \leq \dots \leq x^{N+1}\leq b$. Considere $f^{(k)}:[a,b]\rightarrow\mathbb{R}$ dada por

\begin{align*}
	f^{(k)}(x) &= \left\{\begin{array}{ll}
		0 & \text{se } x\leq x^k\text{ ou } x\geq x^{k+1} \\
		x-x_k & \text{se } x\in[x_k, x_k+\frac{1}{2}(x^{k+1}-x^k)] \\
		x_{k+1}-x & \text{se } x\in[x_k+\frac{1}{2}(x^{k+1}-x^k), x^{k+1}]
	\end{array}\right.
\end{align*}

para $k = 1, \dots, N$. Se $k\neq l$, então

\begin{align*}
	(f^{(k)}, f^{(l)}) &= \int\limits_a^b f^{(k)}(x)\overline{f^{(l)}(x)}dx \\
	&= 0
\end{align*}

de onde esta coleção de tamanho $N > 0$ arbitrário é ortogonal e portanto linearmente independente.

Obs: Na definição de conjunto gerador, supusemos que os elementos de $V$ eram gerados através de combinações lineares finitas de elementos de S. As bases definidas anteriormente são também chamadas de bases de Hamel por esta propriedade da geração finita.

Queremos poder escrever combinações lineares de infinitos termos como

\begin{align*}
	v &= \sum\limits_{k=0}^\infty \alpha_k v^{(k)}
\end{align*}

e para isso precisamos atribuir um sentido claro para esta expressão. Dizemos que $v = \sum\limits_{k=0}^\infty \alpha_k v^{(k)}$ quando

\begin{align*}
	\lim\limits_{N\rightarrow \infty} \|v - \sum\limits_{k=0}^N \alpha_k v^{(k)}\| = 0
\end{align*}

Def: Um conjunto $S\subseteq V$ é dito \underline{completo} se $\forall v\in V$ existem escalares $\alpha_1, \alpha_2, \dots$ e $s^{(1)}, s^{(2)}, \dots \in S$ tais que $v = \sum\limits_{k=1}^\infty \alpha_k s^{(k)}$, ou equivalentemente,

\begin{align*}
	\lim\limits_{N\rightarrow\infty} \|v - \sum\limits_{k = 1}^{N}\alpha_ks^{(k)}\| = 0
\end{align*}

Supondo que $S$ é completo e ortonormal, podemos escrever qualquer $v\in V$ como $v = \sum\limits_{k=1}^\infty \alpha_k v^{(k)}$, onde $\alpha_k = (v, v^{(k)})$, exatamente como no caso de dimensão finita. Para provar isso, considere a expressão

\begin{align*}
	v - \sum\limits_{k=1}^N \alpha_kv^{(k)}
\end{align*}

e seu produto (interno) por um $v^{(j)}$ com $j \leq N$:

\begin{align*}
	(v - \sum\limits_{k=1}^N\alpha_kv^{(k)}, v^{(j)}) &= (v, v^{(j)}) - \sum\limits_{k=1}^N \alpha_k(v^{(k)}, v^{(j)}) \\
	&= (v, v^{(j)}) - \alpha_j
\end{align*}

Por Cauchy-Schwarz:

\begin{align*}
	|(v-\sum\limits_{k=1}^N\alpha_kv^{(k)}, v^{(j)})| &\leq \|v - \sum\limits_{k=1}^N\alpha_k v^{(k)}\|\|v^{(j)}\|
\end{align*}

Assim, tomando o limite para $N\rightarrow\infty$ temos

\begin{align*}
	0 &= (v - \sum\limits_{k=1}^\infty\alpha_kv^{(k)}, v^{(j)}) \\
	&= (v, v^{(j)}) - \alpha_j
\end{align*}

de onde $\alpha_j = (v, v^{(j)})$ (teorema 1.10.1).

\subsection{Identidade de Parseval}

No contexto acima (quando $S$ é ortonormal) $\|v\| = \sum\limits_{k=0}^\infty |\alpha_k|^2$. Para verificar isso, note que para qualquer $N\in\mathbb{N}$ fixado

\begin{align*}
	\|v - \sum\limits_{k=0}^N \alpha_k s^k\|^2 &= (v - \sum\limits_{k=0}^N \alpha_k s^k, v - \sum\limits_{l=0}^N \alpha_l s^l) \\
	&= (v,v) - \sum\limits_{k=0}^N \alpha_k (s^k, v) - \sum\limits_{l=0}^N \overline{\alpha_l}(v, s^l) + \sum\limits_{k=0}^N \alpha_k\sum\limits_{l=0}^N\overline{\alpha_l} (s^k, s^l) \\
	&= \|v\|^2 - \sum\limits_{k=0}^N |\alpha_k|^2 - \sum\limits_{l=0}^N |\alpha_l|^2 + \sum\limits_{k=0}^N|\alpha_l|^2 \\
	&= \|v\|^2 - \sum\limits_{l=0}^N |\alpha_l|^2
\end{align*}

Como $0 = \lim\limits_{N\rightarrow\infty} \|v - \sum\limits_{k=0}^N\alpha_k s^k\| = \lim\limits_{N\rightarrow\infty} \|v\|^2 - \sum\limits_{k=0}^N|\alpha_k|^2 \Rightarrow \|v\|^2 = \sum\limits_{k=0}^N |\alpha_k|^2$.

\subsection{Séries de Fourier em $C[-T, T]$}

Fato: a coleção $S = \{s^{(k)}\}_{k\in\mathbb{Z}}$ dada por 

\begin{align*}
	s^{(k)}(t) &= e^{i2\pi k t/(2T)}
\end{align*}

é completa em relação a $C[-T, T]$. Este fato garante que qualquer função $f\in C[-T,T]$ pode ser escrita como 

\begin{align*}
	f(t) &= \sum\limits_{k=-\infty}^{\infty} \alpha_k e^{i\pi kt/T}
\end{align*}

onde 

\begin{align*}
	\alpha_k &= \frac{(f,s^{(k)})}{(s^{(k)}, s^{(k)})} \\
	&= \frac{\int\limits_{-T}^T f(t)e^{-i\pi kt/T}dt}{\int\limits_{-T}^Te^{i\pi kt/T}e^{-i\pi kt/T}dt} \\
	&= \frac{1}{2T} \int\limits_{-T}^T f(t)e^{-i\pi kt/T}dt
\end{align*}

\textbf{\underline{Exemplos:}}

\begin{enumerate}
	\item Seja $f(t) = t$ para $t\in [-T, T]$. Temos que $f(t) = \sum\limits_{k=-\infty}^\infty \alpha_k e^{i\pi kt/T}$ onde $\alpha_k = \frac{1}{2T}\int\limits_{-T}^T f(t)e^{-i\pi kt/T}dt$. Note que
	\begin{align*}
		\alpha_0 &= \frac{1}{2T}\int\limits_{-T}^T te^0 dt = \frac{1}{2T}\left[\frac{t^2}{2}\right]_{-T}^T \\
		&= \frac{1}{2T}\left[\frac{T^2}{2} - \frac{(-T)^2}{2}\right] = 0
	\end{align*}
	Para $k\neq 0$:
	\begin{align*}
		\alpha_k = \frac{1}{2T}\int\limits_{-T}^T te^{-i\pi kt/T}dt
	\end{align*}
	Fazendo a integração por partes:
	\begin{align*}
		\left[-\frac{te^{-i\pi kt/T}}{i\pi kt/T}\right]' &= -\frac{e^{-i\pi kt/T}}{i\pi kt/T} + \frac{te^{-i\pi kt/T}}{i\pi kt/T}(i\pi kt/T)
	\end{align*}
	Integrando dos dois lados:
	\begin{align*}
		-\frac{te^{-i\pi kt/T}}{i\pi kt/T} &= \frac{e^{-i\pi kt/T}}{(i\pi kt/T)^2} + \int te^{-i\pi kt/T}
	\end{align*}
	Voltando ao $\alpha_k (k\neq 0)$
	\begin{align*}
		\alpha_k &= \frac{1}{2T}\left[-\frac{te^{-i\pi kt/T}}{i\pi kt/T} - \frac{e^{-i\pi kt/T}}{(i\pi kt/T)^2}\right]_{-T}^T \\
		&= \frac{1}{2T}\left[-\frac{Te^{-i\pi k}}{i\pi k/T} - \frac{e^{-i\pi k}}{(i\pi k)^2} - \left(\frac{Te^{i\pi k}}{i\pi k/T} - \frac{e^{i\pi k}}{(i\pi k)^2}\right)\right] \\
		&= \frac{-(-1)^k}{i\pi k/T}
	\end{align*}
	\item $f(t) = t^2$ em $t\in [-T, T]$
	\begin{align*}
		\beta_k = \frac{1}{2T}\int\limits_{-T}^T f(t)e^{-i\pi kt/T}dt
	\end{align*}
	Integrando por partes (supondo $k \neq 0$):
	\begin{align*}
		\left[-\frac{t^2e^{-i\pi kt/T}}{i\pi kt/T}\right]' &= -\frac{2te^{-i\pi kt/T}}{i\pi kt/T} + \frac{t^2e^{-i\pi kt/T}}{i\pi kt/T}(-i\pi kt/T) \\
	\end{align*}
	Integrando dos dois lados:
	\begin{align*}
		\int t^2e^{-i\pi kt/T} &= -\frac{t^2e^{-i\pi kt/T}}{i\pi k/T} + \frac{2}{i\pi k/T}\int te^{-i\pi kt/T} \\
		&= -\frac{t^2e^{-i\pi kt/T}}{i\pi k/T} + \frac{2}{i\pi k/T}\left[-\frac{te^{-i\pi kt/T}}{i\pi k/T} - \frac{e^{-i\pi kt/T}}{(i\pi k/T)^2}\right] \\
	\end{align*}
	Daí
	\begin{align*}
		\beta_k &= \frac{1}{2T}\left[-\frac{T^2e^{-i\pi k}}{i\pi k/T} - \frac{2Te^{-i\pi k}}{(i\pi k/T)^3} + \frac{T^2e^{-i\pi k}}{i\pi k/T} - \frac{2Te^{i\pi k}}{(i\pi k/T)^2} + \frac{2 e^{i\pi k}}{(i\pi k/T)^3}\right] \\
		&= \frac{-2(-1)^k}{(i\pi k/T)^2}
	\end{align*}
	\item \textbf{Exemplo com descontinuidades}: $f(t) = $ sinal$(t)$ para $t\in [-1, 1]$. O conjunto $S$ é completo também em relação ao espaço vetorial das funções com uma quantidade finita de pontos de descontinuidade. Isso significa que existem $\{\alpha_k\}_{k\in\mathbb{Z}}$ tais que $f(t) = \sum\limits_{k=-\infty}^\infty \alpha_k e^{i\pi kt}$ e pela ortogonalidade da família $S$ segue $\alpha_k = \frac{(f, s^{(k)})}{(s^{(k)}, s^{(k)})} = \frac{1}{2}\int\limits_{-1}^1$sinal$(t)e^{-i\pi kt}dt$ que vale 0 se $k$ é par e $\frac{2}{\pi k}$ se $k$ é ímpar (exercício).
\end{enumerate}

\subsection{O espaço $L^2[a,b]$}

\begin{itemize}
	\item funções integráveis (Lebegue - aceita algumas funções com infinitos pontos de descontinuidade)
	\item funções de ``energia finita'' ($\|f\|^2 = \int\limits_a^b |f(t)|^2dt$)
	\item o conjunto das exponenciais complexas associadas ao intervalo $[a, b]$ é completo em relação a $L^2[a, b]$.
\end{itemize}

Podemos adaptar as exponenciais complexas $s^{(k)}(t) = e^{i\pi kt/T}$ (associadas ao intervalo $[-T, T]$) a um intervalo genérico $[a, b]$ através do mapeamento linear $\varphi(t) = \frac{t-a}{b-a}2T - T$ e considerando as funções

\begin{align*}
	s^{(k)}_\varphi (t) &= s^{(k)}(\varphi(t)) = e^{i\pi k\left[\frac{t-a}{b-a}2 - 1\right]}
\end{align*}

E $e^{i\pi k\left[\frac{t-a}{b-a}2 - 1\right]}$ é uma família de exponenciais complexas ortogonal e completa em relação ao intervalo $[a, b]$.

\textbf{\underline{Obs}: Tudo o que foi desenvolvido anteriormente também vale para um retângulo (ler no livro).}
