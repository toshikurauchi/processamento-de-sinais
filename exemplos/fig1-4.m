# Figura 1.4
# Define um domínio espacial com 201x201 pontos
x = 0:(1/200):1;
# Lembre-se que os índices crescentes da matriz
# refletem valores descrescentes do eixo vertical
y = 1:-(1/200):0;
# Calcula a função do exemplo da página 7
f = 1.5*cos(7*y)'*cos(2*x)+ 0.75*sin(3*y)'*cos(5*x) \
-1.3*cos(15*y)'*sin(9*x) + 1.1*sin(11*y)'*sin(13*x);
# Plota o gráfico 3D da função f(x,y)
figure(1);
mesh(x,y,f);
colormap("gray");
# Normaliza f
lb = min(min(f));
ub = max(max(f));
g = (f-lb)/(ub-lb);
# Exibe (em outra janela) a imagem representada por f
figure(2);
imshow(g);