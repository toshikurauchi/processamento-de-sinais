\section{Funções básicas de representação em espaços vetoriais}

\subsection{Caso discreto}

Base ``canônica'':

\begin{align*}
	\epsilon^j &= \begin{pmatrix}
	0\\
	\vdots\\
	0\\
	1\\
	0\\
	\vdots\\
	0
	\end{pmatrix}&\text{ no caso de }\mathbb{R}^N\text{ ou }\mathbb{C}^N \\
	\epsilon^{i,j} &= \begin{pmatrix}
	0&\dots&0&&\\
	\vdots&\ddots&&&\\0&\dots&1&&\\&&0&\dots&0\\&&\vdots&\ddots&\vdots\\&&0&\dots&0
	\end{pmatrix}&\text{ no caso de }\mathbb{R}^N\text{ ou }\mathbb{C}^N \\
	\epsilon^j(k) &= \left\{\begin{matrix}
		1&\text{se }k = j \\
		0&\text{se }k \neq j
	\end{matrix}
	\right.
	&\text{ no caso das sequências infinitas ou bi-infinitas} \\
	\epsilon^{i,j}(k,l) &= \left\{\begin{matrix}
		1&\text{se }k = i\text{ e } l=j \\
		0&\text{caso contrário}
	\end{matrix}
	\right.
	&\text{ no caso das sequências infinitas ou bi-infinitas}
\end{align*}

\subsection{Funções ``senoidais''}

\begin{align*}
	\sin(\omega t), \cos(\omega t), \sin(\omega t+\varphi), \text{ etc.}
\end{align*}

que correspondem a projeções de movimentos circulares uniformes.

\begin{center}
\includegraphics[width=0.7\textwidth]{images/movCirc.pdf}
\end{center}

Considere $z(t)\in\mathbb{C}$ dado por

\begin{align*}
	z(t) &= \cos(\omega t) + i\sin(\omega t)
\end{align*}

Note que

\begin{align*}
	z(t)^2 &= [\cos(\omega t) + i\sin(\omega t)][\cos(\omega t) + i\sin(\omega t)] \\
	&= \cos^2(\omega t)+2i\sin(\omega t)\cos(\omega t) + i^2\sin^2(\omega t) \\
	&= \frac{1}{2} \cos(2\omega t)+\frac{1}{2}\cos(0) + 2i\left[\frac{1}{2}\sin(2\omega t) + \frac{1}{2}\sin(0)\right] - \left[\frac{1}{2}\cos(0) - \frac{1}{2}\cos(2\omega t)\right] \\
	&= \cos(2\omega t) = i\sin(2\omega t) \\
	&= z(2t)
\end{align*}

\subsection{Relação de Euler}

\begin{align*}
	z(t) &= e^{i\omega t} = \cos(\omega t) + i\sin(\omega t) \\
	&= e^{ix} = \cos{x} + i\sin{x}
\end{align*}

\subsubsection{Exemplo de aplicação}

\begin{align*}
	z(t)^2 = [e^{i\omega t}]^2 = e^{i2\omega t} = z(2t)
\end{align*}

\subsubsection{Dedução da relação de Euler usando séries de Taylor}

Supondo que existam $\{a_k\}_{k=0}^\infty \subseteq \mathbb{C}$:

\begin{align*}
	f(x) &= \sum\limits_{k=0}^\infty a_kx^k
\end{align*}

Se $x=0\Rightarrow f(0) = a_00^0 = a_0$, $f'(x) = \sum\limits_{k=1}^\infty ka_kx^{k-1}\Rightarrow f'(0) = a_1$, $f''(x) = \sum\limits_{k=2}^\infty k(k-1)a_kx^{k-2}\Rightarrow f''(0) = 2\cdot1\cdot a_2 \Rightarrow a_2 = \frac{f''(0)}{2}$. Em geral temos $a_k = \frac{f^{(k)}(0)}{k!}$ de onde $f(x) = \sum\limits_{k=0}^\infty \frac{f^(k)(0)}{k!}x^k$ sempre que existir a representação de Taylor (McLaurin).

\subsubsection{Representações de Taylor para $\sin, \cos, e^x$}

\begin{align*}
	\sin(x) &= \sum\limits_{k=0}^\infty \frac{\sin^(k)(0)}{k!}x^k \\
	&= \frac{\sin(0)}{0!}x^0 + \frac{\cos(0)}{1!}x^1 - \frac{\sin(0)}{2!}x^2 - \frac{\cos(0)}{3!}x^3 + \frac{\sin(0)}{4!}x^4 + \dots \\
	&= x - \frac{x^3}{3!} + \frac{x^5}{5!} - \frac{x^7}{7!} + \dots
\end{align*}

\begin{align*}
	\cos(x) &= \sum\limits_{k=0}^\infty \frac{\cos^(k)(0)}{k!}x^k \\
	&= \frac{\cos(0)}{0!}x^0 - \frac{\sin(0)}{1!}x^1 - \frac{\cos(0)}{2!}x^2 + \frac{\sin(0)}{3!}x^3 + \frac{\cos(0)}{4!}x^4 + \dots \\
	&= 1 - \frac{x^2}{2!} + \frac{x^4}{4!} - \frac{x^6}{6!} + \dots
\end{align*}

\begin{align*}
	e^x = \sum\limits_{k=0}^\infty \frac{[e^x]^{(k)}(0)}{k!}x^k = \sum\limits_{k=0}^\infty\frac{x^k}{k!}
\end{align*}

Substituindo $x=i\omega t$,

\begin{align*}
	e^{iwt} &= \sum\limits_{k=0}^\infty \frac{(i\omega t)^k}{k!} \\
	&= \sum\limits_{k=0}^\infty i^k\frac{(\omega t)^k}{k!} \\
	&= 1 + i(\omega t) - \frac{(\omega t)^2}{2!} - i\frac{(\omega t)^3}{3!} + \frac{(\omega t)^4}{4!} + i\frac{(\omega t)^5}{5!} - \frac{(\omega t)^6}{6!} - i\frac{(\omega t)^7}{7!} + \dots \\
	&= [1 - \frac{(\omega t)^2}{2!} + \frac{(\omega t)^4}{4!} - \frac{(\omega t)^6}{6!} + \dots] \\
	&+ i[\omega t - \frac{(\omega t)^3}{3!} + \frac{(\omega t)^5}{5!} - \frac{(\omega t)^7}{7!} + \dots] \\
	&= \cos(\omega t) + i\sin(\omega t)
\end{align*}

Note que $z(t) = e^{i\omega t}$ representa um movimento circular uniforme de raio unitário e velocidade angular $\omega\frac{\text{rad}}{\text{seg}}$. Note que $e^{-i\omega t} = cos(-\omega t) + i\sin(-\omega t) = \cos(\omega t) - i\sin(\omega t)$. Daí:

\begin{align*}
	\cos(\omega t) &= \frac{1}{2}[e^{i\omega t} + e^{-i\omega t}] \\
	\sin(\omega t) &= \frac{1}{2i}[e^{i\omega t}-e^{-i\omega t}]
\end{align*}

\subsubsection{Famílias de funções básicas}

\begin{tabular}{ll}
	Base de funções reais: &$\{\sin(\omega t), \cos(\omega t)|\omega \geq 0\}$ \\
	Base das exponenciais complexas: &$\{e^{i\omega t}|\omega\in\mathbb{R}\}$
\end{tabular}

\subsubsection{Periodicidade de $e^{i\omega t}$}

O período (comprimento de onda) $\lambda$ é o menor valor positivo que verifica a seguinte propriedade:

\begin{align*}
	\lambda &= \text{ período de }e^{i\omega t} \Leftrightarrow e^{i\omega(t+\lambda)} = e^{i\omega t}, \forall t\in\mathbb{R}
\end{align*}

Note que 

\begin{align*}
	e^{i\omega t}e^{i\omega\lambda} &= e^{i\omega(t+\lambda)} = e^{i\omega t}, \forall t\in\mathbb{R} \\
	\Rightarrow e^{i\omega\lambda} &= 1 \\
	\Rightarrow |\omega|\lambda &= 2\pi \\
	\Rightarrow \lambda &= \frac{2\pi}{|\omega|}
\end{align*}

A frequência é dada por:

\begin{align*}
	q &= \frac{1}{\lambda}
\end{align*}

que é o número de oscilações completas por unidade da variável $t$. Se $t$ é tempo, $q$ é medido em Hz. Se $t$ é espaço, $q$ é medido em ciclos/unidade espacial.

Note que $q = \frac{1}{\lambda} = \frac{|\omega|}{2\pi}$. A função básica $e^{i\omega t}$ pode ser expressada equivalentemente como $e^{2\pi\cdot i\cdot q\cdot t}$ (admitindo que $q$ poderia ser negativo também).

Vamos mostrar (mais adiante) que se $f:[0,T]\rightarrow\mathbb{R}$ (ou $\mathbb{C}$) é limitada e tiver uma quantidade finita de descontinuidades, existem constantes $\{c_k\}_{k\in\mathbb{Z}}\subseteq\mathbb{C}$ tais que $f(t) = \sum\limits_{k=-\infty}^{\infty}c_ke^{2\pi i k (\frac{1}{T})t}$

\underline{\textbf{Outro exemplo:}} se $f:[-T,T]\rightarrow\mathbb{R}$ (ou $\mathbb{C}$) e é limitada e contínua por trechos, então $\exists\{c_k\}_{k\in\mathbb{Z}}\subseteq\mathbb{C}$ tais que $f(t) = \sum\limits_{k=-\infty}^{\infty}c_ke^{2\pi i k (\frac{1}{2T})t}$.

\subsection{Exponenciais complexas em 2 dimensões}

\begin{align*}
	f_{\alpha,\beta}(x, y) &= e^{i(\alpha x + \beta y)} = e^{i\alpha x}e^{i\beta y} \\
	&= \cos(\alpha x + \beta y) + i\sin(\alpha x + \beta y) \\
	&= \cos(\alpha x)\cos(\beta y) - \sin(\alpha x)\sin(\beta y) + i(\sin(\alpha x)\cos(\beta y) + \cos(\alpha x)\sin(\beta y))
\end{align*}

\subsubsection{Famílias de funções básicas em 2D}

\begin{align*}
	&\{e^{i(\alpha x + \beta y)}|\alpha,\beta\in\mathbb{R}\} \\
	&\{\cos(\alpha x + \beta y), \sin(\alpha x + \beta y) | \alpha,\beta\geq 0\text{, ou }\alpha\text{ e }\beta\text{ com sinais diferentes}\} \\
	&\{\cos(\alpha x)\cos(\beta y), \sin(\alpha x)\sin(\beta y), \sin(\alpha x)\cos(\beta y), \cos(\alpha x)\sin(\beta y) | \alpha, \beta\geq 0\}
\end{align*}

Veremos mais para a frente que funções $f(x,y):[0,T]\times [0,S]\rightarrow\mathbb{R}$ (ou $\mathbb{C}$) limitadas e contínuas por trechos podem ser escritos como:

\begin{align*}
	f(x, y) &= \sum\limits_{k=-\infty}^{\infty}\sum\limits_{l=-\infty}^{\infty}c_{k,l}e^{i(2\pi k\frac{1}{T}x + 2\pi l\frac{1}{S}y)}
\end{align*}

\subsection{Rebatimento (\emph{Aliasing})}

Considere a função:

\begin{align*}
	f(t) &= e^{i\omega t}
\end{align*}

discretizada usando-se $N$ pontos por segundo:

\begin{align*}
	f[k] &= e^{i\omega \frac{k}{N}}, k\in\mathbb{Z}
\end{align*}

Seria possível que uma função $g(t) = e^{i\varphi t}$ produzisse valores amostrados $g[k] = e^{i\varphi\frac{k}{N}}, k\in\mathbb{Z}$ tais que $f[k] = g[k]$?

\begin{align*}
	&e^{i\omega\frac{k}{N}} = e^{i\varphi\frac{k}{N}}, \forall k\in\mathbb{Z} \\
	\Leftrightarrow & e^{i\omega\frac{k}{N}}e^{-i\varphi\frac{k}{N}} = 1, \forall k\in\mathbb{Z} \\
	\Leftrightarrow & e^{i(\omega-\varphi)\frac{k}{N}} = 1, \forall k\in\mathbb{Z} \\
	\Leftrightarrow & \left[e^{i(\omega-\varphi)\frac{1}{N}}\right]^k = 1, \forall k\in\mathbb{Z} \\
	\Leftrightarrow & e^{i(\omega-\varphi)\frac{1}{N}} = 1 \\
	\Leftrightarrow & (\omega - \varphi)\frac{1}{N} = m2\pi, \text{ para algum }m\in\mathbb{Z}
\end{align*}

ou seja, as duas funções amostradas $f[k]$ e $g[k]$ serão iguais (em todas as amostras) se e somente se, para algum $m\in\mathbb{Z}$, $\omega-\varphi = mN2\pi$. Usando frequências em Hz ($\omega = 2\pi q$ e $\varphi = 2\pi r$), $q-r = mN$.

Assim, todas as exponenciais $\dots, \omega - 4\pi N, \omega - 2\pi N, \omega, \omega + 2\pi N, \omega + 4\pi N, \dots$ (frequência angular: rad/s), ou $\dots, q-2N, q-N, q, q+N, q+2N, \dots$ (frequência em Hz), ou $\dots, \frac{\omega}{N} - 4\pi, \frac{\omega}{N} - 2\pi, \frac{\omega}{N}, \frac{\omega}{N} + 2\pi, \frac{\omega}{N} + 4\pi, \dots$ (frequência em rad/amostra) são idênticas quando amostradas a $N$ Hz.

É usual restringirmos a faixa de frequências representáveis em sinais amostrados a $N$ Hz ao intervalo $\left(-\frac{N}{2}, \frac{N}{2}\right]$ Hz ou $\left(-\pi N, \pi N\right]$ radianos/segundo.

A mesma faixa em rad/amostra é $\left(-\pi, +\pi\right]$. Neste contexto, a máxima frequência representável, chamada de frequência de Nyquist, é de $\frac{N}{2}$ Hz = $\pi N$ rad/seg = $\pi$ rad/amostra.

\underline{\textbf{Critério de Nyquist}}: para representar (digitalmente) componentes senoidais com frequência de $X$ Hz é necessário tomar pelo menos $2X$ amostras por segundo, ou 2 amostras por período do sinal.

Na conversão analógico $\rightarrow$ digital é necessário garantir que a entrada contenha apenas frequências representáveis, o que é feito por um filtro passa-baixa com frequência de corte $\frac{N}{2}$ Hz:

\begin{center}
\includegraphics[width=0.7\textwidth]{images/filtro-passa-baixa.pdf}
\end{center}
	
\subsubsection{Rebatimento em imagens}

\begin{align*}
	f(x,y) &= e^{i(\alpha x + \beta y)} \\
	&= e^{i\alpha x}e^{i\beta y}
\end{align*}

Amostrando-se o sinal com $N$ amostras/unidade em relação à variável $x$ e $M$ amostras/unidade em relação à variável $y$, teremos:

\begin{align*}
	f[k, l] = e^{i\alpha\frac{k}{N}}e^{i\beta\frac{k}{M}}
\end{align*}

Em relação à variável $x$, todas as frequências abaixo são equivalentes:

\begin{align*}
\{\dots, \alpha - 4\pi N, \alpha - 2\pi N, \alpha, \alpha + 2\pi N, \alpha + 4\pi N, \dots\} &\text{(rad/unidade espacial)} \\
\{\dots, p - 2N, p - N, p, p + N, p + 2 N, \dots\} &\text{(ciclos/unidade espacial)}
\end{align*}

em relação a $y$, temos:

\begin{align*}
\{\dots, \beta - 4\pi M, \beta - 2\pi M, \beta, \beta + 2\pi M, \beta + 4\pi M, \dots\} &\text{(rad/unidade espacial)} \\
\{\dots, q - 2M, q - M, q, q + M, q + 2 M, \dots\} &\text{(ciclos/unidade espacial)}
\end{align*}

onde $p = \frac{\alpha}{2\pi}$ e $q = \frac{\beta}{2\pi}$.

O universo das frequências representáveis com a amostragem de $N\times M$ (pixels/(unidade espacial)$^2$) pode ser descrito como:

\begin{center}
\includegraphics[width=0.7\textwidth]{images/amostragemNM.pdf}
\end{center}

\textbf{Exemplo do livro:}

\begin{align*}
	f(x, y) &= 256\sin(2\pi (50x + 70y))
\end{align*}

amostrado em 60$\times$60, 100$\times$100, 300$\times$300 e 1000$\times$1000 amostras.

\subsubsection{Teorema da amostragem (Shannon - Nyquist)}

Se $f:[a,b]\rightarrow\mathbb{C}$ só contém componentes senoidais entre $\left(-\frac{N}{2}, \frac{N}{2}\right]$ Hz então é possível reconstruir perfeitamente o sinal analógico a partir da sequência amostrada $f[k] = f\left(\frac{k}{N}\right), \forall k$.
