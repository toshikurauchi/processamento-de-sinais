\section{Bancos de Filtros}

\begin{center}
	\includegraphics[width=0.5\textwidth]{images/bancoDeFiltros.pdf}
\end{center}

\underline{\textbf{Exemplo}}:

\begin{align*}
	l:&\text{ filstro da média} \\
	&\text{ }\left(\frac{1}{2}, \frac{1}{2}, 0, 0, \dots\right) \\
	l:&\text{ filstro da diferença} \\
	&\text{ }\left(\frac{1}{2}, -\frac{1}{2}, 0, 0, \dots\right)
\end{align*}

Para $(\dots, x_{-2}, x_{-1}, x_0, x_1, x_2, \dots) = x\in l^2(\mathbb{Z})$ temos

\begin{align*}
	x_l = x*l &= \left(\dots, \frac{x_{-1}+x_{-2}}{2}, \frac{x_{0}+x_{-1}}{2}, \frac{x_{1}+x_{0}}{2}, \dots\right) \\
	x_h = x*h &= \left(\dots, \frac{x_{-1}-x_{-2}}{2}, \frac{x_{0}-x_{-1}}{2}, \frac{x_{1}-x_{0}}{2}, \dots\right) \\
	x_l + x_h &= \left(\dots, x_{-1}, x_0, x_1, \dots\right) = x
\end{align*}

Para $k\in\mathbb{Z}$ qualquer

\begin{align*}
	(X_l)_k &= \frac{x_k + x_{k-1}}{2} \\
	(X_h)_k &= \frac{x_k - x_{k-1}}{2}
\end{align*}

de onde

\begin{align*}
	(x_l + x_h)_k &= x_k \\
	(x_l - x_h)_k &= x_{k-1}
\end{align*}

Assim podemos descartar metade das componentes de $x_l$ e $x_h$ (por exemplo os índices $k = 2n+1, \forall n\in\mathbb{Z}$) e ainda conseguir reconstruir $x$ perfeitamente.

\subsection{Transformada do banco de filtros de Haar}

Dado $x\in l^2(\mathbb{Z})$, e dois filtros $l = \left(\dots, 0, \frac{1}{2}, \frac{1}{2}, 0, \dots\right)\in l^2(\mathbb{Z})$ e $h = \left(\dots, 0, \frac{1}{2}, -\frac{1}{2}, 0, \dots\right)\in l^2(\mathbb{Z})$, obtemos

\begin{align*}
	x*l &= \left(\dots, \frac{x_k + x_{k-1}}{2}, \dots\right) \\
	x*h &= \left(\dots, \frac{x_k - x_{k-1}}{2}, \dots\right)
\end{align*}

Considere o operador de subamostragem (downsampling)

\begin{equation*}
	D: l^2(\mathbb{Z}) \rightarrow l^2(\mathbb{Z})
\end{equation*}

onde

\begin{equation*}
	(D(y))_k = y_{2k}, \forall k\in\mathbb{Z}
\end{equation*}

Definimos

\begin{align*}
	X_l &= D(x*l) = \left(\dots, \frac{x_{2k} + x_{2k-1}}{2}, \dots\right) \\
	X_h &= D(x*h) = \left(\dots, \frac{x_{2k} - x_{2k-1}}{2}, \dots\right)
\end{align*}

A transformada de $x$ será dada pelo par $(X_l, X_h)\in l^2(\mathbb{Z})\times l^2(\mathbb{Z})$. Para inverter essa transformação, consideraremos um algoritmo da forma

\begin{center}
	\includegraphics[width=0.5\textwidth]{images/bancoDeFiltrosHaar.pdf}
\end{center}

Onde $[\uparrow 2]$ denota o operador de superamostragem (upsampling)

\begin{equation*}
	U: l^2(\mathbb{Z})\rightarrow l^2(\mathbb{Z})
\end{equation*}

dado por

\begin{equation*}
	(U(y))_k = \left\{\begin{array}{ll}
		y_{\frac{k}{2}} & \text{ se }k\text{ é par} \\
		0 & \text{ caso contrário.}
	\end{array}\right.
\end{equation*}

Os filtros de síntese $l_s$ (passa-baixas) e $h_s$ (passa-altas) são dadas pelas sequências

\begin{align*}
	l_s &= (\dots, 0, 1, 1, 0, \dots) \Rightarrow y_k = w_k + w_{k+1} \\
	l_h &= (\dots, 0, -1, 1, 0, \dots) \Rightarrow y_k = w_k - w_{k+1}
\end{align*}

Onde os 1's se encontram nas posições $n=-1$ e $n=0$. Observe que

\begin{align*}
	U(X_l) &= \left(\dots, \left\{\begin{array}{ll}
		\frac{x_k + x_{k-1}}{2}, & k \text{ par} \\
		0, & k \text{ ímpar}
	\end{array}\right\}, \dots \right) \\
	U(X_h) &= \left(\dots, \left\{\begin{array}{ll}
		\frac{x_k - x_{k-1}}{2}, & k \text{ par} \\
		0, & k \text{ ímpar}
	\end{array}\right\}, \dots \right)
\end{align*}

e assim

\begin{align*}
	[U(X_l)*l_s]_k &= \left\{\begin{array}{ll}
		\frac{x_k + x_{k-1}}{2}, & k \text{ par} \\
		\frac{x_{k+1} + x_k}{2}, & k \text{ ímpar}
	\end{array}\right. \\
	[U(X_h)*h_s]_k &= \left\{\begin{array}{ll}
		\frac{x_k - x_{k-1}}{2}, & k \text{ par} \\
		\frac{-x_{k+1} + x_k}{2}, & k \text{ ímpar}
	\end{array}\right.
\end{align*}

Assim temos que

\begin{equation*}
	[U(X_l)*l_s] + [U(X_h)*h_s] = x,
\end{equation*}

correspondendo à transformação inversa do banco de filtros de Haar

\underline{\textbf{Obs}}: Os filtros $l_s$ e $h_s$ desse exemplo são não-causais (possuem coeficientes diferentes de 0 em índices negativos), mas o processo da transformada inversa pode ser tornado causal atrasando em 1 amostra o resultado. Assim

\begin{align*}
	\tilde{l}_s &= (\dots, 0, 1, 1, 0, \dots) \\
	\tilde{h}_s &= (\dots, 0, -1, 1, 0, \dots)
\end{align*}

Onde os 1's se encontram nas posições $n=0$ e $n=1$. Teríamos

\begin{equation*}
	[U(X_l)*\tilde{l}_s] + [U(X_h)*\tilde{h}_s]_k = x_{k-1}, \forall k\in\mathbb{Z}
\end{equation*}

ou seja,

\begin{equation*}
	U(X_l)*l_s + U(X_h)*h_s = S(x)
\end{equation*}

onde $S:l^2(\mathbb{Z})\rightarrow l^2(\mathbb{Z})$ é o operador de atraso de 1 amostra $[[S(y)]_{k} = y_{k-1} \forall k]$.

Em geral podemos aceitar um esquema de transformação inversa (resíntese) que produz uma versão atrasada em $m$ amostras do sinal $x$ original:

\begin{equation*}
	U(X_l)*l_s + U(X_h)*h_s = S^m(x)
\end{equation*}

(essa condição é chamada de reconstrução perfeita do sinal original com atraso de $m$ amostras).

\subsection{Transformada com bancos de filtros gerais (2 filtros de análise, 2 filtros de síntese)}

Dados filtros $l_a, h_a, l_s, h_s\in l^2(\mathbb{Z})$ podemos definir as transformações $x\rightarrow (X_l, X_h)$ onde $X_l = D(x*l_a)$ e $X_h = D(x*h_a)$ e $(X_l, X_h)\rightarrow \tilde{x}$ onde $\tilde{x} = U(X_l)*l_s + U(X_h)*h_s$.

Diremos que este esquema de análise-ressíntese possui a propriedade de reconstrução perfeita se

\begin{equation*}
	\tilde{x} = U(D(x*l_a))*l_s + U(D(x*h_a))*h_s = S^m(x)
\end{equation*}

para algum $m\in\mathbb{Z}$.

\underline{\textbf{Exemplo 6.3}}: Dados os filtros de análise do banco de filtros de Haar:

\begin{align*}
	l_a &= \left(\dots, 0, \frac{1}{2}, \frac{1}{2}, 0, \dots\right) \\
	h_a &= \left(\dots, 0, \frac{1}{2}, -\frac{1}{2}, 0, \dots\right)
\end{align*}

Onde $\frac{1}{2}$ está nas posições $n=0$ e $n=1$. Podemos usar a condição de reconstrução perfeita (com $m=0$) para definir os filtros de síntese que correspondem a $l_a$ e $h_a$:

Temos

\begin{align*}
	v_k = [U(D(x*l_a))]_k &= \left\{\begin{array}{ll}
		\frac{x_k + x_{k-1}}{2}, & k\text{ par} \\
		0, & k\text{ ímpar}
	\end{array}\right. \\
	w_k = [U(D(x*h_a))]_k &= \left\{\begin{array}{ll}
		\frac{x_k - x_{k-1}}{2}, & k\text{ par} \\
		0, & k\text{ ímpar}
	\end{array}\right.
\end{align*}

\subsubsection{Reconstrução Perfeita}

Supondo desconhecidos os filtros de síntese $l_s$ e $h_s$, poderíamos escrever a condição de reconstrução perfeita (com $m = 0$) como:

\begin{align*}
	x_n &= \tilde{x}_n \\
	&= [U(D(x*l_a))*l_s]_n + [U(D(x*lh_a))*h_s]_n \\
	&= \sum\limits_{k=-\infty}^\infty v_k (l_s)_{n-k} + \sum\limits_{-\infty}^\infty w_k(h_s)_{n-k} \\
	&= \sum\limits_{k\text{ par}} \left(\frac{x_k+x_{k-1}}{2}\right)(l_s)_{n-k} + \sum\limits_{k\text{ par}} \left(\frac{x_k-x_{k-1}}{2}\right)(h_s)_{n-k} \\
	&= \frac{1}{2}\sum\limits_{k\text{ par}} [(l_s)_{n-k} + (h_s)_{n-k}]x_k + [(l_s)_{n-k} - (h_s)_{n-k}]x_{k-1}, \forall k\in l^2(\mathbb{Z}), \forall n\in\mathbb{Z}
\end{align*}

Considere $x = \delta$ onde $\delta_k = \left\{\begin{array}{ll}
	1 & k = 0 \\
	0 & k \neq 0
\end{array}\right.$ para $n=0$:

\begin{equation*}
	1 = \frac{1}{2}((l_s)_0 + (h_s)_0)
\end{equation*}

para $n\neq 0$, $n$ par:

\begin{equation*}
	0 = \frac{1}{2}((l_s)_n + (h_s)_n)
\end{equation*}

Considere $x = S(\delta)$ $\left(x_k = \left\{\begin{array}{ll}
	1 & \text{ se }k = 1 \\
	0 & \text{ se }k\neq 1
\end{array}\right.\right)$

teremos para $n = 2$:

\begin{align*}
	0 &= \frac{1}{2}((l_s)_0 - (h_s)_0)
\end{align*}

e para $n\neq 2$, $n$ par

\begin{align*}
	0 &= \frac{1}{2}((l_s)_{n-2} - (h_s)_{n-2})
\end{align*}

Destas equações temos

\begin{equation*}
	(l_s)_0 = (h_s)_0 = 1
\end{equation*}

\underline{\textbf{Exemplo}}: Le Gall 5/3

\begin{align*}
	l_a &= \left(\dots, 0, -\frac{1}{8}, \frac{1}{4}, \frac{3}{4}, \frac{1}{4}, -\frac{1}{8}, 0, \dots\right) \\
	h_a &= \left(\dots, 0, -\frac{1}{2}, 1, -\frac{1}{2}, 0, \dots\right) \\
	l_s &= \left(\dots, 0, \frac{1}{2}, 1, \frac{1}{2}, 0, \dots\right) \\
	h_s &= \left(\dots, 0, -\frac{1}{8}, -\frac{1}{4}, \frac{3}{4}, -\frac{1}{4}, -\frac{1}{8}, 0, \dots\right)
\end{align*}

\subsubsection{Propriedades de Filtros}

\begin{align*}
	\text{(Haar)} & \left\{\begin{array}{l}
		(l_s)_k = C(l_a)_{-k} \\
		(h_s)_k = C(h_a)_{-k}
	\end{array}\right\}\forall k \\
	\text{(Le Gall)} & \left\{\begin{array}{l}
		(l_s)_k = (-1)^k(l_a)_k \\
		(h_s)_k = (-1)^k(h_a)_k
	\end{array}\right\}\forall k \\
	\text{filtros adjuntos (time reversal)} & \left\{\begin{array}{l}
		(l_s)_k = (l_a)_{-k} \\
		(h_s)_k = (h_a)_{-k}
	\end{array}\right\}\forall k \\
\end{align*}

\underline{\textbf{Exemplo}}: versão ortogonal (adjunta) de Haar:

\begin{align*}
	l_a &= \left(\dots, 0, \frac{1}{\sqrt{2}}, \frac{1}{\sqrt{2}}, 0, \dots\right) \\
	h_a &= \left(\dots, 0, \frac{1}{\sqrt{2}}, -\frac{1}{\sqrt{2}}, 0, \dots\right) \\
	l_s &= \left(\dots, 0, \frac{1}{\sqrt{2}}, \frac{1}{\sqrt{2}}, 0, \dots\right) \\
	h_s &= \left(\dots, 0, -\frac{1}{\sqrt{2}}, \frac{1}{\sqrt{2}}, 0, \dots\right)
\end{align*}

\subsubsection{Transformada Multietapas usando bancos de filtros (versão iterada)}

\begin{equation*}
	x\in l^2(\mathbb{Z}) \rightarrow \bigbox \rightarrow (X_l, X_h)\in l^2(\mathbb{Z})\times l^2(\mathbb{Z})
\end{equation*}

Onde $X_l$ são os coeficientes de aproximação e $X_h$ os coeficientes de detalhe. Podemos passar $X_l$ e $X_h$ pela mesma transformação, obtendo:

\begin{align*}
	X_l &\rightarrow \bigbox \rightarrow (X_{ll}, X_{lh})\in l^2(\mathbb{Z})\times l^2(\mathbb{Z}) \\
	X_h &\rightarrow \bigbox \rightarrow (X_{hl}, X_{hh})\in l^2(\mathbb{Z})\times l^2(\mathbb{Z})
\end{align*}

A transformada pode ser aplicada iterativamente sobre os coeficientes de aproximação.

\subsection{DWT para sinais finitos}

Seja $x\in\mathbb{C}^N$; queremos construir um sinal $\tilde{x}\in l^2(\mathbb{Z})$ que represente $x$. Algumas opções:

\begin{itemize}
	\item extensão com zeros
	\begin{equation*}
		\tilde{x}_k = \left\{\begin{array}{ll}
			x_k, & \text{ }k\in\{0, 1, \dots, N-1\} \\
			0, & \text{ c.c.}
		\end{array}\right.
	\end{equation*}
	\item extensão periódica
	\begin{equation*}
		\tilde{x}_k = x_{k\mod N}, \forall k\in\mathbb{Z}\text{ (não está em }l^2(\mathbb{Z}))
	\end{equation*}
	\item espelhamento + extensão periódica
	\begin{align*}
		\hat{x}_k &= \left\{\begin{array}{ll}
			x_k, & k = 0, \dots, N-1 \\
			x_{2N-k-1}, & k = N, \dots, 2N-1 
		\end{array}\right. \\
		\tilde{x}_k &= \hat{x}_{k\mod 2N}
	\end{align*}
\end{itemize}

A título de exemplo, desenvolveremos a DWT em $\mathbb{C}^N$ a partir da extensão periódica (caso 2 acima). Trabalharemos com extensões ``pseudo-periódicas'':

\begin{equation*}
	\tilde{x}_k = \left\{\begin{array}{ll}
		x_{k\mod N}, & -qN\leq k\leq +qN \\
		0, & \text{c.c.}
	\end{array}\right.
\end{equation*}

Para $q$ grande e $\tilde{x}_k\in l^2(\mathbb{Z})$.

\underline{\textbf{Teorema}}: A utilização de uma transformada baseada em um banco de filtros com reconstrução perfeita no sinal com reconstrução perfeita no sinal $\tilde{x}\in l^2(\mathbb{Z})$ equivale à mesma sequência de operações em $\mathbb{C}^N$ a partir de $x\in\mathbb{C}^N$, interpretando as convoluções como convoluções circulares.

\underline{\textbf{Prova}}: Pela propriedade de reconstrução perfeita em $l^2(\mathbb{Z})$, temos que os sinais

\begin{align*}
	\tilde{X}_l &= D(\tilde{x}*l_a) \\
	\tilde{X}_h &= D(\tilde{x}*h_a)
\end{align*}

onde $*$ é a convolução em $l^2(\mathbb{Z}$, permitem a reconstrução de $\tilde{x}$ através da expressão

\begin{equation*}
	\tilde{x} = U(\tilde{X}_k)*l_s + U(\tilde{X}_h)*h_s
\end{equation*}

Dado $x\in\mathbb{C}^N$, podemos definir

\begin{equation*}
	X_l = D(x*l_a)\in\mathbb{C}^{N/2}
\end{equation*}

onde $*$ é a convolução circular ($(x*l_a)_n = \sum\limits_{k=0}^{N-1}(l_a)_k x_{n-k}$), $D\in\mathbb{R}^{\frac{N}{2}\times N}$ é uma matriz com 1 nas posições $(i, 2i-1)$ e 0 nas demais.

\begin{equation*}
	X_h = D(x*h_a)\in\mathbb{C}^{N/2}
\end{equation*}

Na etapa de ressíntese, vamos combinar $X_l$ e $X_h$ através da expressão

\begin{equation*}
	U(X_l)*l_s + U(X_h)*h_s = \hat{x}
\end{equation*}

e $U = D^T\in\mathbb{R}^{N\times\frac{N}{2}}$. Queremos mostrar que $\hat{x}$ (versão ressintetizada em domínio finito) é idêntico a $x$. Para fazer isso, vamos observar as correspondências entre $X_k$ e $\tilde{X}_l$ e $X_{h}$ e $\tilde{X}_h$. Note que

\begin{align*}
	(\tilde{x}*l_a)_n &= \sum\limits_{-\infty}^\infty (l_a)_k\tilde{x}_{n-k} \\
	\text{(pois }(l_a)_k = 0\text{ se }k\neq 0, 1, \dots, N-1) &= \sum\limits_{k=0}^{N-1}(l_a)_k\tilde{x}_{n-k} (\forall n\in\mathbb{Z})
\end{align*}

se $n\in\{0, 1, \dots, N-1\}$

\begin{align*}
	(\tilde{x}*l_a)_n &= \sum\limits_{k=0}^{N-1}(l_a)_k\tilde{x}_{n-k} \\
	&= \sum\limits_{k=0}^{N-1}(l_a)_k x_{n-k} \text{(*)}\\
	&= (x*l_a)_n\text{ (convolução circular)}
\end{align*}

Um argumento semelhante mostra que $\text{(convolução circular) }(x*h_a)_n = (\tilde{x}*h_a)_n\text{ (convolução linear)}$ para $n = 0, 1, \dots, N-1$. A subamostragem $D$ preservará apenas as componentes de índice par, tanto no contexto infinito quanto no finito. Assim, a partir do conhecimento de $X_l, X_h\in\mathbb{C}^{N/2}$, podemos recuperar a informação completa de $\tilde{X}_l, \tilde{X}_h\in l^2(\mathbb{Z})$, sendo que

\begin{align*}
	(\tilde{X}_l)_n &= (X_l)_n, \text{ para }n = 0, 1, \dots, \frac{N}{2} - 1 \\
	(\tilde{X}_h)_n &= (X_h)_n, \text{ para }n = 0, 1, \dots, \frac{N}{2} - 1
\end{align*}

Notando que as convoluções de ressíntese possuem a mesma propriedade (*) em relação às convoluções lineares correspondentes,

\begin{align*}
	\hat{X}_n &= [U(X_l)*l_s + U(X_h)*h_s]_n \text{ (convoluções circulares)} \\
	&= [U(X_l)*l_s]_n + [U(X_h)*h_s]_n \in\mathbb{C}^N \\
	&= [U(X_l)*l_s]_n + [U(X_h)*h_s]_n \in l^2\mathbb{Z}\text{ (convoluções lineares)} \\
	&= [U(\tilde{X}_l)*l_s + U(\tilde{X}_h)*h_s]_n \\
	&= \tilde{x}_n \text{ (pela reconstrução perfeita em }l^2(\mathbb{Z})) \\
	&= x_n
\end{align*}

\underline{\textbf{Exemplo}}: considere a DWT com o banco de filtros de Haar ortogonalizado (que satisfaz $(l_s)_k = (l_a)_{-k}, \forall k$ e $(h_s)_k = (h_a)_{-k}, \forall k$) aplicada a um vetor $x\in\mathbb{C}^4$:

\begin{align*}
	X_l &= D(x*l_a) = D\left(\begin{pmatrix}
		x_0 \\ x_1 \\ x_2 \\ x_3
	\end{pmatrix}*\begin{pmatrix}
		\frac{1}{\sqrt{2}} \\ \frac{1}{\sqrt{2}} \\ 0 \\ 0
	\end{pmatrix}\right) = \begin{pmatrix}
		\frac{x_0}{\sqrt{2}} + \frac{x_3}{\sqrt{2}} \\
		\frac{x_2}{\sqrt{2}} + \frac{x_1}{\sqrt{2}}
	\end{pmatrix} \\
	X_h &= D(x*h_a) = D\left(\begin{pmatrix}
		x_0 \\ x_1 \\ x_2 \\ x_3
	\end{pmatrix}*\begin{pmatrix}
		\frac{1}{\sqrt{2}} \\ -\frac{1}{\sqrt{2}} \\ 0 \\ 0
	\end{pmatrix}\right) = \begin{pmatrix}
		\frac{x_0}{\sqrt{2}} - \frac{x_3}{\sqrt{2}} \\
		\frac{x_2}{\sqrt{2}} - \frac{x_1}{\sqrt{2}}
	\end{pmatrix}
\end{align*}

Assim

\begin{equation*}
	X = W_4^a\begin{pmatrix}
		x_0 \\ x_1 \\ x_2 \\ x_3
	\end{pmatrix}
\end{equation*}

Onde (4 é a dimensão da DWT e $a$ vem de ``análise'')

\begin{equation*}
	W_4^a = \begin{pmatrix}
		\frac{1}{\sqrt{2}} & 0 & 0 & \frac{1}{\sqrt{2}} \\
		0 & \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}} & 0 \\
		\frac{1}{\sqrt{2}} & 0 & 0 & -\frac{1}{\sqrt{2}} \\
		0 & -\frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}} & 0
	\end{pmatrix}
\end{equation*}

Na ressíntese, temos

\begin{align*}
	x &= [U(X_l)*l_s] + [U(X_h)*h_s] \\
	&= \begin{pmatrix}
		X_0 \\ 0 \\ X_1 \\ 0
	\end{pmatrix}\begin{pmatrix}
		\frac{1}{\sqrt{2}} \\ 0 \\ 0 \\ \frac{1}{\sqrt{2}}
	\end{pmatrix} + \begin{pmatrix}
		X_2 \\ 0 \\ X_3 \\ 0
	\end{pmatrix}\begin{pmatrix}
		\frac{1}{\sqrt{2}} \\ 0 \\ 0 \\ -\frac{1}{\sqrt{2}}
	\end{pmatrix} \\
	&= \begin{pmatrix}
		\frac{X_0}{\sqrt{2}} \\ \frac{X_1}{\sqrt{2}} \\ \frac{X_1}{\sqrt{2}} \\ \frac{X_0}{\sqrt{2}}
	\end{pmatrix} + \begin{pmatrix}
		\frac{X_2}{\sqrt{2}} \\ -\frac{X_3}{\sqrt{2}} \\ \frac{X_3}{\sqrt{2}} \\ -\frac{X_2}{\sqrt{2}}
	\end{pmatrix} \\
	&= \begin{pmatrix}
		\frac{X_0 + X_2}{\sqrt{2}} \\ \frac{X_1 - X_3}{\sqrt{2}} \\ \frac{X_1 + X_3}{\sqrt{2}} \\ \frac{X_0 - X_2}{\sqrt{2}}
	\end{pmatrix} \\
	&= W_4^s\begin{pmatrix}
		X_0 \\ X_1 \\ X_2 \\ X_3
	\end{pmatrix}
\end{align*}

Onde ($s$ vem de ``síntese'')

\begin{equation*}
	W_4^s = \begin{pmatrix}
		\frac{1}{\sqrt{2}} & 0 & \frac{1}{\sqrt{2}} & 0 \\
		0 & \frac{1}{\sqrt{2}} & 0 & -\frac{1}{\sqrt{2}} \\
		0 & \frac{1}{\sqrt{2}} & 0 & \frac{1}{\sqrt{2}} \\
		\frac{1}{\sqrt{2}} & 0 & -\frac{1}{\sqrt{2}} & 0
	\end{pmatrix}
\end{equation*}

Observe que:

\begin{enumerate}
	\item $W_4^s = (W_4^a)^T$
	\item todas as colunas (e linhas) de $W_4^s$ e $W_4^a$ são ortogonais (2 a 2)
\end{enumerate}

Logo

\begin{align*}
	W_4^s W_4^a &= I \text{ (pois a iDWT é a inversa da DWT)} \\
	W_4^a (W_4^a)^T &= I \text{ (ortonormalidade do banco de filtros)}
\end{align*}

\subsection{Formulação Matricial da DWT em $\mathbb{C}^N$}

\underline{\textbf{Análise}}: dado $x\in\mathbb{C}^N$,

\begin{align*}
	X &= \begin{pmatrix}
		X_l \\ X_h
	\end{pmatrix} \\
	&= \begin{pmatrix}
		D(x*l_a) \\ D(x*h_a)
	\end{pmatrix} \\
	&= \begin{pmatrix}
		DM_{l_a}x \\ DM_{h_a}x
	\end{pmatrix} \\
	&= \begin{pmatrix}
		DM_{l_a} \\ DM_{h_a}
	\end{pmatrix}x \\
	&= W_N^a x
\end{align*}

\underline{\textbf{Síntese}}: dado $X\in\mathbb{C}^N$,

\begin{align*}
	x &= (U(X_l)*l_s) + (U(X_h)*h_s) \\
	&= M_{l_s}UX_l + M_{h_s}UX_h \\
	&= \begin{pmatrix}
		M_{l_s}U & M_{h_s}U
	\end{pmatrix}\begin{pmatrix}
		X_l \\ X_h
	\end{pmatrix} \\
	&= W_N^s X
\end{align*}

Se um banco de filtros possui a propriedade (filtros adjuntos ou revertidos no tempo)

\begin{align*}
	(l_s)_k &= (l_a)_{-k}, \forall k \\
	(h_s)_k &= (h_a)_{-k}, \forall k
\end{align*}

teremos (exercício 4.9)

\begin{align*}
	M_{l_s} &= (M_{l_a})^T \\
	M_{h_s} &= (M_{h_a})^T
\end{align*}

Neste caso,

\begin{align*}
	W_N^s &= \begin{pmatrix}
		M_{l_s}U & M_{h_s}U
	\end{pmatrix} \\
	&= \begin{pmatrix}
		(M_{l_a})^TU^T & (M_{h_a})^TU^T
	\end{pmatrix} \\
	&= \begin{pmatrix}
		(M_{l_a}U)^T & (M_{h_a}U)^T
	\end{pmatrix} \\
	&= \begin{pmatrix}
		M_{l_a}U \\ M_{h_a}U
	\end{pmatrix}^T \\
	&= (W_N^a)^T
\end{align*}

ou seja, as matrizes $W_N^s$ e $W_N^a$, além de serem a inversa uma da outra, são matrizes unitárias.

\subsection{Formulação Matricial da DWT de Vários Níveis}

\underline{\textbf{DWT de 2 níveis}}

\begin{center}
\includegraphics[width=0.4\textwidth]{images/DWTmulti.pdf}
\end{center}

\begin{align*}
	X^{(2)} &= \begin{pmatrix}
		W_{N/2}^a & 0 \\
		0 & I
	\end{pmatrix}W_N^ax \\
	&= W_N^{a,(2)}x
\end{align*}

\underline{\textbf{DWT de 3 níveis}}

\begin{align*}
	X^{(3)} &= \begin{pmatrix}
		X_{lll} \\ X_{llh} \\ X_{lh} \\ X_{h}
	\end{pmatrix} \\
	&= \begin{pmatrix}
		W_{N/4}^a & 0 & 0 & 0 \\
		0 & I & 0 & 0 \\
		0 & 0 & I & 0 \\
		0 & 0 & 0 & I
	\end{pmatrix}W_N^{a,(2)}x \\
	&= W_N^{a,(3)}x
\end{align*}

é possível generalizar por indução.

\underline{\textbf{Obs}}: Custo de implementação da DWT de vários níveis é sempre \textbf{linear} (considerando que os filtros tem suporte O(1)).

\subsection{Reconstrução da DWT por camadas}

Considere a DWT de segunda ordem:

\begin{align*}
	X^{(2)} &= W_N^{a,(2)}x\text{ (equação de análise)} \\
	x &= W_N^{s,(2)}X^{(2)}\text{ (equação de síntese)}
\end{align*}

Note que

\begin{align*}
	x &= W_N^{s,(2)}\left(\begin{pmatrix}
		X_{ll} \\ 0 \\ 0
	\end{pmatrix} + \begin{pmatrix}
		0 \\ X_{lh} \\ 0
	\end{pmatrix} + \begin{pmatrix}
		0 \\ 0 \\ X_h
	\end{pmatrix}\right) \\
	&= W_N^{s,(2)}\begin{pmatrix}
		X_{ll} \\ 0 \\ 0
	\end{pmatrix} + W_N^{s,(2)}\begin{pmatrix}
		0 \\ X_{lh} \\ 0
	\end{pmatrix} + W_N^{s}\begin{pmatrix}
		0 \\ 0 \\ X_h
	\end{pmatrix} \\
	&= \alpha_2(x) + \delta_2(x) + \delta_1(x)
\end{align*}

Note que $W_N^{s,2}(0,0,X_h)^T = W_N^s(0,0,X_h)^T$. Onde $\alpha_2(x)$ é a aproximação de 2$^a$ ordem, $\delta_2(x)$, os detalhes de 2$^a$ ordem e $\delta_1(x)$, os detalhes de 1$^a$ ordem. No primeiro nível, temos

\begin{align*}
	x &= \alpha_1(x) + \delta_1(x) \\
	&= W_N^s \begin{pmatrix}
		X_l \\ 0
	\end{pmatrix} + W_N^s \begin{pmatrix}
		0 \\ X_h
	\end{pmatrix}
\end{align*}

No caso da DWT de terceira ordem, temos

\begin{align*}
	x &= W_N^{s,(3)}\begin{pmatrix}
		X_{lll} \\ 0 \\ 0 \\ 0
	\end{pmatrix} + W_N^{s,(3)}\begin{pmatrix}
		0 \\ X_{llh} \\ 0 \\ 0
	\end{pmatrix} + W_N^{s,(2)}\begin{pmatrix}
		0 \\ 0 \\ X_{lh} \\ 0
	\end{pmatrix} + W_N^s\begin{pmatrix}
		0 \\ 0 \\ 0 \\ X_{h}
	\end{pmatrix} \\
	&= \alpha_3(x) + \delta_3(x) + \delta_2(x) + \delta_1(x)
\end{align*}

e em geral

\begin{equation*}
	\alpha_r(x) = \alpha_{r+1}(x) + \delta_{r+1}(x)
\end{equation*}

ou

\begin{equation*}
	\alpha_{r+1} = \alpha_r(x) - \delta_{r+1}(x)
\end{equation*}

\subsection{DWT em 2D}

\underline{\textbf{Def}}: Dada $A\in\mathcal{M}_{M\times N}(\mathbb{C})$, podemos definir um DWT de ordem $r$ pela expressão

\begin{equation*}
	\hat{A} = W_M^{a,(r)}A\left(W_N^{a,(r)}\right)^T
\end{equation*}

cuja DWT inversa será

\begin{equation*}
	A = W_M^{s,(r)}A\left(W_N^{s,(r)}\right)^T
\end{equation*}

\subsection{Seção 6.7: Desenho de bancos de filtros com reconstrução perfeita}

\begin{equation*}
	x = U(D(x*l_a))*l_s + U(D(x*h_a))*h_s, \forall x\in l^2(\mathbb{Z})
\end{equation*}

A propriedade da reconstrução perfeita pode ser traduzida para o domínio da transformada z. Por exemplo,

\begin{align*}
	&\text{se } x\leftrightarrow X(z) = \sum\limits_{k=-\infty}^\infty x_kz^{-k} \\
	&\text{e } l_a\leftrightarrow L_a(z) = \sum\limits_{k=-\infty}^\infty (l_a)_kz^{-k} \\
	&\text{então } x*l_a\leftrightarrow X(z)L_a(z)
\end{align*}

O efeito de aplicar a subamostragem seguida da superamostragem em um sinal $y\in l^2(\mathbb{Z})$ é

\begin{equation*}
	w = U(D(y))
\end{equation*}

onde 

\begin{equation*}
	w_k = \left\{\begin{array}{ll}
		y_k & \text{ se }k\text{ é par} \\
		0 & \text{ se }k\text{ é ímpar}
	\end{array}\right.
\end{equation*}

Logo a transformada z de $w$ se relaciona com a transformada z de $y$ como

\begin{align*}
	W(z) &= \sum\limits_{n=-\infty}^\infty w_kz^{-k} \\
	&= \sum\limits_{n=-\infty, n\text{ par}}^\infty y_kz^{-k} \\
	&= \sum\limits_{n=-\infty}^\infty \frac{1}{2}(y_k + (-1)^{-k})z^{-k} \\
	&= \frac{1}{2}\sum\limits_{n\in\mathbb{Z}}y_k^{-k} + \frac{1}{2}\sum\limits_{n\in\mathbb{Z}}y_k(-z)^{-k} \\
	&= \frac{1}{2}Y(z) + \frac{1}{2}Y(-z)
\end{align*}

Essa é a prova da Prop. 6.7.1, correspondente ao exercício 6.21. Podemos então escrever a condição de reconstrução perfeita no domínio da transformada z como:

\begin{align*}
	z^{-m}X(z) =& \left(\frac{1}{2}X(z)L_a(z) + \frac{1}{2}X(-z)L_a(-z)\right)L_s(z) + \\
	&\left(\frac{1}{2}X(z)H_a(z) + \frac{1}{2}X(-z)H_a(-z)\right)H_s(z), \forall X\text{ correspondente a }x\in l^2(\mathbb{Z})
\end{align*}

onde o fator $z^{-m}$ permite um atraso de $m$ amostras na reconstrução. Equivalentemente

\begin{equation*}
	z^{-m}X(z) = \frac{1}{2}\left[L_a(z)L_s(z)+H_a(z)H_s(z)\right]X(z) + \frac{1}{2}\left[L_a(-z)L_s(z)+H_a(-z)H_s(z)\right]X(-z)
\end{equation*}

Uma condição suficiente para garantir essa propriedade é exigir que

\begin{equation*}
	\begin{array}{l}
		(6.22)\\
		(6.23)
	\end{array}\left\{\begin{array}{l}
		L_a(z)L_s(z) + H_a(z)H_s(z) = 2z^{-m} \\
		L_a(-z)L_s(z) + H_a(-z)H_s(z) = 0
	\end{array}\right.
\end{equation*}

Pelo exercício 6.26 (lista 3) essa condição também é necessária.

\subsubsection{6.7.3: Estratégia 1 - Desenho dos filtros de síntese a partir dos filtros }

Supondo $L_a(z)$ e $H_a(z)$ conhecidos, a solução do sistema (6.22, 6.23) será:

\begin{equation*}
	\begin{array}{l}
		(6.34)\\
		(6.35)
	\end{array}\left\{\begin{array}{l}
		L_s(z) = \frac{2H_a(-z)z^{-m}}{H_a(-z)L_a(z)-H_a(z)L_a(-z)} \\
		H_s(z) = \frac{2L_a(-z)z^{-m}}{L_a(-z)H_a(z)-L_a(z)H_a(-z)}
	\end{array}\right.
\end{equation*}

A utilização destas expressões para definir vetores de coeficientes $l_s$ e $h_s$ dependem de:

\begin{enumerate}[I)]
	\item denominadores não-nulos
	\item expressões n forma $\sum w_kz^{-k}$, onde $w_k$ são os coeficientes
\end{enumerate}

\underline{\textbf{Exemplo}}: considere os filtros de Haar:

\begin{align*}
	l_a &= \left(\dots, 0, \frac{1}{2}, \frac{1}{2}, 0, \dots\right) \\
	h_a &= \left(\dots, 0, \frac{1}{2}, -\frac{1}{2}, 0, \dots\right)
\end{align*}

cujas transformadas z são

\begin{align*}
	L_a(z) &= \frac{1}{2} + \frac{1}{2}z^{-1} \\
	H_a(z) &= \frac{1}{2} - \frac{1}{2}z^{-1} \\
\end{align*}

Por (6.34, 6.35) com $m=0$, temos

\begin{align*}
	H_a(-z)L_a(z)-H_a(z)L_a(-z) &= \left(\frac{1}{2}-\frac{1}{2}(-z)^{-1}\right)\left(\frac{1}{2}+\frac{1}{2}z^{-1}\right)-\left(\frac{1}{2}-\frac{1}{2}z^{-1}\right)\left(\frac{1}{2}+\frac{1}{2}(-z)^{-1}\right) \\
	&= \frac{1}{4} + \frac{1}{2}z^{-1} + \frac{1}{4}z^{-2} - (\frac{1}{4} - \frac{1}{2}z^{-1} + \frac{1}{4}z^{-2}) \\
	&= z^{-1}
\end{align*}

Logo

\begin{align*}
	L_s(z) &= \frac{2H_a(-z)}{z^{-1}} \\
	&= \frac{2\left(\frac{1}{2}-\frac{1}{2}(-z)^{-1}\right)}{z^{-1}} \\
	&= z^{1} + 1
\end{align*}

Daí

\begin{equation*}
	l_s = (\dots, 0, 1, 1, 0, \dots), \text{ (os 1's estão nas posições }n=-1\text{ e }n=0)
\end{equation*}

\begin{align*}
	H_s(z) &= \frac{2L_a(-z)}{-z^{-1}} \\
	&= \frac{2\left(\frac{1}{2}+\frac{1}{2}(-z)^{-1}\right)}{-z^{-1}} \\
	&= -z^{+1} + 1
\end{align*}

De onde

\begin{equation*}
	h_s = (\dots, 0, -1, 1, 0, \dots), \text{ (os 1's estão nas posições }n=-1\text{ e }n=0)
\end{equation*}

\underline{\textbf{Exemplo}}: se $l_a = \left(\dots, 0, \frac{1}{2}, 0, \frac{1}{2}, 0, \dots\right)$ e $h_a = \left(\dots, 0, \frac{1}{2}, -\frac{1}{2}, 0, \dots\right)$ então $L_a(z) = \frac{1}{2} + \frac{1}{2}z^{-2}$ e

\begin{equation*}
	\left\{\begin{array}{l}
		L_s(z) = \frac{2z^2(z+1)}{z^2+1} \\
		H_s(z) = -2z
	\end{array}\right.
\end{equation*}

portanto isso \textbf{não produz} um banco de filtros \textbf{FIR}

\underline{\textbf{Observação 6.5}} (Tabela 6.2): Seja $g\in l^2(\mathbb{Z})$ cuja transformada z é $G(z) = \sum\limits_{n=-\infty}^\infty g_kz^{-k}$, iremos considerar as seguintes sequências obtidas a partir de $g$:

\begin{center}
\begin{tabular}{|l|l|}
	\hline
	\textbf{Sequência} & \textbf{Transformada z} \\ \hline
	$\tilde{g}_k = (-1)^kg_k$ & $\tilde{G}(z) = G(-z)$ \\
	$\tilde{g}_k = g_{-k}$ & $\tilde{G}(z) = G(z^{-1})$ \\
	$\tilde{g}_k = g_{N-1-k}$ & $\tilde{G}(z) = z^{-N+1}G(z^{-1})$ \\
	$\tilde{g}_k = (-1)^kg_{N-1-k}$ & $\tilde{G}(z) = (-z)^{-N+1}G(-z^{-1})$ \\
	\hline
\end{tabular}
\end{center}

\underline{\textbf{Observação 6.6}}: gostaríamos de garantir que os filtros $l_a$ e $l_s$ sejam passa-baixas e $h_a$ e $h_s$ sejam passa-altas, embora isso não seja necessário para a reconstrução perfeita. Vamos considerar como passa-baixas um filtro $g\in l^2(\mathbb{Z})$ que satisfaça

\begin{equation*}
	\left\{\begin{array}{l}
		G(1) \neq 0 \\
		G(-1) = 0
	\end{array}\right.
\end{equation*}

Onde 1 é a frequência d.c. (0 Hz) e -1 a frequência de Nyquist. Diremos que $g$ é passa-altas se

\begin{equation*}
	\left\{\begin{array}{l}
		G(1) = 0 \\
		G(-1) \neq 0
	\end{array}\right.
\end{equation*}

Note que as construções $\tilde{g}_k = (-1)^kg_k$ e $\tilde{g}_k = (-1)^kg_{N-1-k}$ invertem o comportamento dos filtros, ou seja, transformam passa-baixas em passa-altas e vice-versa. As transformações $\tilde{g}_k = g_{-k}$ e $\tilde{g}_k = g_{N-1-k}$ preservam a característica (passa-baixa ou passa-alta) do filtro $g$.

\subsubsection{6.7.4: Estratégia 2 -  Desenho de filtros usando filtros-produto}

Para satisfazer as equações

\begin{equation*}
	\begin{array}{l}
		(6.32)\\
		(6.33)
	\end{array}\left\{\begin{array}{l}
		L_a(z)L_s(z) + H_a(z)H_s(z) = 2z^{-m} \\
		L_a(-z)L_s(z) + H_a(-z)H_s(z) = 0
	\end{array}\right.
\end{equation*}

Uma maneira é forçar as condições

\begin{equation*}
	\left\{\begin{array}{l}
		L_s(z) = H_a(-z) \\
		H_s(z) = -L_a(-z)
	\end{array}\right.
\end{equation*}

Nesse caso a equação 3.33 é verificada automaticamente, e basta considerar a equação 6.32 modificada:

\begin{equation*}
	\begin{array}{ll}
		(6.32') & L_a(z)L_s(z) - L_a(-z)L_s(-z) = 2z^{-m}
	\end{array}
\end{equation*}

Note que a condição forçada

\begin{equation*}
	L_s(z) = H_a(-z) ^ H_s(z) = -L_a(-z)
\end{equation*}

corresponde aos coeficientes

\begin{equation*}
	\left\{
	\begin{array}{l}
		(l_s)_k = (-1)^k(h_a)_k \\
		(h_s)_k = -(-1)^k(l_a)_k
	\end{array}
	\right.
\end{equation*}

e isso produz filtros de síntese com os comportamentos $l_s=$ passa-baixa e $h_s=$ passa-alta esperados. Podemos considerar

\begin{equation*}
	P(z) = L_a(z)L_s(z)
\end{equation*}

e reescrever (6.32') como

\begin{equation*}
	\begin{array}{ll}
		(6.32'') & P(z) - P(-z) = 2z^{-m}
	\end{array}
\end{equation*}

Como a expressão $P(z) - P(-z)$ é uma função ímpar ($f(-t) = -f(t)$), então o expoente $m$ do lado direito tem que ser \textbf{ímpar}, pois se $P(z) = \sum\limits_{k=-\infty}^\infty p_kz^{-k}$ então

\begin{align*}
	P(z) - P(-z) &= \sum\limits_{k=-\infty}^\infty p_kz^{-k} - \sum\limits_{k=-\infty}^\infty p_k(-z)^{-k} \\
	&= \sum\limits_{k=-\infty}^\infty (p_k - (-1)^kp_k)z^{-k} \\
	&= \left\{\begin{array}{ll}
		0 & \text{ se }k\text{ é par} \\
		2p_k & \text{ se }k\text{ é ímpar}
	\end{array}\right.
\end{align*}

\underline{\textbf{Exemplo}}: Considere $m=1$; Vamos tentar obter um filtro $P(z)$ com grau 3:

\begin{equation*}
	P(z) = p_0 + p_1z^{-1} + p_2z^{-2} + p_3z^{-3}
\end{equation*}

Por (6.32')

\begin{equation*}
	2p_1z^{-1} + 2p_3z^{-3} = 2z^{-1} (\forall z)
\end{equation*}

temos $p_1 = 1$ e $p_3 = 0$. Para garantir que $P(z)$ é um filtro passa-baixas, precisamos da condição

\begin{align*}
	&\left\{\begin{array}{ll}
		P(-1) &= 0 \\
		P(1) &\neq 0
	\end{array}\right. \\
	\Rightarrow &\left\{\begin{array}{ll}
		p_0 - p_1 + p_2 - p_3 &= 0 \\
		p_0 + p_1 + p_2 + p_3 &\neq 0
	\end{array}\right. \\
	\Rightarrow &\left\{\begin{array}{ll}
		p_0 + p_2 &= 1 \\
		p_0 + p_1 + p_2 + p_3 &\neq 0
	\end{array}\right. \\
\end{align*}

Por exemplo, fazendo

\begin{equation*}
	p_0 = p_2 = \frac{1}{2},
\end{equation*}

temos

\begin{equation*}
	P(z) = \frac{1}{2} + z^{-1} + \frac{1}{2}z^{-2}
\end{equation*}

Fatorando $P(z)$,

\begin{equation*}
	P(z) = \frac{1}{2}(1 + z^{-1})^2
\end{equation*}

Podemos definir os filtros $L_a(\cdot)$ e $L_s(\cdot)$ como

\begin{equation*}
	L_a(z) = L_s(z) = \frac{1}{\sqrt{2}}(1 + z^{-1})
\end{equation*}

que corresponde aos filtros

\begin{align*}
	l_a &= (\dots, 0, \frac{1}{\sqrt{2}}, \frac{1}{\sqrt{2}}, 0, \dots) \\
	l_s &= (\dots, 0, \frac{1}{\sqrt{2}}, \frac{1}{\sqrt{2}}, 0, \dots)
\end{align*}

com filtros passa-altas

\begin{align*}
	h_a &= (\dots, 0, \frac{1}{\sqrt{2}}, -\frac{1}{\sqrt{2}}, 0, \dots) \\
	h_s &= (\dots, 0, -\frac{1}{\sqrt{2}}, \frac{1}{\sqrt{2}}, 0, \dots)
\end{align*}

(Filtros de Haar com atraso de 1 amostra)

Se tivéssemos escolhido $p_0 = \frac{3}{4}$ e $p_2 = \frac{1}{4}$, teríamos

\begin{equation*}
	P(z) = \frac{3}{4} + z^{-1} + \frac{1}{4}z^{-2} = \frac{1}{4}(1 + z^{-1})(3 + z^{-1})
\end{equation*}

se atribuíssemos

\begin{align*}
	L_a(z) &= \frac{1}{2}(1 + z^{-1})\text{ (passa-baixa)} \\
	L_s(z) &= \frac{1}{2}(3 + z^{-1})\text{ (não é passa-baixa)}
\end{align*}

\subsubsection{6.7.5: Estratégia 3}

A mesma estratégia forçando a presença de fatores $(1 + z^{-1})$ para os filtros $L_a$ e $L_s$:

\begin{equation*}
	P(z) = (1 + z^{-1})^{2r}Q(z)
\end{equation*}

Onde $r$ é um inteiro positivo, $Q(z)$ é um polinômio de grau $n$. Note que $(1-z^{-1})^{2r}$ possui coeficientes entre $-2r$ e 0 e $Q(z)$ entre 0 e $n$. A condição $P(z) - P(-z) = 2z^{-m}$ preserva apenas os expoentes ímpares entre $-2r$ e $+n$; supondo que $n$ é par teremos $r + \frac{n}{2}$ expoentes ímpares correspondendo a $r + \frac{n}{2}$ restrições lineares sobre os coeficientes $q_0, q_1, \dots, q_n$, de $Q(z)$. Para obter uma solução única, basta que

\begin{align*}
	\#\text{restrições} &= \#\text{variáveis} \\
	r + \frac{n}{2} &= n + 1
\end{align*}

Assim, basta escolher $n = 2(r-1)$ para produzir uma solução única $Q(z)$.

\underline{\textbf{Exemplo}}: tomando $r = 2$ e $n = 2$, temos $P(z) = (1 + z^{-1})^4(q_0 + q_1z + q_2z^2)$ e

\begin{align*}
	P(z) - P(-z) &= (2q_1 + 8q_2)z + (8q_0 + 12q_1 + 8q_2)z^{-1} + (8q_0 + 2q_1)z^{-3} \\
	&= 2z^{-1}
\end{align*}
\begin{align*}
	&\Leftrightarrow \left\{\begin{array}{ll}
		2q_1 + 8q_2 &= 0 \\
		8q_0 + 12q_1 + 8q_2 &= 2 \\
		8q_0 + 2q_1 &= 0
	\end{array}\right. \\
	&\Leftrightarrow \left\{\begin{array}{ll}
		q_0 &= -\frac{1}{16} \\
		q_1 &= \frac{1}{4} \\
		q_2 &= -\frac{1}{16}
	\end{array}\right.
\end{align*}

Deste modo, temos

\begin{align*}
	P(z) &= (1 + z^{-1})^4(-\frac{1}{16} + \frac{1}{4}z - \frac{1}{16}z^2) \\
	&= -\frac{1}{16}(1 + z^{-1})^4(z - r_1)(z - r_2)
\end{align*}

onde $r_1 = 2 - \sqrt{3}$ e $r_2 = 2 + \sqrt{3}$.

Podemos atribuir os fatores de $P(z)$ a $L_a(z)$ e $L_s(z)$ com o cuidado de manter fatores $(1 + z^{-1})$ nos dois filtros (garantindo a condição de passa-baixa). Por exemplo

\begin{align*}
	&\left\{\begin{array}{ll}
		L_a(z) &= -\frac{1}{8}(1 + z^{-1})^2(q - 4z + z^2) \\
		L_s(z) &= \frac{1}{2}(1 + z^{-1})^2
	\end{array}\right. \\
	\Rightarrow &\left\{\begin{array}{ll}
		L_a(z) &= -\frac{1}{8}(z^2 - 2z - 6 - 2z^{-1} + z^{-2}) \\
		L_s(z) &= \frac{1}{2}(1 + 2z^{-1} + z^{-2})
	\end{array}\right. \\
	\Rightarrow &\left\{\begin{array}{ll}
		l_a &= (\dots, 0, -\frac{1}{8}, \frac{1}{4}, \frac{3}{4}, \frac{1}{4}, -\frac{1}{8}, 0, \dots) \\
		l_s &= (\dots, 0, \frac{1}{2}, 1, \frac{1}{2}, 0, \dots)
	\end{array}\right. \\
\end{align*}

Que são utilizados para construir os filtros passa-alta utilizando

\begin{align*}
	(h_a)_k &= (-1)^k(l_s)_k \\
	(h_s)_k &= -(-1)^k(l_a)_k
\end{align*}

(Filtros Le Gall 5/3 - tabela 6.3)

Se definirmos $L_a(z)$ e $L_s(z)$ a partir do mesmo filtro-produto $P(z)$ como

\begin{align*}
	L_a(z) &= \frac{(\sqrt{3} + 1)\sqrt{2}}{8}(1 + z^{-1})^2(z - r_1) \\
	L_s(z) &= -\frac{(\sqrt{3} - 1)\sqrt{2}}{8}(1 + z^{-1})^2(z - r_2)
\end{align*}

teremos os filtros de Daubechies com 4 coeficientes:

\begin{align*}
	l_a &= \left(\dots, \frac{1 + \sqrt{3}}{4\sqrt{2}}, \frac{3 + \sqrt{3}}{4\sqrt{2}}, \frac{3 - \sqrt{3}}{4\sqrt{2}}, \frac{1 - \sqrt{3}}{4\sqrt{2}}, 0, \dots\right) \\
	l_s &= \left(\dots, \frac{1 - \sqrt{3}}{4\sqrt{2}}, \frac{3 - \sqrt{3}}{4\sqrt{2}}, \frac{3 + \sqrt{3}}{4\sqrt{2}}, \frac{1 + \sqrt{3}}{4\sqrt{2}}, 0, \dots\right)
\end{align*}

\subsubsection{6.7.5: Estratégia 4 - Desenho de bancos de filtros ortogonais}

Condição de ortogonalidade no domínio do tempo:

\begin{align*}
	(l_s)_k &= (l_a)_{-k}, \forall k \\
	(h_s)_k &= (h_a)_{-k}, \forall k
\end{align*}

E no domínio da transformada z:

\begin{align*}
	L_s(z) &= L_a(z^{-1}) \\
	H_s(z) &= H_a(z^{-1})
\end{align*}

A condição para reconstrução perfeita pode ser substituída por

\begin{equation*}
	\left\{\begin{array}{ll}
		L_a(z)L_a(z^{-1}) + H_a(z)H_a(z^{-1}) &= 2z^{-m} \\
		L_a(-z)L_a(z^{-1}) + H_a(-z)H_a(z^{-1}) &= 0
	\end{array}\right.
\end{equation*}

Podemos impor a condição adicional $(h_a)_k = (-1)^k(l_a)_{N-1-k}$, que corresponde a

\begin{equation*}
	H_a(z) = (-z)^{-(N-1)}L_a(-z^{-1}) = -z^{-(N-1)}L_a(-z^{-1}), \text{ (se }N\text{ é par)}
\end{equation*}

Neste caso,

\begin{align*}
	L_a(-z)L_a(z^{-1}) + H_a(-z)H_a(z^{-1}) &= L_a(-z)L_a(z^{-1}) + z^{-(N-1)}L_a(z^{-1})(-z^{+(N-1)}L_a(-z)) \\
	&= L_a(-z)L_a(z^{-1}) - L_a(z^{-1})L_a(-z) \\
	&= 0
\end{align*}

o que justifica as hipóteses simplificadoras introduzidas ($(h_a)_k = (-1)^k(l_a)_{N-1-k}$ com $N$ par). A condição de reconstrução perfeita fica

\begin{align*}
	L_a(z)L_a(z^{-1}) - z^{-(N-1)}L_a(-z^{-1})(-z^{+(N-1)})L_a(-z) &= 2z^{-m} \\
	L_a(z)L_a(z^{-1}) + L_a(-z)L_a(-z^{-1})
\end{align*}

Definindo $P(z) = L_a(z)L_a(z^{-1})$, temos que satisfazer

\begin{align*}
	P(z) + P(-z) &= 2z^{-m}
\end{align*}

Como a função é par então só existem coeficientes pares, portanto $m$ é par.

Por outro lado, note que

\begin{equation*}
	P(z^{-1}) = L_a(z^{-1})L_a((z^{-1})^{-1}) = P(z)
\end{equation*}

e assim

\begin{equation*}
	P(z) + P(-z) = P(z^{-1}) + P(-z^{-1}) = 2(z^{-1})^{-m}
\end{equation*}

Portanto apenas $m = 0$ produzirá soluções. Assim temos que encontrar $P(z)$ tal que

\begin{equation*}
	P(z) + P(-z) = 2
\end{equation*}

Supondo que $l_a$ tenha coeficientes não-nulos $(l_0, l_1, \dots, l_{N-1})$, podemos impor as restrições associadas a um filtro passa-baixas:

\begin{align*}
	L_a(1) &= \sum\limits_k l_k1^k = \sum\limits_{k=0}^{N-1}l_k \neq 0 \\
	L_a(-1) &= l_0 - l_1 + l_2 - l_3 \dots = 0
\end{align*}

Note ainda que $L_a(-1) = 0\Rightarrow P(-1) = 0 \Rightarrow P(1) = 2\Rightarrow L_a(1) = \sqrt{2}$ ou seja $\sum\limits_{k=0}^{N-1}l_k = \sqrt{2}$.

\textbf{A solução de Daubechies}:

Defina

\begin{align*}
	P(z) &= \left(\frac{z + z^{-1}}{2}\right)
\end{align*}

onde

\begin{equation*}
	Q(x) = c\int\limits_{-1}^x(1-y^2)^{\frac{N}{2}-1}dy
\end{equation*}

Onde $c$ é uma constante de normalização. Escolhendo qualquer $N$ par temos uma solução para o desenho de filtros ortogonais.

\underline{\textbf{Exemplo}}: Se $N = 2$, temos

\begin{align*}
	Q(x) &= x + 1
\end{align*}

e $P(z) = \frac{1}{2}z^{-1} + 1 + \frac{1}{2}z$ (Filtros de Haar).

Se $N = 4$, teremos

\begin{align*}
	Q(x) &= 1 + \frac{3}{2}x - \frac{1}{2}x^3
\end{align*}

e $P(z) = -\frac{1}{16}z^{-3} + \frac{9}{16}z^{-1} + 1 + \frac{9}{16}z - \frac{1}{16}z^3$. Definindo $L_a(z) = l_a + l_1z^{-1} + l_2z^{-2} + l_3z^{-3}$ e resolvendo o sistema $P(z) = L_a(z)L_a(z^{-1})$ teremos os filtros de Daubechies de 4 coeficientes.
