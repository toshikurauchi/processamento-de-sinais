\section{Espaços vetoriais}

Recordação de Álgebra Linear (Seção 1.4 do livro)

\subsection{Def 1.4.1}

Um espaço vetorial sobre $\mathbb{R}$ (ou $\mathbb{C}$) é um conjunto $V$ munido das operações de soma (entre elementos de $V$) e multiplicação de elementos de $V$ por escalares (em $\mathbb{R}$ ou em $\mathbb{C}$) com as seguintes propriedades:

\begin{enumerate}
	\item Fecho por adição: $\forall u, v\in V$ a soma $u+v$ está bem definida e pertence a $V$;
	\item Fecho por multiplicação por escalar: $\forall u\in V$ e $\forall\alpha\in\mathbb{R}$ (ou $\forall\alpha\in\mathbb{C}$) temos $\alpha u$ bem definido e pertence a $V$;
	\item A soma e o produto por escalar satisfazem as propriedades algébricas abaixo $\forall a,b\in\mathbb{R}$ (ou $\mathbb{C}$) e $\forall u, v\in V$:
	\begin{enumerate}
		\item $u+v = v+u$ (comutatividade)
		\item $(u+v)+w = u+(v+w)$ (associatividade)
		\item $\exists$ um vetor $\mathbf{0}$ t.q. $u+\mathbf{0} = \mathbf{0}+u = u$ (elemento neutro da soma)
		\item $\forall u\in V$ $\exists$ um vetor $w$ t.q. $u+w = \mathbf{0}$ (elemento inverso da soma)
		\item $(ab)u = a(bu)$
		\item $(a+b)u = au + bu$
		\item $a(u+v) = au + av$
		\item $1u = u$
	\end{enumerate}
\end{enumerate}

\subsection{Proposição 1.4.1}

\begin{enumerate}[(a)]
	\item O elemento neutro $\mathbf{0}$ é único;
	\item $0u = \mathbf{0}$ para qualquer $u\in V$;
	\item Para cada $u\in V$, o elemento inverso é único.
\end{enumerate}

\subsubsection{Demonstração (a)}

Suponha que exista um vetor $\mathtt{0} \neq \mathbf{0}$ tal que $u+\mathtt{0} = \mathtt{0} + u = u, \forall u\in V$. Como $\mathbf{0}$ é um elemento neutro da soma vale que $\mathbf{0} + \mathtt{0} = \mathtt{0}$, mas da suposição temos que $\mathbf{0} + \mathtt{0} = \mathbf{0}$ então $\mathtt{0} = \mathbf{0} + \mathtt{0} = \mathbf{0}$, uma contradição.
\qed

\subsubsection{Demonstração (b)}

Denotaremos $-u$ o inverso da soma de $u\in V$.

\begin{align*}
	(1 + 0)u &= 1u + 0u \text{ (f)} \\
	(1 + 0)u &= u + 0u \text{ (h)} \\
	1u + (-u) &= u + (-u) + 0u \\
	u + (-u) &= \mathbf{0} + 0u \text{ (c) e (h)} \\
	\mathbf{0} &= 0u \text{ (c) e (d)} 
\end{align*}
\qed

\subsubsection{Demonstração (c)}

Suponha que existam $v\in V$ e $w\in V$ tais que $v\neq w$ e $u + v = \mathbf{0}$ e $u + w = \mathbf{0}$. Daí:

\begin{align*}
	u + v &= \mathbf{0} \\
	u + v &= u + w \\
	u + v + v &= u + v + w \\
	\mathbf{0} + v &= \mathbf{0} + w \\
	v &= w \text{ (c)}
\end{align*}

Uma contradição.
\qed

\subsection{Exemplos}

\subsubsection{1.1)}

Espaço vetorial $\mathbb{R}^n$ (ou $\mathbb{C}^n$):
\begin{align*}
	\mathbb{R}^n &= \{(x_1,x_2,\dots, x_n)|x_i\in\mathbb{R}, i=1,2,\dots, n\} \\
	\mathbb{C}^n &= \{(x_1,x_2,\dots, x_n)|x_i\in\mathbb{C}, i=1,2,\dots, n\}
\end{align*}

\large{\textbf{Soma vetorial:}}

\begin{align*}
	(x_1,x_2,\dots,x_n)+(y_1,y_2,\dots,y_n) &= (x_1+y_1, x_2+y_2, \dots, x_n+y_n)
\end{align*}

Onde $x_i + y_i\in\mathbb{R}$ (ou $\mathbb{C}$), $1\leq i\leq n$.

\large{\textbf{Produto por escalar:}}

\begin{align*}
\alpha(x_1,x_2,\dots,x_n) &= (\alpha x_1,\alpha x_2,\dots,\alpha x_n)	
\end{align*}

\large{\textbf{Propriedades Algébricas:}}

\large{\textbf{a)}}
\begin{align*}
	(u_1,u_2,\dots,u_n) + (v_1,v_2,\dots, v_n) &= (u_1+v_1,u_2+v_2,\dots, u_n+v_n) \\
	\text{ (comutatividade em }\mathbb{R}) &= (v_1+u_1,v_2+u_2,\dots, v_n+u_n) \\
	&= (v_1,v_2,\dots, v_n) + (u_1,u_2,\dots,u_n)
\end{align*}

\large{\textbf{b)}}

\begin{align*}
	((u_1,\dots,u_n) + (v_1,\dots,v_n)) + (w_1,\dots,w_n) &= (u_1+v_1,\dots,u_n+v_n) + (w_1,\dots,w_n) \\
	&= (u_1+v_1+w_1,\dots,u_n+v_n+w_n) \\
	\text{ (associatividade em }\mathbb{R}) &= (u_1+(v_1+w_1),\dots,u_n+(v_n+w_n)) \\
	&= (u_1,\dots,u_n) + (v_1+w_1,\dots,v_n+w_n) \\
	&= (u_1,\dots,u_n) + ((v_1,\dots,v_n) + (w_1,\dots,w_n))
\end{align*}

\large{\textbf{c)}}

Precisa verificar que $\mathbf{0} = (0,0,\dots,0)$ satisfaz a propriedade.

\large{\textbf{d)}}

Precisa verificar que $w = (-u_1,-u_2,\dots, -u_n)$ satisfaz a propriedade $u+w=\mathbf{0}$.

\large{\textbf{e-h)}}

Recaem sobre as propriedades análogas no corpo ($\mathbb{R}$ ou $\mathbb{C}$) aplicadas a cada uma das componentes.

\subsubsection{1.2)}

\begin{align*}
	M_{m,n}(\mathbb{R}) &= \mathbb{R}^{m\times n}=\left\{\begin{pmatrix}
		a_11 & \dots & a_1n \\
		a_21 & \dots & a_2n \\
		\vdots & \ddots & \\
		a_n1 & \dots & a_nn \\
	\end{pmatrix}a_{i,j}\in\mathbb{R}, i=1,\dots, m, j=1,\dots,n\right\} \\
	M_{m,n}(\mathbb{C}) &= \mathbb{C}^{m\times n}=\left\{\begin{pmatrix}
		a_11 & \dots & a_1n \\
		a_21 & \dots & a_2n \\
		\vdots & \ddots & \\
		a_n1 & \dots & a_nn \\
	\end{pmatrix}a_{i,j}\in\mathbb{C}, i=1,\dots, m, j=1,\dots,n\right\}
\end{align*}

É um espaço vetorial com as operações:

\begin{align*}
	\begin{pmatrix}
		\ddots & \vdots & \\
		\dots & a_{i,j} & \dots \\
		 & \vdots & \ddots \\
	\end{pmatrix}
	+
	\begin{pmatrix}
		\ddots & \vdots & \\
		\dots & b_{i,j} & \dots \\
		 & \vdots & \ddots \\
	\end{pmatrix}
	&=
	\begin{pmatrix}
		\ddots & \vdots & \\
		\dots & a_{i,j} + b_{i,j} & \dots \\
		 & \vdots & \ddots \\
	\end{pmatrix} \\
	\alpha
	\begin{pmatrix}
		\ddots & \vdots & \\
		\dots & a_{i,j} & \dots \\
		 & \vdots & \ddots \\
	\end{pmatrix}
	&=
	\begin{pmatrix}
		\ddots & \vdots & \\
		\dots & \alpha a_{i,j} & \dots \\
		 & \vdots & \ddots \\
	\end{pmatrix}
\end{align*}

As propriedades da definição de espaço vetorial são verificadas analogamente ao caso do $\mathbb{R}^n$ ou $\mathbb{C}^n$, transferindo o trabalho para componente isoladamente. Estes espaços são estruturalmente idênticos aos espaços vetoriais $\mathbb{R}^{mn}$ e $\mathbb{C}^{mn}$.

\subsubsection{1.3)}

Considere o conjunto

\begin{align*}
	V &= \{(x_1,x_2,\dots | x_i\in\mathbb{R}\text{ ou }\mathbb{C}, \forall i\in\mathbb{N}\}
\end{align*}

Soma: $(x_1,x_2,\dots) + (y_1,y_2,\dots) = (x_1+y_1,x_2+y_2,\dots)$ ou, mais formalmente, $\{x_i\}_{i\in\mathbb{N}}+\{y_i\}_{i\in\mathbb{N}}=\{z_i\}_{i\in\mathbb{N}}$, onde $z_i = x_i+y_i,\forall i\in\mathbb{N}$.

Produto por escalar: $\alpha(x_1,x_2,\dots) = (\alpha x_1,\alpha x_2,\dots)$ ou, mais formalmente, $\alpha\{x_i\}_{i\in\mathbb{N}}=\{z_i\}_{i\in\mathbb{N}}$, onde $z_i = \alpha x_i, \forall i\in\mathbb{N}$

Variante: $W = \{(\dots, x_{-2}, x_{-1}, 0, x_1, x_2, \dots | x_i\in\mathbb{R}, i\in\mathbb{Z}\}$ com soma $\{x_i\}_{i\in\mathbb{Z}}+\{y_i\}_{i\in\mathbb{Z}}=\{x_i+y_i\}_{i\in\mathbb{Z}}$ e produto $\alpha\{x_i\}_{i\in\mathbb{Z}}=\{\alpha x_i\}_{i\in\mathbb{Z}}$.

\subsubsection{1.4)}

\large{\textbf{Def 1.4.2}}: Um subespaço vetorial de um espaço vetorial $V$ é um subconjunto $W\subseteq V$ que é fechado pela soma vetorial e pelo produto por escalar.

Considere o subconjunto das sequências infinitas ($u\in V$) ou o subconjunto das sequências bi-infinitas que possuem componentes limitados (dado $u\in V$ ou $u\in W$ existe um escalar $M>0$ t.q. $|u_i|\leq M,\forall i$).

Considere

\begin{align*}
	l^\infty &= L^\infty(\mathbb{N}) = \{(x_1,x_2,\dots) | x_i\in\mathbb{R}(\text{ ou }\mathbb{C}),\forall i\in\mathbb{N}\text{ e }\exists M>0\text{ t.q. }|x_i|\leq M \forall i\in\mathbb{N}\}
\end{align*}

ou ainda

\begin{align*}
	l^\infty = L^\infty(\mathbb{Z}) = \{&(\dots,x_{-2},x_{-1},x_0,x_1,x_2,\dots) | \\
	& x_i\in\mathbb{R}(\text{ ou }\mathbb{C}),\forall i\in\mathbb{Z}\text{ e }\exists M>0\text{ t.q. }|x_i|\leq M \forall i\in\mathbb{Z}\}
\end{align*}

Vamos mostrar que estes conjuntos são fechados pela soma e multiplicação por escalar:

\large{\textbf{Soma:}}

Sejam $\{x_i\}_{i\in\mathbb{N}}$ t.q. $|x_i| \leq M_x, \forall i\in\mathbb{N}$ e $\{y_i\}_{i\in\mathbb{N}}$ t.q. $|y_i| \leq M_y, \forall i\in\mathbb{N}$.

Por construção, o resultado da soma é $\{z_i\}_{i\in\mathbb{N}}$ onde $z_i = x_i + y_i, \forall i\in\mathbb{N}$. Existe $M_z$ t.q. $|z_i| \leq M_z,\forall i\in\mathbb{N}$?

Note que $|z_i| = |x_i+y_i| \leq |x_i| + |y_i| \leq M_x + M_y$ utilizando a desigualdade triangular e as limitações em $x$ e $y$. É verdade $\forall i\in\mathbb{N}$.

Assim, tomando $M_z = M_x + M_y$ temos que $|z_i| \leq M_z,\forall i\in\mathbb{N}$ e portanto $\{x_i\}_{i\in\mathbb{N}} + \{y_i\}_{i\in\mathbb{N}} = \{z_i\}_{i\in\mathbb{N}}\in l^\infty$ (mostrar que $L^\infty(\mathbb{Z})$ é fechado pela soma é análogo).

Para ver que $l^\infty$ é fechado por produtos por escalar, sejam $\{x_i\}_{i\in\mathbb{N}}\in l^\infty$ (t.q. $|x_i| \leq M_x, \forall i\in\mathbb{N}$) e $\alpha\in\mathbb{R}$ (ou $\mathbb{C}$) qualquer, temos que $\alpha\{x_i\}_{i\in\mathbb{N}} = \{\alpha x_i\}_{i\in\mathbb{N}}$, onde $|\alpha x_i| = |\alpha||x_i| \leq |\alpha|M_x$ e isso mostra que $\alpha\{x_i\}_{i\in\mathbb{N}}\in l^\infty$ (o argumento é análogo para $L^\infty(\mathbb{Z})$).

\subsubsection{1.5)}

Considere o conjunto das sequências infinitas ou bi-infinitas tais que

\begin{align*}
	\sum\limits_{i\in\mathbb{N}\text{ ou }\mathbb{Z}}|x_i|^2 &< \infty (*)
\end{align*}

Vamos verificar que a soma e o produto por escalar usando sequências que satisfazem $(*)$ sempre produzem resultados que satisfazem $(*)$.

Considere $\{x_i\}_{i\in\mathbb{Z}}$,$\{y_i\}_{i\in\mathbb{Z}}\in L\infty(\mathbb{Z})$ tais que

\begin{align*}
	\sum\limits_{i\in\mathbb{Z}}|x_i|^2 &< \infty \text{ e} \\
	\sum\limits_{i\in\mathbb{Z}}|x_i|^2 &< \infty
\end{align*}

e seja $\{z_i\}_{i\in\mathbb{Z}}$ definida como $z_i=x_i+y_i,\forall i\in\mathbb{Z}$.

Note que

\begin{align*}
	|z_i|^2 &= |x_i+y_i| \leq 2|x_i|^2 + 2|y_i|^2, \forall i\in\mathbb{Z}
\end{align*}

Logo

\begin{align*}
	\sum\limits_{i\in\mathbb{Z}}|z_i|^2 &=  \sum\limits_{i\in\mathbb{Z}}2|x_i|^2 + 2|y_i|^2 \\
	&= 2\sum\limits_{i\in\mathbb{Z}}|x_i|^2 + 2\sum\limits_{i\in\mathbb{Z}}|y_i|^2 \\
	&< \infty
\end{align*}

No caso do produto de $\{x_i\}_{i\in\mathbb{Z}}$ com $\sum\limits_{i\in\mathbb{Z}}|x_i|^2 < \infty$ por um escalar $\alpha$ temos que $\{\alpha x_i\}_{i\in\mathbb{Z}}$ satisfaz

\begin{align*}
	\sum\limits_{i\in\mathbb{Z}}|\alpha x_i|^2 &=  \sum\limits_{i\in\mathbb{Z}}|\alpha|^2|x_i|^2 \\
	&= |\alpha|^2\sum\limits_{i\in\mathbb{Z}}|x_i|^2 \\
	&< \infty
\end{align*}

\subsubsection{1.6)}

Considere o conjunto das funções contínuas definidas num intervalo $[a,b]\subseteq\mathbb{R}$ ($a,b\in\mathbb{R}$ fixados):

\begin{align*}
	C[a,b] &= \{f:[a,b]\rightarrow\mathbb{R}\text{ ou }\mathbb{C} | f\text{ é contínua em }[a,b]\}
\end{align*}

que é o mesmo que 

\begin{align*}
	C[a,b] &= \{f:[a,b]\rightarrow\mathbb{R}\text{ ou }\mathbb{C} | \forall \bar{t}\in [a,b],\exists\lim\limits_{t\rightarrow\bar{t}} f(t)\text{ e }f(t) = \lim\limits_{t\rightarrow\bar{t}} f(t)\}
\end{align*}

Sejam $f,g\in C[a,b]$, e considere as definições $f+g = h:[a,b]\rightarrow\mathbb{R} (\mathbb{C})$ dada por $h(x) = f(x) + g(x), \forall x\in [a,b]$ e $\alpha f= k:[a,b]\rightarrow\mathbb{R} (\mathbb{C})$ dada por $k(x) = \alpha f(x), \forall x\in [a,b]$.

Temos que $h(x)$ e $k(x)$ são funções contínuas (ver livro de Cálculo I), e as propriedades algébricas (a)-(h) da def. de espaço vetorial são verificadas independentemente para cada $x\in[a,b]$.

Definimos analogamente o conjunto $C(a,b)$ das funções contínuas definidas no intervalo aberto $(a,b)\subseteq\mathbb{R}$.

\subsubsection{1.7)}

O subespaço $V=\{f:[a,b]\rightarrow\mathbb{R}|f$ possui uma quantidade finita de descontinuidades em $[a,b]\}$.

\begin{align*}
	(f+g)(t) &\coloneqq f(t) + g(t)
\end{align*}

Para mostrar que o conjunto é fechado para a soma basta verificar que $\{t\in [a,b]|f+g$ é descontínua em $t\}\subseteq\{t\in [a,b]|f$ é descontínua em $t\}\cup\{t\in [a,b]|g$ é descontínua em $t\}$, o que implica que o número de descontinuidades em $f+g$ é menor ou igual à soma das descontinuidades de $f$ e $g$, que é finita.

\begin{align*}
	(\alpha f)(t) &\coloneqq \alpha f(t), \forall t\in [a,b]
\end{align*}

Note que o número de descontinuidades de $\alpha f$ é o mesmo que o número de descontinuidades de $f$, que é finito.

As propriedades algébricas são verificadas facilmente (recaem sobre as propriedades análogas em $\mathbb{R}$ ou $\mathbb{C}$).

\subsubsection{1.8)}

Considere o conjunto $V = \{f\in C(a,b)|\int\limits_a^b f(t)^2dt < \infty\}$. Para ver que $V$ é fechado pela soma, sejam $f,g \in V$ e observe que:

\begin{align*}
	\int\limits_a^b[(f+g)(t)]^2dt &\leq \int\limits_a^b2f(t)^2 + 2g(t)^2dt \\
	&= 2\int\limits_a^b f(t)^2dt + 2\int\limits_a^b g(t)^2dt \\
	&< \infty
\end{align*}

Além disso,

\begin{align*}
	\int\limits_a^b (\alpha f)(t)^2dt = \alpha^2\int\limits_a^b f(t)^2dt < \infty
\end{align*}

mostrando que $V$ é fechado por multiplicação por escalar. Isso mostra que $V$ é subespaço vetorial de $C(a,b)$ e, portanto, é um espaço vetorial.

Note que a integral é imprópria e deve ser entendida como:

\begin{align*}
	\int\limits_a^b f(x) &= \lim\limits_{p\rightarrow a^+}\int\limits_p^r f(x) + \lim\limits_{q\rightarrow b^-}\int\limits_r^q f(x)
\end{align*}

\subsubsection{1.9)}

Considere o conjunto $L^2([a,b]\times [c,d])$ das funções

\begin{align*}
	f:[a,b]\times [c,d]\rightarrow\mathbb{R}\text{ ou }(\mathbb{C})
\end{align*}

contínuas e que satisfazem

\begin{align*}
	\int\limits_a^b\int\limits_c^d f(x,y)^2dy dx < \infty
\end{align*}

O conjunto

\begin{align*}
	C([a,b]\times [c,d]) = \{f: [a,b]\times [c,d]\rightarrow\mathbb{R}\text{ ou }\mathbb{C}\}
\end{align*}

é um espaço vetorial munido das operações usuais:

\begin{align*}
	(f+g)(x,y) &= f(x,y) + g(x,y), \forall x\in [a,b], \forall y\in [c,d] \\
	(\alpha f)(x,y) &= \alpha f(x,y), \forall x\in [a,b], \forall y\in [c,d]
\end{align*}

Seja $\Omega = [a,b]\times [c,d]$. Note ainda que $L^2(\Omega)$ é um subespaço vetorial de $C(\Omega)$, pois

\begin{align*}
	\int\limits_a^b\int\limits_c^d (f+g)^2(x,y)dydx &\leq \int\limits_a^b\int\limits_c^d 2f^2(x,y) + 2g^2(x,y) dydx \\
	&= 2\int\limits_a^b\int\limits_c^d f^2(x,y)dydx + 2\int\limits_a^b\int\limits_c^d g^2(x,y)dydx \\
	&< \infty
\end{align*}

e de forma análoga mostramos que $\int\limits_a^b\int\limits_c^d (\alpha f)(x,y)dydx < \infty$.
