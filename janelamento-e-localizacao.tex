\section{Capítulo 5: Janelamento e Localização}

Uma análise de Fourier ``janelada'' consiste em recortar um sinal $x\in\mathbb{C}^N$ em um intervalo $[m, m+M)$ e tomar a DFT do vetor

\begin{equation*}
	\tilde{x} = (x_m, x_{m+1}, \dots, x_{m+M-1})'
\end{equation*}

Podemos pensar em um passo intermediário que consiste em definir $y\in\mathbb{C}^N$ por $y_n = x_nw_n$ onde $w_n = \left\{\begin{array}{ll}
	1 & s\text{ se }n\in[m,m+M) \\
	0 & \text{ c.c.}
\end{array}\right.$

Mais passos intermediários:

\begin{itemize}
	\item Shift circular: passar de $y\in\mathbb{C}^N$ para $\tilde{y}=(y_m, \dots, y_{m+M-1}, 0, 0, \dots, 0)$ (com $N-M$ zeros);
	\item Recorte de $\tilde{y}\in\mathbb{C}^N$ para $\tilde{x} = (\tilde{y}_0, \tilde{y}_1, \dots, \tilde{y}_{M-1})\in\mathbb{C}^M$.
\end{itemize}

\underline{\textbf{Análise do 1$^o$ passo}}: $x\rightarrow y = X.W$.

\underline{\textbf{Digressão}}: teorema da convolução ``espectral'' (Prop. 5.2.1: Se $x, w\in\mathbb{C}^N$ e $y = x.w\in\mathbb{C}^N$ (multiplicação ponto a ponto), então $Y = \frac{1}{N}X*W$ onde $X, Y$ e $W$ são as DFT's de $x, y$ e $w$).

\underline{\textbf{Digressão 2}}: Se $X = DFT(x)$, temos

\begin{equation*}
	X_k = \sum\limits_{n=0}^{N-1}x_ne^{-i2\pi kn/N}
\end{equation*}

e

\begin{align*}
	x_n &= \frac{1}{N}\sum\limits_{k=0}^{N-1}X_ke^{i2\pi kn/N} \\
	&= \frac{1}{N}\sum\limits_{l=0}^{-N+1}X_{-l}e^{-i2\pi ln/N} \\
	&= \frac{1}{N}\sum\limits_{l=0}^{N-1}X_{-l}e^{-i2\pi ln/N} \\
	&= \frac{1}{N} DFT(\overleftarrow{X})_n
\end{align*}

Onde $\overleftarrow{X}\in\mathbb{C}^N$ dado por $\overleftarrow{X}_k = X_{-k}$. Isso mostra que

\begin{equation*}
	x = \frac{1}{N} DFT(\overleftarrow{X})
\end{equation*}

Voltando ao contexto da multiplicação de sinais ($x, y, w\in\mathbb{C}^N$ com $y_n = x_nw_n$), temos

\begin{align*}
	y &= x.w \\
	\Rightarrow \frac{1}{N} DFT(\overleftarrow{Y}) &= \left(\frac{1}{N} DFT(\overleftarrow{X})\right).\left(\frac{1}{N} DFT(\overleftarrow{W})\right) \\
	\Rightarrow DFT(\overleftarrow{Y}) = \frac{1}{N} DFT(\overleftarrow{X}).DFT(\overleftarrow{W}) \\
	\Rightarrow \overleftarrow{DFT(Y)} = \overleftarrow{\frac{1}{N} DFT(X)}.\overleftarrow{DFT(W)} \\
	\Rightarrow DFT(Y) = \frac{1}{N} DFT(X).DFT(W)
\end{align*}

Aplicando o teorema da convolução original temos então que

\begin{equation*}
	Y = \frac{1}{N}X*W
\end{equation*}

Assim o passo $y=x.w$ produz $Y = \frac{1}{N}X*W$.

\underline{\textbf{Exemplo}}: Se $x\in E_{N,k}$, então, 

\begin{equation*}
	X = (0, 0, \dots, 1, 0, \dots, 0)'
\end{equation*}

temos 

\begin{align*}
	(X*W)_r &= \sum\limits_{l=0}X_lW_{r-l} \\
	&= W_{r-k}
\end{align*}

\underline{\textbf{Análise do 2$^o$ passo}}: $y\rightarrow \tilde{y}$, onde $\tilde{y} = y_{m+n\mod N}$. No domínio espectral, temos

\begin{align*}
	\tilde{Y}_k &= \sum\limits_{n=0}^{N-1}\tilde{y}_ne^{-i2\pi kn/N} \\
	&= \sum\limits_{n=0}^{N-1}y_{m+n}e^{-i2\pi kn/N} \\
	&= \left(\sum\limits_{l=0}^{N-1}y_le^{-i2\pi kl/N}\right)e^{i2\pi km/N} \\
	&= e^{i2\pi km/N}Y_k
\end{align*}

\underline{\textbf{Análise do 3$^o$ passo}}: $\tilde{y}\in\mathbb{C}^N\rightarrow \tilde{x}\in\mathbb{C}^M$:

Hipótese simplificadora: $N = \alpha M$ para $\alpha\in\mathbb{N}$.

\begin{align*}
	\tilde{X}_k &= \sum\limits_{n=0}^{M-1}\tilde{x}_ne^{-i2\pi kn/M} \\
	&= \sum\limits_{n=0}^{M-1}\tilde{y}_ne^{-i2\pi kn/M}(\text{pois } \tilde{y}_n = \tilde{x}_n, n = 0, 1, \dots, M-1) \\
	&= \sum\limits_{n=0}^{N-1}\tilde{y}_ne^{-i2\pi kn/M}(\text{pois } \tilde{y}_n = 0, \forall n \geq M) \\
	&= \sum\limits_{n=0}^{N-1}\tilde{y}_ne^{-i2\pi (\alpha k)n/N}(\text{pois } M = N/\alpha) \\
	&= \tilde{Y}_{(\alpha k)}
\end{align*}

Pensando na transformação completa ($x\in\mathbb{C}^N\rightarrow\tilde{x}\in\mathbb{C}^M$):

\begin{align*}
	\tilde{X}_k &= \tilde{Y}_{(\alpha k)} \\
	&= e^{i2\pi (\alpha k)m/N}Y_{(\alpha k)} \\
	&= \frac{e^{i2\pi (\alpha k)m/N}}{N}(X*W)_{(\alpha k)} \\
	&= \frac{e^{i2\pi km/M}}{\alpha M}(X*W)_{(\alpha k)}
\end{align*}

\subsection{Espectograma}

A partir de um sinal $x\in\mathbb{C}^N$ e parâmetros $n, M\in\mathbb{N}$, construir os espectros dos sinais

\begin{equation*}
	\tilde{x}_{m,M} = (x_m, x_{m+1}, \dots, x_{m+M-1})'
\end{equation*}

para $m = 0, n, 2n, \dots,$ ``$\left\lfloor\frac{N}{n}\right\rfloor n$''.

Cada coluna do espectrograma contém $M$ valores que correspondem às frequências $0, 1, 2, \dots, M-1$ ou $-\frac{M}{2}+1, -\frac{M}{2}+2, \dots, 0, 1, 2, \dots, \frac{M}{2}$ no contexto da DFT em $\mathbb{C}^M$, que corresponde às frequências $\left[\left(-\frac{M}{2}+1\right)\frac{1}{T}, \dots, 0, \frac{1}{T}, \frac{2}{T}, \dots, \frac{M}{2T}\right]$Hz onde $T = $ duração de $M$ amostras $=M\frac{1}{R}$ segundos, onde $R$ é a taxa de amostragem em Hz, ou seja, cada espectro $\tilde{X}_{m,M}$ mostra as frequências $-\frac{R}{2}+\frac{R}{M}, \dots, 0, \frac{R}{M}, \frac{2R}{M}, \dots, \frac{R}{2}$.

\subsection{Janelamento com outras janelas}

As descontinuidades da janela retangular afetam o seu espectro. Por esse motivo pode ser interessante utilizar outras janelas. O caso ideal ocorreria quando a convolução do espectro do sinal com o espectro da janela não alterasse o espectro do sinal. Entretanto isso só ocorreria se o espectro da janela fosse o delta de Dirac (que só ocorre no sinal constante igual a 1).

Alternativamente utilizaremos a janela triangular, a janela gaussiana e a janela de Hamming ($\phi(t) = 0.54 - 0.46\cos(2\pi t)$) para $t = (j-m)/M, j = m, m+1, \dots, m+M$.
