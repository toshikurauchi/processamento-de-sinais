\section{Transformada Rápida de Fourier (FFT)}

Lembrando a equação de análise

\begin{equation*}
	X_k = \sum\limits_{n=0}^{N-1} x_n e^{-i2\pi kn/N}, \forall k\in\mathbb{Z}
\end{equation*}

Considere que $N = 2^B$ onde $B\in\mathbb{N}$. Separando a somatória anterior em índices pares e ímpares temos:

\begin{align*}
	X_k &= \sum\limits_{n = 0, 2, 4, \dots, N-2} x_ne^{-i2\pi kn/N} + \sum\limits_{n = 1, 3, \dots, N-1}x_ne^{-i2\pi kn/N} \\
	&= \sum\limits_{m = 0}^{\frac{N}{2} - 1} x_{2m}e^{-i2\pi k(2m)/N} + \sum\limits_{m = 0}^{\frac{N}{2} - 1}x_{2m+1}e^{-i2\pi k(2m+1)/N} \\
	&= \sum\limits_{m = 0}^{\frac{N}{2} - 1}x_m^{par}e^{-i2\pi km/(\frac{N}{2})} + \sum\limits_{m = 0}^{\frac{N}{2} - 1}x_m^{impar} e^{-i2\pi lm/(\frac{N}{2})e^{-i2\pi k/N}} \\
	&= [DFT(x^{par})]_k + e^{-i2\pi k/N}[DFT(x^{impar})]_k, \forall k\in\mathbb{Z} (*)
\end{align*}

\subsection{O algoritmo da FFT}

$FFT(x)$

\begin{itemize}
	\item Construa $x^{par}$ e $x^{impar}$ (custo = $\alpha N$)
	\item Computa $FFT(x^{par})$ e $FFT(x^{impar})$ (custo = $2T(\frac{N}{2})$)
	\item Monta $X_0, X_1, \dots, X_{N-1}$ de acordo com a expressão $(*)$ (custo = $\beta N$)
\end{itemize}

Seja $T(N)$ o tempo para computar a $FFT(x)$ com $x\in\mathbb{C}^N$ ($\gamma = \alpha + \beta$).

\begin{align*}
	T(N) &= \gamma N + 2T(\frac{N}{2}) \\
	&= \gamma N + 2[\gamma \frac{N}{2} + 2T(\frac{N}{4})] \\
	&= 2\gamma N + 4T(\frac{N}{4}) \\
	&= 3\gamma N + 8T(\frac{N}{8}) \\
	&= l\gamma N + 2^l T(\frac{N}{2^l}) \text{ (provar por indução)}
\end{align*}

para $l = 1, 2, \dots, B$. Quando $l = B = \log_2N$ temos $T(\frac{N}{2^l}) = O(1)$ e portanto 

\begin{align*}
	T(N) &= \gamma N\log_2N + NO(1) \\
	&= O(N\log N)
\end{align*}

\subsection{DFT 2D}

\textbf{\underline{Def}}: Dada uma imagem $A\in M_{M,N}(\mathbb{C})$, definimos sua DFT como a matriz $\hat{A}\in M_{M,N}(\mathbb{C})$ dada por (Equação de Análise):

\begin{align*}
	\hat{A}_{kl} &= (A, E_{M,N,k,l}) \\
	&= \sum\limits_{m=0}^{M-1}\sum\limits_{n=0}^{N-1}A_{mn}e^{-i2\pi (km/M + ln/N)}
\end{align*}

com a correspondente equação de síntese dada por:

\begin{align*}
	A &= \frac{1}{MN}\sum\limits_{m=0}^{M-1}\sum\limits_{n=0}^{N-1} \hat{A}_{k,l} E_{M,N,k,l} \text{ (forma vetorial)} \\
	A_{mn} &= \frac{1}{MN}\sum\limits_{m=0}^{M-1}\sum\limits_{n=0}^{N-1} \hat{A}_{k,l} e^{i2\pi (km/M+ln/N)} \text{ (forma por componentes)}
\end{align*}

\textbf{\underline{Obs}}: Podemos usar os índices $k = 0, 1, \dots, M-1$ e $l = 0, 1, \dots, N-1$ como nas equações acima, mas também podemos usar outras faixas de $M$ e $N$ índices, como por exemplo: $k = -\frac{M}{2} + 1, \dots, 0, \dots, \frac{M}{2}$ e $l = -\frac{N}{2} + 1, \dots, 0, \dots, \frac{N}{2}$.

\subsubsection{Cálculo da DFT2D}

Note que

\begin{align*}
	\hat{A}_{k,l} &= \sum\limits_{m=1}^{M-1}\sum\limits_{n=0}^{N-1} A_{mn}e^{-i2\pi (km/M + ln/N)} \\
	&= \sum\limits_{n=0}^{N-1}\left[\sum\limits_{m=1}^{M-1} A_{mn} e^{-i2\pi km/M}\right]e^{-i2\pi ln/N} \\
	&= \sum\limits_{n=0}^{N-1} (F_M A)_{k,n} e^{-i2\pi ln/N} \\
	&= \text{ coeficiente da frequência } l \text{ na DFT da linha }k\text{ da matriz }F_M A \\
	&= \left[(F_M A)F_N^T\right]_{k,l}, \forall k,l
\end{align*}

Onde $\sum\limits_{m=1}^{M-1} A_{mn} e^{-i2\pi km/M} = DFT(A^n)_k$, $A^n$ é a $n$-ésima coluna de $A$ e $A_k$ é a $k$-ésima linha de $A$. Consequentemente $\hat{A} = F_M A F_N^T$ (proposição 2.7.1). Mas essa multiplicação de matrizes consome tempo proporcional a O($M^2N$) + O($MN^2$) = O($M^2N^2$), ou seja, ainda não ganhamos nada. Devemos aplicar a FFT para calcular esse resultado.

\subsection{Calculando o produto $F_M A F_N^T$}

Usando a FFT:

\begin{align*}
	F_M A &= \left[FFT(A^0) | FFT(A^1) | \dots | FFT(A^{N-1})\right] \\
	(F_M A)F_N^T &= \left[\begin{matrix}
		FFT(F_M A)_0 \\ FFT(F_M A)_1 \\ \vdots \\ FFT(F_M A)_{M-1}
	\end{matrix}\right]
\end{align*}

Como são realizadas $N$ FFTs e depois $M$ FFTs o custo total é: O($NM\log M + MN\log N$) = O($MN\log(MN)$).
