# Algoritmos para Processamento de Áudio, Imagem e Vídeo

Essas são as notas de aula da disciplina ministrada pelo professor Marcelo Queiroz no segundo semestre de 2013 no IME-USP. Erros, principalmente de digitação, são bastante prováveis, então recomenda-se cuidado durante a leitura ;)

# Compilação

O arquivo principal é [processamentoDeSinais.tex](processamento-de-sinais/src/master/processamentoDeSinais.tex). Para compilá-lo basta executar em um terminal:

	$ latex processamentoDeSinais.tex

ou,

	$ pdflatex processamentoDeSinais.tex

# Colaboração

Caso você deseje colaborar de alguma forma (conteúdo, correções, etc.) entre em contato comigo para que eu o adicione como colaborador.

# PDF

A última versão do arquivo PDF compilado pode ser encontrada em [processamento-de-sinais/src/master/processamentoDeSinais.pdf](https://bitbucket.org/toshikurauchi/processamento-de-sinais/raw/2a8fc806c4525b131f8786910572c2639322bc12/processamentoDeSinais.pdf).